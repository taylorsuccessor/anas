<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="5">
                <table>
                    <tr>
                        <td class="title">
                            <img src="/assets/admin/images/logo.png" style="width:100%; max-width:300px;">
                        </td>

                        <td>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="5">

















@if(isset($policy->id))

{!! $policy->body() !!}

@else
<div style="max-width:100%; white-space: initial;">





Privacy Policy
Our privacy policy describes the ways in which we collect, store, use and protect your personal information and it is important for you to review this privacy policy.
By "personal information" we mean information that can be associated with a specific person and can be used to identify that person.
We do not consider anonymised information to constitute personal information as it cannot be used to identify a specific person. We collect personal information from you when you use application or its related websites  or when you telephone or email our customer support team).
By providing us with your personal information you expressly consent to us processing your personal information in accordance with the terms of our privacy policy.
We may amend our privacy policy at any time by posting a revised version on the Site.
The revised version will be effective at the time we post it and, following such posting, your continued use of the Site will constitute your express consent to us continuing to process your personal information in accordance with the terms of our revised privacy policy.

Our privacy policy covers the following topics:
our collection of your personal information
our use of your personal information
your use of your and other users’ personal information
accessing, reviewing and amending your personal information
protecting your personal information
how you can contact us about privacy questions

Our collection of your personal information
As part of your registration on the Site, you will be asked to provide us with certain personal information, such as your name, email and/or telephone number
We use this financial information for billing purposes and for the fulfillment of your orders.
Following your registration on the Site, you should not post any personal information (including any financial information)
We will collect transactional information based on your activities using the Site (such as buying and selling items and participating in auctions), This transactional information is used solely in relation to the transactions you undertake on the Site and for no other reasons.
Please note that we may use your Internet protocol (or IP) address (which is a unique number assigned to your computer server or your Internet service provider (or ISP)) to analyse user trends and improve the administration of the Site.
We may also collect information about your computer (for example, mobile type) and navigation information (for example, the pages you visit on the Site) along with the times that you access the Site.
Finally, we may collect additional information from or about you in other ways not specifically described here.
For example, we may collect information related to your contact with our customer support team or store results when you respond to a survey.
We may also collect feedback ratings and other comments relating to your use of the application.

Our use of your personal information
We only use your personal information to provide services and customer support to you; to measure and improve our services to you; to prevent illegal activities and implement our user agreement with you ("User Agreement"); troubleshoot problems; collect fees; provide you with promotional emails.
Though we make every effort to preserve your privacy, we may need to disclose your personal information to law enforcement agencies, government agencies or other third parties where we are compelled so to do by court order or similar legal procedure; where we are required to disclose your personal information to comply with law; where we are cooperating with an ongoing law enforcement investigation or where we have a good faith belief that our disclosure of your personal information is necessary to prevent physical harm or financial loss, to report suspected illegal activity or to investigate a possible violation of our User Agreement.
In the event of a sale of Jomlah, any of its affiliates and subsidiaries or any related business assets, your personal information may be disclosed to any potential purchaser for the purposes of the continued provision of the Site or otherwise in relation to any such sale.
We may share your personal information with our other group companies so as to provide joint content and services to you, to help detect illegal acts and/or the violations of our policies.
This contact information will be the personal information provided by you to us on registration and, as a result, should always be kept up-to-date.
By registering on the Site, you give us your express consent to receive promotional emails about our services and emails announcing changes to, and new features on, the Site.
Additionally, we do use comments made by you about the Site for marketing purposes and by making such comments you expressly consent to our using such comments for marketing purposes.

Your use of your and other users’ personal information.
Site members may need to share personal information (including financial information) with each other to complete transactions on the Site. You should respect, at all times, the privacy of other Site members.
We cannot guarantee the privacy of your personal information when you share personal information with other Site members so you should always seek information on the privacy and security policies of any other Site members with whom you are transacting prior to sharing any of your personal information with another Site member.
This privacy policy does not cover your release of your personal information to another Site member.
You agree to use any personal information received from another Site member in relation to a transaction on the Site solely in relation to such transaction and shall not use the information received from another Site member for any other purposes (except with the express consent of the other Site member).
You acknowledge and agree and you shall use personal information received from another Site member in accordance with all applicable laws.

Accessing, reviewing and amending your personal information
You can access and review your personal information in the My Account section of the Site.
If your personal information changes in any way or is incorrectly presented on the Site you should immediately update or correct your personal information (as applicable) by accessing the My Account section on the Site or, alternatively, by contacting our customer support team.
The "Customer Support" link at the top of each Site webpage contains our customer support email and phone details.

Please note that we shall retain your personal information during and following the end of your use of the Site as required to comply with law, for technical troubleshooting requirements, to prevent fraud, to assist in any legal investigations and to take any other actions otherwise permitted by law.

No spam or spoof emails
We do not tolerate spam. To report Site related spam or spoof emails, please forward the email to jomlahsystem@gmail.com. You may not use our communication tools to send spam or otherwise send content that would violate our User Agreement. We automatically scan and may manually filter messages to check for spam, viruses, phishing attacks and other malicious activity or illegal or prohibited content.

Protecting your personal information
the Internet is not a secure medium and we cannot guarantee the privacy of your personal information.
Choose your password carefully using unique numbers, letters and special characters.
Never share your username and password with anyone.
If you are concerned that your username or password has been compromised, please contact our customer support team immediately and ensure you change your password by logging onto the My Account section of the Site.



How you can contact us about privacy questions
If you have questions or concerns about our collection and use of your personal information, please contact our customer support team

Last update: February 5th, 2018


</div>

@endif

            </td>
        </tr>









    </table>
</div>
</body>
</html>
