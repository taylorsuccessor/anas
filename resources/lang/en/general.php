<?php

return [

    'name'=>'Name',
    'id'=>'Id',
    'search'=>'Search',
    'showing'=>'Showing',
    'to'=>'to',
    'of'=>'of',
    'entries'=>'entries',
    'all'=>'All',

    'dashboard'=>'Dashboard',

    'CopyRights'=>'2016 &copy; Fintolog.com',

    'language'=>'Language',


    'notification'=>'Notification',
    'setting'=>'Setting',
    'basic'=>'Basic',
    'edit_config'=>'Edit Config',
    'roles'=>'Roles',
    'rolesCreate'=>'Create Role',
    'created_at'=>'Created At',
    'updated_at'=>'Updated At',
    'logout'=>'Logout',

    'authentication'=>'Authentication',
    'authenticationList'=>'Authentication List',
    'authenticationCreate'=>'Create Authentication ',
    'authenticationMenuBottomHeader'=>'AuthenticationMenu Bottom Header',
    'authenticationMenuBottomDescription'=>'Authentication Menu Bottom Description',



    'layout'=>'Layout',
    'layoutList'=>'Layout List',
    'layoutCreate'=>'Create Layout ',
    'layoutMenuBottomHeader'=>'LayoutMenu Bottom Header',
    'layoutMenuBottomDescription'=>'Layout Menu Bottom Description',




    'role'=>'Role',
    'roleList'=>'Role List',
    'roleCreate'=>'Create Role ',
    'roleMenuBottomHeader'=>'RoleMenu Bottom Header',
    'roleMenuBottomDescription'=>'Role Menu Bottom Description',


    'user'=>'User',
    'userList'=>'User List',
    'userCreate'=>'Create User ',
    'userMenuBottomHeader'=>'UserMenu Bottom Header',
    'userMenuBottomDescription'=>'User Menu Bottom Description',


'advertise'=>'Advertise',
'area'=>'Area',
    'cart'=>'Sales',
    'category'=>'Category',
    'cms'=>'Cms',
    'communication'=>'Communication',
    'company_type'=>'Company Type',
    'contact_us'=>'Contact Us',
    'faq'=>'FAQ',
    'order'=>'Order',
    'product'=>'Product',
    'product_type'=>'Product Type',
'invoice'=>'Invoice',

    'driver'=>'Driver',
    'customer'=>'Customer',
    'vip_customer'=>'VIP Customer',
    'admin'=>"Admin",

    'pendingInvoice'=>'Pending Order',
    'assignedInvoiceDriver'=>'Assigned Order To driver',
    'executedInvoice'=>'Executed Order',
    'rejectInvoice'=>'Rejected Order',
    'confirmedInvoice'=>'Confirmed Order',

    'english'=>'English',
    'arabic'=>' عربي ',
    'changeQuantity'=>'Change Quantity',
    'addOffer'=>'Add Offer',
    'noResult'=>'No Result',

    'active'=>'Active',
    'edit'=>'Edit',
    'create'=>'Create',
    'delete'=>'Delete',
    'deleteConfirmMessage'=>'Are you sure that you want to delete this?',
    'invalidRole'=>' Please login . ',
    'addPermission'=>'Add Permission',
    'addDenyPermission'=>'Add Deny Permission',

    'roleSections'=>['Section','Module','Action','Method'],
    'create'=>'Create',
    'edit'=>'Edit',
    'detail'=>'Detail',
    'details'=>'Details',
    'add'=>'Add',
    'deleted'=>'Deleted successfully',
    'added'=>'Added successfully',
    'updated'=>' updated successfully ',
    'customerReport'=>'Customer Report',
    'driverReport'=>'Driver Report',
    'adminReport'=>'Admin Report',
    'report'=>'Report',
    'subCategory'=>'Sub Category',
    'mainCategory'=>'Main Category',
    'totalNumber'=>'Total Number',
    'totalPrice'=>'Total Price',
    'cartUserReport'=>'User Sales',
    'cartProductReport'=>'Items Sales',
    'print'=>'Print',
    'excel'=>'EXCEL',
    'youCantDelete'=>'you can not delete this because of related elements',
'productReport'=>'Product Report',
];