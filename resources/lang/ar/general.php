<?php

return [

    'name'=>' الاسم ',
    'id'=>' الرقم ',
    'search'=>'  بحث ',
    'showing'=>' اظهار ',
    'to'=>' من ',
    'of'=>' إلى ',
    'entries'=>' سجل ',
    'all'=>'All',

    'dashboard'=>' اللوحة الرئيسية ',

    'CopyRights'=>' جميع الحقوق محفوظة لشركة الحلول التقنية المالية ',

    'language'=>' اللغة ',


    'notification'=>' اشعار ',
    'setting'=>' إعدادات ',
    'basic'=>' أساسي ',
    'edit_config'=>' تعديل الإعدادات ',
    'roles'=>' الصلاحيات ',
    'rolesCreate'=>' إنشاء صلاحية ',
    'created_at'=>' تاريخ الإنشاء ',
    'updated_at'=>' تاريخ التعديل ',
    'logout'=>' تسجيل خروج ',

    'authentication'=>' التوثيق ',
    'authenticationList'=>'Authentication List',
    'authenticationCreate'=>'Create Authentication ',
    'authenticationMenuBottomHeader'=>'AuthenticationMenu Bottom Header',
    'authenticationMenuBottomDescription'=>'Authentication Menu Bottom Description',



    'layout'=>' القالب ',
    'layoutList'=>'Layout List',
    'layoutCreate'=>'Create Layout ',
    'layoutMenuBottomHeader'=>'LayoutMenu Bottom Header',
    'layoutMenuBottomDescription'=>'Layout Menu Bottom Description',




    'role'=>' الصلاحيات ',
    'roleList'=>' قائمة الصلاحيات ',
    'roleCreate'=>' إنشاء صلاحيات ',
    'roleMenuBottomHeader'=>' ملاحظة الصلاحيات ',
    'roleMenuBottomDescription'=>'Role Menu Bottom Description',


    'user'=>' المستخدمين ',
    'userList'=>' قائمة المستخدمين ',
    'userCreate'=>' انشاء مستخدم ',
    'userMenuBottomHeader'=>'UserMenu Bottom Header',
    'userMenuBottomDescription'=>'User Menu Bottom Description',


    'advertise'=>' الإعلانات ',
    'area'=>' الأماكن ',
    'cart'=>' السلة ',
    'category'=>' الفئات ',
    'cms'=>' محتوى الصفحات ',
    'communication'=>' معلومات الاتصال ',
    'company_type'=>' نوع الشركة ',
    'contact_us'=>' اتصل بنا ',
    'faq'=>' أسئلة عامة ',
    'order'=>' الطلبات ',
    'product'=>' المنتجات ',
    'product_type'=>' أنواع المنتجات ',
    'invoice'=>' الفاتورة ',

    'driver'=>' السائقين ',
    'customer'=>' عميل ',
    'vip_customer'=>' عميل مميز ',
    'admin'=>" مدير ",

    'pendingInvoice'=>' فواتير مقدمة للتأكيد ',
    'assignedInvoiceDriver'=>' الفواتير المسندة للسائقين ',
    'executedInvoice'=>' الفواتير المنجزة ',
    'rejectInvoice'=>' الفواتير المرفوضة ',
    'confirmedInvoice'=>' الفواتير الموافق عليها ',

    'english'=>'English',
    'arabic'=>' عربي ',
    'changeQuantity'=>' تغيير الكميات ',
    'addOffer'=>' إضافة عروض ',
    'noResult'=>' لاتوجد نتائج ',

    'active'=>' نشط ',

    'edit'=>' تعديل ',
    'create'=>' انشاء ',
    'delete'=>' حذف ',

    'deleteConfirmMessage'=>' هل انت متأكد من أنك تريد الحذف ؟ ',
    'invalidRole'=>' الرجاء تسجيل الدخول. ',

    'addPermission'=>' اضافة تصريح ',
    'addDenyPermission'=>' اضافة منع ',

    'roleSections'=>[' القسم ',' الفرع ',' الإجراء ',' الطلب '],

    'create'=>'إضافة',
    'edit'=>'تعديل',
    'detail'=>' التفاصيل ',
    'details'=>' التفاصيل ',
    'add'=>' إضافة ',
    'deleted'=>' تم الحذف بنجاح ',
    'added'=>' تمت الإضافة بنجاح ',
    'updated'=>' تم التعديل بنجاح ',

    'customerReport'=>' تقرير العملاء ',
    'driverReport'=>' تقرير السائقين ',
    'adminReport'=>' تقرير المدراء ',
    'report'=>' التقارير ',
    'subCategory'=>' الفئات الفرعية ',
    'mainCategory'=>' الفئات الرئيسية ',
    'totalNumber'=>' العدد الكلي ',
    'totalPrice'=>' السعر الكلي ',
    'cartUserReport'=>' مبيعات المستخدم ',
    'cartProductReport'=>' مبيعات المنتجات ',

    'print'=>' طباعة ',
    'excel'=>' إكسل ',

    'youCantDelete'=>' لا يمكن الحذف لوجود عناصر مرتبطة به ',
    'productReport'=>' تقرير المنتجات '
];