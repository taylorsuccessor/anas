<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => ' ان حقل '.' :attribute '.' يجب ان يكون تاريخ أكبر من '.' :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => ' حقل تأكيد كلمة السر غير مطابق لحقل كلمة السر ',
    'date'                 =>  ' ان حقل '.' :attribute '.' يجب ان يكون تاريخ صحيح ',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ' ان البريد الإلكتروني يجب ان يكون بريد صحيح ',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ' ان حقل '.' :attribute '.' يجب ان يكون رقما ',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => ' ان حقل '.'   :attribute '.' مطلوب ',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => ' إن قيمة حقل '.' :attribute '.' محجوزة مسبقاً. ',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'phone'=> ' رقم الهاتف ',
        'user_name'=>' اسم المستخدم  ',
        'mobile'=>' الموبايل ',
        'title'=>' العنوان ',
        'message'=>' الرسالة ',

        'name_en'=>'  الاسم الانكليزي ',


        'name_ar'=>'الاسم العربي',


        'active'=>'نشط',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',

        'id'=>'الرقم',






        'active'=>'نشط',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',


        'item_id'=>'رقم المنتج',






        'img'=>'الصورة',


        'description_en'=>'تفاصيل المنتج',


        'description_ar'=>'تفاصيل المنتج العربي',


        'product_type_id'=>'نوع المنتج',


        'category_id'=>'الفئة',


        'barcode'=>'الباركود',


        'quantity'=>'الكمية',


        'price'=>'السعر',


        'offer_price'=>'سعر العرض',


        'offer_type'=>'نوع العرض',


        'special'=>'مميز',


        'active'=>'نشط',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',
        'qr'=>' ك.ر ',




        'parent_id'=>'الفئة الرئيسية',


        'img'=>'الصورة',


        'active'=>'نشط',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',
        'mainCategory'=>' فئة أساسية ',


        'address_en'=>'العنوان',


        'address_ar'=>'العنوان العربي',


        'phone'=>'الرقم',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',




        'title_en'=>'العنوان',


        'title_ar'=>'العنوان العربي',


        'description_en'=>'تفاصيل الاعلان',


        'description_ar'=>'تفاصيل الاعلان العربي',


        'img'=>'الصورة',


        'order'=>'الترتيب',


        'type'=>'النوع',


        'default'=>'مفضل',


        'active'=>'نشط',


        'available_from'=>'متاحة من تاريخ',


        'available_to'=>'متاحة حتى تاريخ',


        'order_id'=>'رقم الطلب',


        'product_id'=>'رقم المنتج',


        'quantity'=>'الكمية',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',



        'question_en'=>'السؤال',


        'question_ar'=>'السؤال العربي',


        'answer_en'=>'الجواب',


        'answer_ar'=>'الجواب العربي',

        'name'=>'الاسم',


        'title'=>'العنوان',


        'type'=>'النوع',


        'status'=>'الحالة',


        'to_field'=>'حقل المرسل إليه',


        'to_email'=>'بريد المرسل اليه',


        'language'=>'اللغة',


        'data'=>'المعلومات',


        'body'=>'محتوى التنبيه',

        'user_id'=>'المستخدم',


        'total'=>'المبلغ',


        'driver_id'=>'السائق',


        'area_id'=>'المنطقة',


        'status'=>'الحالة',

        'email'=>'البريد',


        'guest_email'=>'البريد الاضافي',


        'password'=>'كلمة السر',


        'android_device_id'=>'رقم جهاز الاندرويد',


        'ios_device_id'=>'رقم جهاز آيفون',


        'last_login'=>'اخر دخول',


        'first_name'=>'الاسم الأول',


        'last_name'=>'الاسم الأخير',


        'birth_day'=>'تاريخ الميلاد',


        'avatar'=>'البديل',


        'phone'=>'رقم الهاتف',


        'mobile'=>'رقم الموبايل الإضافي',


        'area_id'=>'المنطقة',


        'country'=>'الدولة',


        'address'=>'العنوان',


        'gender'=>'الجنس',


        'occupation'=>'العمل',


        'type'=>'النوع',


        'session_id'=>'رقم الجلسة',


        'lat'=>'خطوط العرض',


        'long'=>'خطوط الطول',


        'created_at'=>'تاريخ الانشاء',


        'updated_at'=>'تاريخ التعديل',
        'password_confirmation'=>"تأكيد كلمة السر",

        'company_type_id'=>'نوع الشركة',
        'company_type'=>'نوع الشركة',
        'driverMap'=>'خريطة السائقين',

        'status'=>' الحالة ',
        'active'=>'  النشاط ',

    ],

];
