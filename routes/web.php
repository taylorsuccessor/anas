<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




Route::post('files/upload','\App\Http\Controllers\common\files\FilesController@postUpload')->name('common.files.upload');
Route::get('files/file-browser','\App\Http\Controllers\common\files\FilesController@getFileBrowser')->name('common.files.browser');
Route::post('files/upload-ajax','\App\Http\Controllers\common\files\FilesController@postUploadAjax')->name('common.files.uploadAjax');
Route::get('files/file-input-popup','\App\Http\Controllers\common\files\FilesController@getFileInputPopup')->name('common.files.fileInputPopup');



Route::get('policy',function(){
$policy=\App\module\cms\model\Cms::where('slug','=','policy')->first();
return view('policy',['policy'=>$policy]);
})->name('common.policy');


Route::get('/', function () {

if(\Auth::check()){
    return \Redirect('admin/order');
    }else{
    return \Redirect('/admin/login');
    }
});