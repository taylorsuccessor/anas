-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2017 at 08:14 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anas`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE `advertise` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2017-01-01',
  `available_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2019-01-01',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`id`, `title_en`, `title_ar`, `description_en`, `description_ar`, `img`, `order`, `type`, `default`, `active`, `available_from`, `available_to`, `created_at`, `updated_at`) VALUES
(1, 'How.', 'Never.', 'Mock Turtle at last, and managed to swallow a.', 'Don\'t let me hear the words:-- \'I speak severely.', 'Possimus vitae quia tempore sit.', '0', '1', '1', '1', '2020-10-10 08:44:14', '2015-10-27 11:43:14', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(2, 'Gryphon.', 'The poor.', 'Alice, \'when one wasn\'t always growing larger.', 'It\'s the most interesting, and perhaps after all.', 'Adipisci ut magni ut quos.', '3', '0', '1', '1', '2019-04-03 03:29:03', '2015-11-21 17:46:11', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(3, 'Alice.', 'Gryphon..', 'The Hatter shook his head contemptuously. \'I.', 'She was a dispute going on rather better now,\'.', 'Ut expedita quia maiores doloremque.', '0', '1', '1', '0', '2019-09-22 17:49:14', '2015-01-04 20:57:49', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(4, 'Gryphon..', 'When.', 'Dodo, pointing to the rose-tree, she went on.', 'Down, down, down. Would the fall was over. Alice.', 'Porro perspiciatis aut sint quisquam.', '1', '0', '1', '0', '2015-12-02 09:42:48', '2017-10-19 18:43:50', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(5, 'In a.', 'The.', 'I\'d only been the right house, because the.', 'Alice laughed so much contradicted in her own.', 'Enim assumenda libero praesentium quo.', '6', '0', '1', '1', '2020-01-06 01:38:49', '2020-11-29 00:07:37', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(6, '.', 'What.', 'Queen. \'Sentence first--verdict afterwards.\'.', 'AND WASHING--extra.\"\' \'You couldn\'t have wanted.', 'Impedit placeat hic inventore commodi.', '3', '1', '1', '0', '2017-11-11 16:16:24', '2020-08-19 00:04:49', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(7, 'King.', 'You see.', 'He says it kills all the rats and--oh dear!\'.', 'The first question of course had to kneel down.', 'Velit id vitae dolorem sunt commodi.', '7', '1', '0', '1', '2019-09-12 23:03:36', '2015-08-17 07:40:27', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(8, 'WHAT are.', 'Queen.', 'Bill,\' she gave a little bottle that stood near..', 'Alice to herself, \'it would have made a snatch.', 'Autem explicabo commodi voluptatum.', '6', '0', '0', '0', '2019-06-27 03:25:13', '2019-05-01 14:40:25', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(9, 'Alice.', 'I don\'t.', 'Gryphon. \'Do you take me for a moment to be.', 'Alice dear!\' said her sister; \'Why, what a dear.', 'Quia accusamus porro eveniet dolorem.', '9', '1', '1', '0', '2019-10-27 22:30:56', '2020-11-09 01:05:15', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(10, 'Queen..', '.', 'She did not venture to go down--Here, Bill! the.', 'White Rabbit; \'in fact, there\'s nothing written.', 'Minima quo veritatis unde qui ipsum.', '7', '0', '1', '0', '2016-01-03 20:55:33', '2018-04-08 03:25:44', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(11, 'The.', 'Alice.', 'I give you fair warning,\' shouted the Queen.', 'Eaglet, and several other curious creatures..', 'Atque rerum hic quia.', '5', '0', '1', '0', '2020-10-27 23:18:25', '2016-04-10 21:18:26', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(12, 'EVER.', 'Alice\'s.', 'This is the capital of Paris, and Paris is the.', 'Hatter was the matter worse. You MUST have meant.', 'Expedita itaque rerum et ab fugit qui.', '1', '1', '1', '0', '2016-03-29 06:54:05', '2018-03-21 01:05:22', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(13, 'An.', 'The.', 'The hedgehog was engaged in a moment. \'Let\'s go.', 'Hatter. \'You MUST remember,\' remarked the King.', 'Repudiandae est quidem animi maiores.', '8', '1', '1', '1', '2015-05-05 00:14:01', '2019-10-23 03:51:33', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(14, 'I\'ve.', 'I\'d.', 'Alice. \'Why, there they lay on the floor: in.', 'March Hare and his buttons, and turns out his.', 'Voluptas qui omnis et fugiat.', '0', '0', '1', '1', '2017-06-17 20:12:23', '2018-01-19 04:25:40', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(15, 'I can.', 'Mock.', 'King said to herself, (not in a shrill, loud.', 'Classics master, though. He was an immense.', 'Illum tempore soluta repellendus.', '8', '1', '1', '0', '2018-02-18 06:43:30', '2018-03-25 18:25:04', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(16, 'She soon.', '.', 'Gryphon added \'Come, let\'s hear some of YOUR.', 'I might venture to say it any longer than that,\'.', 'At quis ex dolores ut qui in.', '2', '1', '0', '1', '2020-05-03 19:54:31', '2018-07-20 12:36:29', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(17, 'And with.', 'Dinah.', 'ARE you talking to?\' said one of these cakes,\'.', 'I\'m quite tired of being all alone here!\' As she.', 'Et impedit qui facere aut dicta earum.', '2', '0', '0', '1', '2016-02-07 10:19:54', '2018-02-24 01:55:40', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(18, '.', 'Gryphon.', 'CHORUS. \'Wow! wow! wow!\' While the Owl and the.', 'I hate cats and dogs.\' It was all dark overhead;.', 'Et qui rerum error facilis suscipit.', '7', '0', '0', '0', '2016-12-28 00:24:35', '2019-08-31 20:26:24', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(19, 'CAN all.', 'Please.', 'March Hare said to the conclusion that it would.', 'VERY wide, but she had someone to listen to her..', 'A rerum vel facilis qui.', '5', '1', '1', '1', '2019-08-11 16:53:46', '2019-09-23 02:37:34', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(20, 'King..', 'I like.', 'First it marked out a new kind of sob, \'I\'ve.', 'At last the Mouse, in a louder tone. \'ARE you to.', 'Laudantium labore vel veritatis non.', '6', '0', '1', '1', '2019-07-03 18:09:28', '2016-01-24 06:37:49', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(21, 'Cheshire.', 'I.', 'IS that to be a walrus or hippopotamus, but then.', 'And argued each case with MINE,\' said the Mock.', 'Quia ut amet ut aut odit ducimus sit.', '7', '1', '1', '0', '2016-04-17 11:23:50', '2019-06-18 22:23:51', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(22, 'Queen..', 'Nobody.', 'Dormouse. \'Fourteenth of March, I think that.', 'So they got settled down again, the Dodo.', 'Soluta quis provident esse.', '0', '1', '1', '1', '2016-12-16 21:02:32', '2015-07-07 14:41:21', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(23, 'I.', 'White.', 'Alice, and her face brightened up at this.', 'I give you fair warning,\' shouted the Gryphon.', 'Non atque rem praesentium nihil minus.', '1', '0', '0', '1', '2019-06-12 09:52:19', '2017-05-18 19:24:31', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(24, 'I\'m.', 'I can.', 'The soldiers were always getting up and throw.', 'And argued each case with MINE,\' said the Mock.', 'Non enim aliquid rerum ad.', '6', '0', '1', '1', '2016-07-26 02:31:04', '2015-08-11 15:14:31', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(25, 'Cat..', 'Alice.', 'Alice, with a large caterpillar, that was.', 'Alice, and she ran across the field after it.', 'Sequi vero atque quos ratione quo.', '2', '1', '1', '1', '2017-06-26 15:34:53', '2015-05-16 20:31:33', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(26, 'THAT..', 'I to.', 'Canterbury, found it so VERY tired of being.', 'Gryphon, sighing in his throat,\' said the sage.', 'Itaque culpa sit deserunt.', '5', '0', '0', '0', '2017-07-31 16:40:45', '2016-02-05 08:11:26', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(27, 'And so.', 'March.', 'Seven flung down his cheeks, he went on, \'if you.', 'Five, \'and I\'ll tell you his history,\' As they.', 'Ex numquam omnis voluptatem sint eius.', '8', '1', '1', '0', '2018-11-23 10:44:20', '2015-09-19 05:00:44', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(28, 'However.', 'Alice.', 'I could shut up like a sky-rocket!\' \'So you did.', 'On which Seven looked up eagerly, half hoping.', 'Iure laborum placeat ut earum.', '4', '1', '0', '1', '2017-01-07 13:29:51', '2019-03-18 18:39:39', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(29, 'I was, I.', 'Alice to.', 'I don\'t want to stay in here any longer!\' She.', 'Alice could not stand, and she told her sister.', 'Illum illum dolores et iste.', '0', '0', '0', '0', '2015-08-14 09:59:55', '2019-07-16 16:05:17', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(30, 'Exactly.', 'However.', 'And the moral of that is--\"Be what you were.', 'It did so indeed, and much sooner than she had.', 'Voluptas in ut sit aut.', '4', '1', '0', '1', '2017-08-12 21:42:28', '2015-05-01 09:10:33', '2017-12-15 15:23:40', '2017-12-15 15:23:40');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name_en`, `name_ar`, `active`, `created_at`, `updated_at`) VALUES
(1, 'BEST butter,\'.', 'Wonderland.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(2, 'March, I.', 'Next came the.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(3, 'Alice.', 'Gryphon, \'you.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(4, 'CHAPTER IX..', 'Alice laughed.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(5, 'The Mouse.', 'March Hare..', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(6, 'Pigeon. \'I.', 'Alice.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(7, 'I the same.', 'There was a.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(8, 'Queen.', 'Rabbit.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(9, 'SOMEBODY.', 'The miserable.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(10, 'However, on.', 'So they began.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(11, 'She\'ll get me.', 'Dormouse is.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(12, 'YOURS: I.', 'Alice!\' she.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(13, 'Duchess..', 'But do cats.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(14, 'The door led.', 'But there.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(15, 'Queen. \'Can.', 'Queen till.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(16, 'France-- Then.', 'Duchess and.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(17, 'I\'m sure _I_.', 'I\'m a deal.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(18, 'Cheshire Cat.', 'As there.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(19, 'Mouse was.', 'You see the.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(20, 'I could, if I.', 'At last the.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(21, 'But here, to.', 'Queen.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(22, 'Alice, a good.', 'I am in the.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(23, 'They all made.', 'Alice said;.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(24, 'WOULD put.', 'And yet I.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(25, 'Queen.', 'YOU must.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(26, 'Alice.', 'This question.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(27, 'I never.', 'All this time.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(28, 'I\'ll manage.', 'Alice again.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(29, 'Alice was.', 'Queen! The.', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(30, 'Alice. One of.', 'Said he.', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40');

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `name`, `email`, `type`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Thora Gibson Sr.', 'braun.sydnie@yahoo.com', '0', '2', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(2, 'Pauline Adams', 'satterfield.dudley@gmail.com', '3', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(3, 'Prof. Kendall Raynor', 'vicenta77@hoppe.com', '7', '1', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(4, 'Melody Smith', 'bergstrom.seth@denesik.com', '0', '0', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(5, 'Sibyl Mertz', 'windler.ada@gmail.com', '4', '6', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(6, 'Valentin Kreiger', 'ykirlin@hotmail.com', '1', '3', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(7, 'Maryam Sipes', 'smann@goodwin.com', '0', '2', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(8, 'Adell Barrows', 'lyla.kozey@yahoo.com', '0', '2', '2017-12-15 15:23:40', '2017-12-15 15:23:40'),
(9, 'Prof. Providenci Bailey', 'zmante@rowe.biz', '4', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'Francisca Harris DVM', 'yvette00@gmail.com', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, 'Modesta Kunze', 'metz.betty@schiller.com', '4', '7', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'Dr. Jacinto Wolff IV', 'gibson.alena@hills.com', '5', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'Jimmie Green', 'kiehn.henri@yahoo.com', '7', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'Makenzie Koepp DDS', 'qlehner@tromp.biz', '8', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, 'Frederique Ankunding V', 'thora.botsford@hotmail.com', '9', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'Prof. Queenie Pacocha', 'janae19@yahoo.com', '7', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, 'Miss Nellie Boehm', 'alexzander96@gmail.com', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'Dane O\'Hara', 'markus77@hotmail.com', '8', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, 'Willow Hilll PhD', 'gabe.harber@gutmann.org', '2', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'Mr. Enrico Powlowski', 'stiedemann.genevieve@hotmail.com', '8', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, 'Ms. Glenna Predovic', 'jacky.ritchie@yahoo.com', '2', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'Keenan Jones', 'braun.roy@hotmail.com', '7', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'Karley Feest Sr.', 'padberg.tamia@gmail.com', '2', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, 'Ms. Chloe Reichert Jr.', 'donna99@yahoo.com', '4', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'Melody Carter MD', 'heathcote.mireille@douglas.com', '2', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, 'Dr. Cara Tremblay PhD', 'eleonore.white@treutel.com', '9', '7', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, 'Kaitlyn Simonis MD', 'grayce.feest@gmail.com', '7', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'Polly Pfeffer', 'oconnell.nicola@harber.com', '1', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'Lizzie Sipes', 'gladyce.zemlak@bins.com', '1', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'Ludie McClure', 'lulu.johnson@torphy.org', '2', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `order_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, '27', '42', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, '7', '11', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, '50', '22', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, '29', '47', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, '50', '29', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, '25', '30', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, '17', '50', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, '22', '15', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, '12', '14', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, '4', '2', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, '46', '2', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, '10', '7', '6', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, '45', '22', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, '40', '21', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, '28', '32', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, '0', '49', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, '37', '12', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, '29', '28', '6', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, '25', '47', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, '14', '33', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, '7', '41', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, '30', '19', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, '38', '20', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, '45', '17', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, '5', '28', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, '6', '22', '9', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, '9', '31', '8', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, '39', '13', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, '27', '7', '6', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, '50', '40', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_en`, `name_ar`, `parent_id`, `img`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Alice, that.', 'There.', '4', 'Ratione eum et est.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, 'I was a large.', '.', '10', 'Dolor quia ex doloribus itaque expedita.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, 'Will you.', 'I wish.', '24', 'Animi in est nulla at.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, 'WAS a narrow.', 'Alice.', '50', 'Excepturi non dolores a.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, 'Alice. \'Now.', 'What.', '37', 'Sint recusandae ad animi vero praesentium est.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, 'Caterpillar.', 'Why, I.', '3', 'Accusamus incidunt velit ut dicta est distinctio.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, 'Everything is.', 'Mock.', '44', 'Ut voluptas expedita architecto ut nisi.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, 'I\'ve.', 'That.', '2', 'Voluptatibus ducimus quis dolorum corporis.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, 'I\'m perfectly.', 'This did.', '47', 'Rem quia temporibus est nisi ipsa distinctio.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'Footman\'s.', 'So she.', '24', 'Amet quas optio qui expedita ipsum culpa.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, 'Alice in a.', 'I do.', '22', 'Atque quo impedit molestiae autem.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'I\'m quite.', 'His.', '14', 'Necessitatibus cumque et hic eaque nisi optio.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'Alice, and.', 'I can.', '34', 'Aut ullam unde nisi reiciendis odit facere.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'The Dormouse.', 'Queen.', '20', 'Vitae in mollitia eum modi vitae tenetur aut ea.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, 'Latin.', '.', '48', 'Excepturi molestias optio qui ab.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'Alice to.', 'Queen\'s.', '2', 'Eum magnam ut nemo ad.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, 'AND.', 'And in.', '22', 'Saepe cumque animi voluptatibus ab dolorem.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'I? Ah, THAT\'S.', 'When the.', '21', 'Aut nesciunt ut sed quia.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, 'Mary Ann.', 'As she.', '30', 'Ex odio ea blanditiis aspernatur ex aut.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'How are you.', 'Queen:.', '14', 'Autem alias saepe sunt voluptatem ex ut nihil.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, 'I suppose.', 'I should.', '7', 'Vitae aperiam aut magni ut.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'And she\'s.', 'Duchess.', '29', 'Quidem labore odio cupiditate neque totam.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'Alice an.', 'THAT..', '35', 'Sed magni quis deserunt voluptatum dolorem rerum.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, 'The Cat\'s.', 'NOT be.', '37', 'Illo est quos fugiat.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'Prizes!\'.', '.', '8', 'Amet non est accusamus.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, 'She\'ll get me.', 'Hatter.', '31', 'Numquam dolor in perferendis a.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, 'At this the.', 'I almost.', '32', 'Dicta sed aut iusto a consequatur.', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'Alice very.', 'Alice.', '49', 'Maxime ad quia quo.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'Alice could.', 'Elsie.', '44', 'Quam repellendus iusto eos quia et asperiores.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'I had our.', 'I think.', '40', 'Vero ut sequi similique aut nihil.', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE `cms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title_en`, `title_ar`, `body_en`, `body_ar`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'About Us', 'About Us', 'about us page body english', 'about us page body arabic', 'about-us', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `communication`
--

CREATE TABLE `communication` (
  `id` int(10) UNSIGNED NOT NULL,
  `address_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `communication`
--

INSERT INTO `communication` (`id`, `address_en`, `address_ar`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'my address english', 'my address arabic', '0785555734', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_type`
--

CREATE TABLE `company_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_type`
--

INSERT INTO `company_type` (`id`, `name_en`, `name_ar`, `active`, `created_at`, `updated_at`) VALUES
(1, 'national', ' عالمي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, 'national', ' عالمي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, 'national', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, 'arabic', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, ' global', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, 'national', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, 'arabic', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, 'national', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, 'arabic', ' عالمي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'national', ' عالمي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, ' global', ' محلي ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'arabic', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'national', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'national', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, ' global', ' عالمي ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'national', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, ' global', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'arabic', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, ' global', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'arabic', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, ' global', ' عالمي ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'national', ' عالمي ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'arabic', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, ' global', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'arabic', ' وطني ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, ' global', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, ' global', ' وطني ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'arabic', ' محلي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'arabic', ' عالمي ', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'arabic', ' عالمي ', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_name`, `phone`, `title`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Nola Champlin', '(910) 639-2798 x858', 'After a.', 'This seemed to be full of smoke from one of the song. \'What trial is it?\'.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, 'Tad Yost', '1-585-886-6899', 'Rabbit.', 'Duchess, \'chop off her head!\' the Queen shouted at the Caterpillar\'s making.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, 'Rosalinda Mills', '604.817.4698', 'Edgar.', 'Duchess was sitting on a branch of a book,\' thought Alice to find her in an.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, 'Roberto Spinka', '+1 (672) 887-2552', 'EVEN.', 'Hatter, \'you wouldn\'t talk about her pet: \'Dinah\'s our cat. And she\'s such a.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, 'Mr. Monserrat Paucek', '(535) 654-9522', 'It was.', 'Alice quietly said, just as she spoke, but no result seemed to Alice an.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, 'Dr. Cordie Grant PhD', '1-584-351-3158', 'And she.', 'Alice rather unwillingly took the watch and looked at it uneasily, shaking it.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, 'Unique Carroll', '1-729-587-1507 x71723', 'MINE.\'.', 'Do come back in their mouths; and the little passage: and THEN--she found.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, 'Lia Becker', '(505) 921-5884 x839', 'The.', 'I don\'t care which happens!\' She ate a little animal (she couldn\'t guess of.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, 'Myrtle Champlin', '435.961.3476', 'Alice.', 'Queen. \'Can you play croquet?\' The soldiers were silent, and looked very.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'Yvette Sanford', '+1 (509) 949-5603', 'Gryphon.', 'I suppose you\'ll be asleep again before it\'s done.\' \'Once upon a little more.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, 'Ana King Sr.', '1-463-709-9538 x7353', 'I should.', 'Alice thought over all she could remember about ravens and writing-desks.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'Modesta Wilkinson', '686.401.2861', 'They\'re.', 'Alice a little timidly, \'why you are very dull!\' \'You ought to go from here?\'.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'Peter Hayes', '1-871-697-9577', 'She was.', 'I should think you\'ll feel it a violent shake at the other, looking uneasily.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'Ms. Cheyenne Brakus', '1-357-685-9269 x5265', 'Hatter:.', 'Dormouse, who seemed to Alice an excellent opportunity for croqueting one of.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, 'Maye Kautzer', '+1 (734) 457-0713', 'Has.', 'I to do?\' said Alice. \'Why not?\' said the Footman. \'That\'s the most.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'Seth Renner', '+18434398318', 'Alice.', 'March Hare meekly replied. \'Yes, but some crumbs must have prizes.\' \'But who.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, 'Deven Erdman', '608-852-8488 x070', 'Alice.', 'Gryphon, and the poor little Lizard, Bill, was in the other: the Duchess was.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'Prof. Delpha Lind MD', '765-513-0460 x7138', '.', 'I wonder if I like being that person, I\'ll come up: if not, I\'ll stay down.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, 'Abelardo Walker', '645.839.8473', 'Go on!\'.', 'Footman continued in the middle of her age knew the name again!\' \'I won\'t have.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'Ansley Quigley', '649-790-9748', 'I\'m.', 'D,\' she added in a moment. \'Let\'s go on in a great many more than three.\'.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, 'Emilio Torp', '615.682.3261 x9181', 'Do cats.', 'I only knew the meaning of it in large letters. It was all finished, the Owl.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'Dr. Peter Beier PhD', '965.690.8288 x8988', 'Alice.', 'Alice, a little shriek, and went on: \'But why did they live at the Footman\'s.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'Prof. Jeffrey Koch', '(546) 394-9211 x94061', 'I!\' he.', 'I suppose I ought to be listening, so she began thinking over other children.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, 'Amani Balistreri', '1-605-907-1659', 'White.', 'I say again!\' repeated the Pigeon, but in a large kitchen, which was.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'Jermain Cartwright', '486.469.5904 x59186', 'Alice.', 'WAS a narrow escape!\' said Alice, and she trembled till she had somehow fallen.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, 'Brittany Eichmann', '390-407-6109', 'CHAPTER.', 'Cat: \'we\'re all mad here. I\'m mad. You\'re mad.\' \'How do you know the song.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, 'Albert Lakin', '+1.747.661.8760', 'But her.', 'But, now that I\'m perfectly sure I can\'t get out again. Suddenly she came in.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'Marques Luettgen III', '575.849.0139 x2797', 'Father.', 'MINE,\' said the Mouse was speaking, and this time she saw in my kitchen AT.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'Stan Hegmann', '(883) 716-3349 x897', 'King.', 'Caterpillar. Here was another long passage, and the little magic bottle had.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'Prof. Jace Dare', '(365) 924-7666', 'Alice.', 'Fish-Footman was gone, and the executioner ran wildly up and straightening.', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question_en`, `question_ar`, `answer_en`, `answer_ar`, `created_at`, `updated_at`) VALUES
(1, 'Queen. \'Their heads are.', 'White Rabbit.', 'And then, turning to Alice with one of the.', 'Alice\'s shoulder, and it was good manners for.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, 'Now, if you wouldn\'t squeeze.', 'Caterpillar.', 'Involved in this way! Stop this moment, and.', 'So she tucked it away under her arm, with its.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, 'Cat: \'we\'re all mad here..', 'Alice, \'and those.', 'Alice, who was a dead silence instantly, and.', 'Dormouse began in a trembling voice, \'Let us get.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, 'In the very tones of her.', 'Duchess, \'and.', 'YOUR business, Two!\' said Seven. \'Yes, it IS his.', 'M--\' \'Why with an M?\' said Alice. \'I mean what I.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, 'Dormouse followed him: the.', 'Hatter. \'You MUST.', 'Who for such a nice little histories about.', 'Alice; \'all I know all sorts of things--I can\'t.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, 'Alice would not join the.', 'The Duchess took.', 'The Mouse looked at it, and they can\'t prove I.', 'King; \'and don\'t be particular--Here, Bill!.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, 'Turtle. \'Hold your tongue.', 'So she stood.', 'Cat. \'--so long as I do,\' said the King eagerly.', 'And oh, I wish you wouldn\'t squeeze so.\' said.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, 'Hatter. \'Stolen!\' the King.', 'They all made of.', 'I can\'t quite follow it as far down the little.', 'Hatter. \'Stolen!\' the King and Queen of Hearts.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, 'Queen shrieked out. \'Behead.', 'Alice, swallowing.', 'I shan\'t! YOU do it!--That I won\'t.', 'Yet you finished the first verse,\' said the.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'Gryphon went on, \'that.', 'Soup! Soup of the.', 'Alice, \'Have you guessed the riddle yet?\' the.', 'Gryphon repeated impatiently: \'it begins \"I.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, 'WHAT things?\' said the Mock.', 'Alice, who had.', 'It was, no doubt: only Alice did not get dry.', 'Queen will hear you! You see, she came upon a.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'Alice, \'and why it is.', 'Queen, turning.', 'Hush!\' said the Dodo solemnly, rising to its.', 'THESE?\' said the Queen, the royal children, and.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'Alice in a rather offended.', 'Seaography: then.', 'Alice again, in a minute or two she stood still.', 'VERY much out of the way--\' \'THAT generally.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'Queen, tossing her head to.', 'I BEG your.', 'I\'m afraid, sir\' said Alice, (she had grown to.', 'Alice in a whisper.) \'That would be quite as.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, 'March Hare. \'He denies it,\'.', 'Dinah: I think.', 'English, who wanted leaders, and had to ask his.', 'LESS,\' said the Caterpillar. Alice said nothing;.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'Alice; not that she did not.', 'Hatter continued.', 'And she kept on good terms with him, he\'d do.', 'GAVE HER ONE, THEY GAVE HIM TWO--\" why, that.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, 'Alice in a shrill, loud.', 'And yet I wish you.', 'How she longed to change the subject of.', 'Now, if you were INSIDE, you might do very well.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'Queen. \'You make me smaller.', 'Alice to herself.', 'In a little bit of mushroom, and raised herself.', 'But there seemed to quiver all over with fright..', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, 'I\'ve got to?\' (Alice had.', 'I beg your.', 'March Hare was said to Alice, very loudly and.', 'Please, Ma\'am, is this New Zealand or.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'Now, if you cut your finger.', 'HEARTS. Alice was.', 'Alice. \'I\'ve tried the roots of trees, and I\'ve.', 'Alice looked at the window, I only knew the.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, 'So she set to work, and very.', 'Dormouse. \'Write.', 'I can\'t put it right; \'not that it was empty:.', 'However, the Multiplication Table doesn\'t.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'Majesty,\' said the Cat, and.', 'Turtle--we used to.', 'Alice could think of what sort it was).', 'Pigeon. \'I\'m NOT a serpent!\' said Alice in a.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'Majesty,\' he began. \'You\'re.', 'Dodo. Then they.', 'RABBIT\' engraved upon it. She stretched herself.', 'I think that very few little girls in my kitchen.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, 'Mock Turtle, capering wildly.', 'Alice took up the.', 'Hatter, \'I cut some more bread-and-butter--\'.', 'On various pretexts they all crowded together at.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'I\'ve got to the table for.', 'The adventures.', 'Cheshire cat,\' said the Caterpillar. \'Well.', 'HE taught us Drawling, Stretching, and Fainting.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, 'I find a number of bathing.', 'Queen, \'and take.', 'Caterpillar\'s making such VERY short remarks.', 'Has lasted the rest of it in asking riddles that.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, 'King. Here one of the earth..', 'The Duchess took.', 'March Hare said in a confused way, \'Prizes!.', 'I wish I hadn\'t cried so much!\' said Alice.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'And oh, my poor hands, how.', 'ITS.', 'Dormouse is asleep again,\' said the Mock Turtle..', 'I know?\' said Alice, timidly; \'some of the.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'CURTSEYING as you\'re falling.', 'M?\' said Alice..', 'Duchess: \'and the moral of that is--\"Birds of a.', 'However, when they passed too close, and waving.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'He looked at it uneasily.', 'MINE.\' The Queen.', 'Alice. \'Stand up and said, \'That\'s right, Five!.', 'King, looking round the refreshments!\' But there.', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_09_122828_create_advertise_table', 1),
(4, '2017_10_09_122828_create_area_table', 1),
(5, '2017_10_09_122828_create_car_table', 1),
(6, '2017_10_09_122828_create_cart_table', 1),
(7, '2017_10_09_122828_create_category_table', 1),
(8, '2017_10_09_122828_create_cms_table', 1),
(9, '2017_10_09_122828_create_communication_table', 1),
(10, '2017_10_09_122828_create_company_type_table', 1),
(11, '2017_10_09_122828_create_contact_us_table', 1),
(12, '2017_10_09_122828_create_faq_table', 1),
(13, '2017_10_09_122828_create_notification_table', 1),
(14, '2017_10_09_122828_create_order_table', 1),
(15, '2017_10_09_122828_create_product_table', 1),
(16, '2017_10_09_122828_create_product_type_table', 1),
(17, '2017_10_09_122828_create_role_table', 1),
(18, '2017_10_09_122828_create_role_user_table', 1),
(19, '2017_10_09_122828_create_user_table', 1),
(20, '2017_10_09_122828_create_vue_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `name`, `title`, `type`, `status`, `to_field`, `to_email`, `language`, `data`, `body`, `created_at`, `updated_at`) VALUES
(1, 'order_confirmed', 'Order confirmed from admin', 'email', '0', 'user_email', '', 'en', '{{$user_email}}/{{$order->id}}/{{$order->total()}}', 'Order admin confirm = {{$user_email}} - {{$order->id}} - {{$order->total()}}', NULL, NULL),
(2, 'order_assigned', 'Order Assigned', 'email', '0', 'user_email', '', 'en', '{{$user_email}}/{{$order->id}}/{{$order->total()}}', 'Order Assigned = {{$user_email}} - {{$order->id}} - {{$order->total()}}', NULL, NULL),
(3, 'order_executed', 'Order executed', 'email', '0', 'user_email', '', 'en', '{{$user_email}}/{{$order->id}}/{{$order->total()}}', 'Order executed = {{$user_email}} - {{$order->id}} - {{$order->total()}}', NULL, NULL),
(4, 'order_pending', 'User confirm Order', 'email', '0', 'user_email', '', 'en', '{{$user_email}}/{{$order->id}}/{{$order->total()}}', 'pending order = {{$user_email}} - {{$order->id}} - {{$order->total()}}', NULL, NULL),
(5, 'new_user', 'new user', 'email', '0', 'user_email', '', 'en', '{{$user->first_name}}/{{$new_user_email}}/{{$user->last_name}}/{{$user->phone}}', '{{$user->first_name}}\\r\\n<br>\\r\\n{{$new_user_email}}\\r\\n<br>{{$user->last_name}}{{$user->phone}}', NULL, NULL),
(6, 'reset_password', 'title reset Password', 'email', '0', 'user_email', '', 'en', '{{route(\'admin.password.reset\',[$token]) }}/{{$user->email}}', 'my Body notification for (reset_password) with title Title reset_password\r\n<a href=\"{{route(\'admin.password.reset\',[$token]) }}?email={{$user->email}}\">reset password </a>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `driver_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `total`, `driver_id`, `area_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '41', '0', '30', '48', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, '11', '0', '47', '3', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, '39', '1', '43', '48', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, '18', '8', '23', '5', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, '39', '8', '17', '11', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, '33', '8', '27', '3', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, '36', '1', '19', '37', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, '31', '3', '0', '49', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, '0', '6', '14', '10', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, '12', '2', '25', '36', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, '40', '2', '16', '22', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, '6', '1', '48', '42', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, '49', '7', '5', '12', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, '39', '0', '39', '27', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, '13', '4', '42', '19', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, '17', '6', '21', '7', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, '36', '2', '9', '28', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, '29', '3', '44', '44', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, '12', '6', '46', '6', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, '41', '0', '1', '5', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, '17', '0', '33', '24', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, '24', '0', '14', '8', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, '5', '9', '30', '10', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, '8', '9', '16', '14', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, '37', '5', '37', '43', '2', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, '33', '7', '20', '13', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, '9', '6', '26', '33', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, '17', '2', '48', '21', '4', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, '16', '9', '49', '18', '3', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, '32', '7', '16', '40', '5', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `offer_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `offer_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `special` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `item_id`, `name_en`, `name_ar`, `img`, `description_en`, `description_ar`, `product_type_id`, `category_id`, `barcode`, `quantity`, `price`, `offer_price`, `offer_type`, `special`, `active`, `created_at`, `updated_at`) VALUES
(1, '55858375', 'Commodi dolor eos.', 'Praesentium quae.', 'Ut culpa est ex.', 'Mock Turtle. \'No, no! The adventures first,\'.', 'I can\'t show it you myself,\' the Mock Turtle.', '43', '23', '6', '7', '9', '7', '2', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, '43181540', 'Eum recusandae.', 'Sed blanditiis.', 'Quia placeat qui dolorem sit in fuga.', 'Mock Turtle, \'but if they do, why then they\'re a.', 'We must have been changed several times since.', '50', '41', '5', '6', '7', '4', '0', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, '35236452', 'Est ipsum magni sed.', 'Perferendis rem eos.', 'Modi aliquam a consequatur et non sed.', 'By the time they had to fall upon Alice, as she.', 'Alice could see, when she got used to do:-- \'How.', '44', '31', '6', '9', '7', '5', '0', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, '51322629', 'Ad quia eveniet.', 'Ipsa quia nostrum.', 'Numquam quaerat reprehenderit repellat.', 'Good-bye, feet!\' (for when she had never before.', 'Don\'t let him know she liked them best, For this.', '15', '34', '8', '3', '7', '9', '1', '1', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, '31545865', 'Ullam minima est.', 'Nesciunt dolor.', 'Quae earum aut et sed.', 'Dormouse! Turn that Dormouse out of the busy.', 'I\'ll just see what I could shut up like a.', '1', '35', '7', '1', '2', '4', '0', '1', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, '29492481', 'Non tenetur debitis.', 'Architecto illo aut.', 'Aspernatur explicabo quod in libero.', 'March Hare. Visit either you like: they\'re both.', 'Forty-two. ALL PERSONS MORE THAN A MILE HIGH TO.', '31', '31', '1', '5', '1', '1', '1', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, '27047260', 'Expedita adipisci.', 'Et voluptates.', 'Suscipit nulla cumque qui quos sit et.', 'The Dormouse shook itself, and was gone across.', 'HE taught us Drawling, Stretching, and Fainting.', '49', '6', '1', '8', '1', '4', '1', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, '74758737', 'Repellat et.', 'Molestiae.', 'Modi est nesciunt doloremque quis.', 'ARE a simpleton.\' Alice did not feel encouraged.', 'Alice: \'I don\'t think--\' \'Then you may nurse it.', '19', '33', '8', '0', '8', '4', '2', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, '70145336', 'Qui mollitia.', 'Consequuntur esse.', 'Minima quaerat harum et totam quae.', 'CHAPTER X. The Lobster Quadrille is!\' \'No.', 'It\'s the most confusing thing I ever saw one.', '24', '38', '6', '3', '2', '0', '1', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, '11230464', 'Distinctio magnam.', 'Quam ab recusandae.', 'Facilis libero nihil qui illum aliquid.', 'First, she tried hard to whistle to it; but she.', 'Alice, \'I\'ve often seen them so shiny?\' Alice.', '7', '2', '5', '9', '5', '7', '2', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, '74702872', 'Molestiae explicabo.', 'Harum ducimus velit.', 'Nesciunt ut voluptas quia quia omnis.', 'Kings and Queens, and among them Alice.', 'King, \'that saves a world of trouble, you know.', '25', '45', '7', '2', '4', '8', '0', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, '81703033', 'Eos aut occaecati.', 'Amet aut sit qui.', 'Quo et facilis nihil unde.', 'Gryphon, before Alice could hear the Rabbit came.', 'Alice did not at all what had become of it; and.', '28', '21', '5', '8', '4', '9', '1', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, '52645965', 'Labore accusantium.', 'Dolorem ipsum.', 'Magnam et quas dolorum sit.', 'But her sister was reading, but it puzzled her.', 'I used--and I don\'t think,\' Alice went on.', '46', '41', '1', '0', '3', '9', '0', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, '73765688', 'Laboriosam debitis.', 'Velit maxime.', 'Et consequatur sed qui minus assumenda.', 'So Alice began to say when I got up and repeat.', 'THESE?\' said the Hatter, with an air of great.', '36', '16', '6', '8', '9', '3', '0', '1', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, '71806104', 'Et sint repellendus.', 'Ducimus et atque.', 'Sunt ut quo magni corporis dolore.', 'I shan\'t go, at any rate he might answer.', 'Knave. The Knave did so, and giving it something.', '44', '17', '5', '4', '9', '9', '2', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, '41085659', 'Quaerat praesentium.', 'Nihil quia.', 'Quia magnam voluptatem error non eaque.', 'Owl, as a partner!\' cried the Mouse, in a rather.', 'This time there could be beheaded, and that he.', '18', '14', '5', '2', '4', '5', '1', '1', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, '56506511', 'Reprehenderit sed.', 'Repellendus et non.', 'Inventore sed ad eum ratione.', 'Alice; not that she ought to be beheaded!\' \'What.', 'Lobster Quadrille?\' the Gryphon as if nothing.', '22', '19', '5', '3', '2', '6', '1', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, '684877', 'Eos iste et.', 'Quis unde minima.', 'Cupiditate nisi ipsum aut iste nisi.', 'II. The Pool of Tears \'Curiouser and curiouser!\'.', 'Alice, that she wasn\'t a bit hurt, and she.', '15', '20', '6', '7', '9', '2', '1', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, '48907067', 'Quam id.', 'Beatae alias.', 'Sit aut ipsa optio tempore dolorem.', 'Gryphon. \'Well, I shan\'t go, at any rate.', 'Hatter went on, half to itself, \'Oh dear! Oh.', '25', '40', '7', '1', '0', '3', '1', '1', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, '57791077', 'Optio quae sit.', 'Nostrum est.', 'Dolores repellendus autem iure natus.', 'But at any rate: go and live in that poky little.', 'Alice\'s shoulder, and it said in a great hurry.', '47', '22', '7', '1', '9', '7', '2', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, '75598465', 'Odio vero corporis.', 'Asperiores eos sit.', 'Quasi blanditiis quisquam unde ullam.', 'Mary Ann!\' said the Gryphon whispered in reply.', 'Nobody moved. \'Who cares for fish, Game, or any.', '30', '17', '2', '0', '4', '5', '2', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, '65411033', 'Et modi vel et.', 'Tenetur repellat.', 'Esse eos itaque ut repudiandae.', 'Bill! catch hold of anything, but she heard a.', 'So they got settled down in an angry voice--the.', '7', '4', '9', '2', '5', '0', '1', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, '96678466', 'Voluptatum dolore.', 'Sunt voluptates.', 'Molestiae voluptatem in quia sit.', 'Alice kept her waiting!\' Alice felt a violent.', 'Drawling, Stretching, and Fainting in Coils.\'.', '35', '33', '9', '7', '4', '2', '0', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, '89022908', 'Suscipit quidem.', 'Cumque voluptatum.', 'Alias blanditiis debitis aut incidunt.', 'Hatter. \'Stolen!\' the King very decidedly, and.', 'WHAT?\' thought Alice \'without pictures or.', '9', '26', '2', '1', '6', '6', '0', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, '73063383', 'Nulla ullam beatae.', 'Est natus.', 'Eum voluptas in accusamus.', 'I to do?\' said Alice. \'Call it what you were all.', 'SOME change in my kitchen AT ALL. Soup does very.', '15', '37', '3', '2', '6', '6', '2', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, '74462073', 'Ad excepturi.', 'Dignissimos veniam.', 'Ea maiores ad occaecati culpa natus.', 'For some minutes the whole party at once and put.', 'Queen, and Alice, were in custody and under.', '46', '24', '1', '5', '8', '4', '1', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, '83430499', 'Quas quia magni.', 'Quis non sunt sit.', 'Et omnis ullam et quisquam atque.', 'NOT, being made entirely of cardboard.) \'All.', 'I think I must have imitated somebody else\'s.', '34', '13', '5', '1', '7', '6', '0', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, '63357544', 'Accusantium et amet.', 'Sint distinctio.', 'Iure necessitatibus aut adipisci sunt.', 'Gryphon, and the Queen, who was passing at the.', 'I beat him when he finds out who I am! But I\'d.', '35', '49', '8', '8', '0', '3', '1', '0', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, '99163492', 'Et deleniti.', 'Omnis laudantium et.', 'Numquam omnis voluptatem maiores.', 'Alice, very much of a feather flock together.\"\'.', 'Gryphon, sighing in his sleep, \'that \"I breathe.', '23', '28', '8', '4', '2', '5', '0', '0', '0', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, '72397667', 'Qui numquam eos et.', 'Eum in fugit hic.', 'Velit in et quam.', 'The first thing I\'ve got to?\' (Alice had no.', 'I don\'t put my arm round your waist,\' the.', '4', '49', '6', '7', '8', '9', '2', '1', '1', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `name_en`, `name_ar`, `created_at`, `updated_at`) VALUES
(1, ' KG   - Turtle..', ' ????  - How she.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(2, ' KG   - All on a.', ' ??????  - Alice.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, ' one element  - Alice\'s.', ' ????  - Owl and.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, ' one element  - Cat, \'a.', ' ????  - Alice.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, ' one element  - Alice;.', ' ????  - I BEG.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, ' KG   - I should.', ' ????  - Cheshire.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, ' package  - I\'m not.', ' ????  - I can\'t.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, ' package  - Mouse.', ' ??????  - Dormouse.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, ' package  - CHAPTER.', ' ????  - I hadn\'t.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, ' package  - Alice as.', ' ????  - Duchess.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, ' one element  - She had.', ' ??????  - So she.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, ' one element  - Queen.', ' ??????  - TOOK A.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, ' KG   - Mock.', ' ??????  - Some of.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, ' package  - I can.', ' ????  - Alice.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, ' one element  - As she.', ' ??????  - Footman.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, ' one element  - YOUR.', ' ????  - I think.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, ' one element  - There.', ' ??????  - She had.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, ' package  - Alice.', ' ????  - VERY.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, ' one element  - Alice.', ' ??????  - Alice to.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, ' one element  - Mock.', ' ????  - Alice.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, ' one element  - Knave.', ' ????  - Lobster.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, ' one element  - Dormouse.', ' ??????  - Queen.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, ' one element  - THEIR.', ' ??????  - They had.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, ' KG   - Queen\'s.', ' ????  - No.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, ' one element  - Gryphon.', ' ????  - Oh, I.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, ' KG   - I did:.', ' ????  - I give.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, ' package  - Alice.', ' ????  - IN the.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, ' package  - WOULD.', ' ????  - ONE with.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, ' one element  - .', ' ????  - ONE with.', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, ' KG   - This.', ' ????  - I BEG.', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allow_permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deny_permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `slug`, `name`, `allow_permission`, `deny_permission`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '*.*.*.*', '', NULL, NULL),
(2, 'repudiandae-sit-eligendi-est-est-eos', 'Repudiandae sit eligendi est est eos.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(3, 'earum-qui-similique-aspernatur-velit', 'Earum qui similique aspernatur velit.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(4, 'fugit-in-omnis-libero', 'Fugit in omnis libero.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(5, 'vitae-modi-dolore-laborum-error', 'Vitae modi dolore laborum error.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(6, 'qui-magnam-dolores-error-explicabo', 'Qui magnam dolores error explicabo.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(7, 'et-voluptas-et-commodi-rerum-dolorem', 'Et voluptas et commodi rerum dolorem.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(8, 'et-qui-omnis-impedit', 'Et qui omnis impedit.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(9, 'unde-eum-vel-veniam', 'Unde eum vel veniam.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(10, 'fuga-alias-delectus-ea-eos-error-et', 'Fuga alias delectus ea eos error et.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(11, 'nobis-atque-aut-sed-fugiat-ab-illum', 'Nobis atque aut sed fugiat ab illum.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(12, 'dolore-maxime-optio-id-totam', 'Dolore maxime optio id totam.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(13, 'consequatur-et-quia-facilis-quod-nihil', 'Consequatur et quia facilis quod nihil.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(14, 'sed-saepe-pariatur-et-error-ad-cum', 'Sed saepe pariatur et error ad cum.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(15, 'doloribus-ut-quia-explicabo-illum', 'Doloribus ut quia explicabo illum.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(16, 'nam-dolorum-est-vitae', 'Nam dolorum est vitae.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(17, 'aperiam-velit-corrupti-rem-quod-ut', 'Aperiam velit corrupti rem quod ut.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(18, 'ut-fugiat-odit-itaque-non-quasi', 'Ut fugiat odit itaque non quasi.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(19, 'eum-ex-ullam-repellat-quis-laudantium', 'Eum ex ullam repellat quis laudantium.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(20, 'et-dolorum-dolores-aliquam-alias', 'Et dolorum dolores aliquam alias.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(21, 'debitis-ipsa-sint-et-est', 'Debitis ipsa sint et est.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(22, 'ab-quo-aliquid-magnam-ex', 'Ab quo aliquid magnam ex.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(23, 'nostrum-est-iure-earum-nam', 'Nostrum est iure earum nam.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(24, 'laboriosam-sint-est-molestiae-sed', 'Laboriosam sint est molestiae sed.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(25, 'modi-tempore-atque-doloremque-eum-vel', 'Modi tempore atque doloremque eum vel.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(26, 'deserunt-aut-ut-ut-sunt-non', 'Deserunt aut ut ut sunt non.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(27, 'illum-qui-dolorum-adipisci', 'Illum qui dolorum adipisci.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(28, 'est-sapiente-ut-sed-earum-beatae', 'Est sapiente ut sed earum beatae.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(29, 'facilis-sit-laborum-et', 'Facilis sit laborum et.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(30, 'et-eveniet-consequuntur-porro-adipisci', 'Et eveniet consequuntur porro adipisci.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41'),
(31, 'est-magnam-ut-reiciendis-unde', 'Est magnam ut reiciendis unde.', '*.*.*.*', '', '2017-12-15 15:23:41', '2017-12-15 15:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(6, 0),
(3, 6),
(0, 4),
(4, 1),
(1, 9),
(1, 9),
(9, 1),
(4, 7),
(6, 3),
(3, 0),
(6, 3),
(3, 5),
(5, 1),
(4, 8),
(6, 0),
(0, 1),
(1, 3),
(4, 0),
(1, 3),
(2, 7),
(7, 1),
(0, 6),
(0, 3),
(0, 0),
(7, 3),
(5, 8),
(2, 4),
(1, 2),
(1, 4),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guest_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `android_device_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_device_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `guest_email`, `password`, `android_device_id`, `ios_device_id`, `last_login`, `first_name`, `last_name`, `birth_day`, `avatar`, `phone`, `mobile`, `area_id`, `country`, `address`, `gender`, `occupation`, `type`, `company_type_id`, `session_id`, `lat`, `long`, `remember_token`, `token`, `created_at`, `updated_at`) VALUES
(1, 'taylorsuccessor@gmail.com', 'taylorsuccessor@gmail.com', '$2y$10$qlFG1SZ.oGxMSoM4A8TTIuMsilrNsXktmMzS26iz2nHSnIWgq7TJC', '', '', '', 'taylor', 'successor', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', NULL, 'awYeGHcm4SK0nSWTCfOYB4F0ML3cujCtOIFaj5DMArObpQo88GTeCiyRjpEA', NULL, NULL),
(2, 'reginald.corwin@ondricka.com', 'josefina39@gmail.com', '$2y$10$GUR/OUeTe8QcKvAxnPGJ/OjszX/yumpevlw9N4g8enTjw8T1zDgQO', '9f8a237a129468907d80284990d1e95ab7353ef5', 'dbd8a12e89c27e1ff5645c5e7bd6e33aed3efa12', '1978-09-27 04:18:53', 'Zackary Roberts I', 'Mr. Erick Beatty', '1990-06-25', '2', '981.266.7980 x43825', '850.905.8979 x024', '9', '4', 'Voluptatem dicta qui sit nam.', '0', 'Fuga aut.', '8', '40', 'd3ef91ba0245a03c3547349a760d9e35fb82aa84', '31.950640', '35.916435', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(3, 'srogahn@hotmail.com', 'josiane.herzog@gmail.com', '$2y$10$ZwfpMTMAlW6FxBQPExTlnOrVqX3RHV0vROeMN4GtliwXNru0wwZT.', '1506408151a89fc9c18192fcd50bed4e0255aacf', 'bb75bdb119c376bdcc698067ffd7b875922fe1f5', '1985-01-04 09:55:56', 'Armani Zieme III', 'Miss Nelda Schultz', '1994-07-10', '3', '+1-615-995-6607', '1-924-392-1802 x2917', '6', '2', 'A quia ea voluptas eum consequuntur.', '0', 'Eum sit.', '10', '20', '2b2040b188e0cd37c15c111a230975dc1cc41e3b', '31.967243', '35.880043', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(4, 'luettgen.jordyn@hotmail.com', 'vokuneva@abernathy.com', '$2y$10$Nh6CQ6PT0z6W5gtYOdB5oOvG/eonLWWGuByi8CMbCJ9xLfvDYWo9S', 'e70fc306d3bf6e87903e9aa26a3d8051d3489a22', '17c40b4a3897d93cd3b6b7c08b8dbff5daa09a89', '1978-09-21 20:32:39', 'Haley Schmitt', 'Prof. Durward Corwin V', '2012-01-17', '4', '842-819-7271', '787-661-2681 x497', '9', '4', 'Nihil commodi cumque voluptatem.', '0', 'Et aut.', '3', '2', 'affc991890903a8e5cbab7aea60b2076caa1ad22', '31.944231', '35.903389', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(5, 'conroy.jerrell@bogan.net', 'theo.jaskolski@padberg.com', '$2y$10$aXfTuwK3nwPDCCSKxEIKEOV2.lSMe0e3RVC64Ryzx1/qFvRHVB9Oq', 'd8b9d4227e90f3ccba2792f980f5fd414abb9251', 'a3e23737cdb9b47bab6569691251bc113c098c28', '2007-01-18 18:35:09', 'Sabryna McLaughlin', 'Eden Kemmer II', '2000-08-27', '5', '318-695-7083', '(254) 232-7025', '9', '2', 'Ea at deleniti debitis est sed molestiae esse.', '0', 'Adipisci.', '5', '18', '4af3c2471b70482400969f504aa8c1ec58ec0356', '31.969573', '35.910599', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(6, 'heloise.lebsack@schultz.com', 'albina03@denesik.biz', '$2y$10$mQ5UEQnRZh1Y8Qdwi0VWDOEavU9XlsjlNFig6dlIOdFA3jLLT6VQK', 'b714bb1517979c8f318e2c7a5fbe076393537886', '35d7f8c0ce2dcf1b0435a8a26d37efd6b8b93704', '2006-04-25 00:36:53', 'Ted Vandervort', 'Aletha Conn', '1983-03-12', '8', '+1-485-836-4864', '+1-483-408-9212', '0', '3', 'Corporis accusamus eum numquam quas ipsam.', '0', 'Et.', '0', '15', '98fd7230d7b19e3cc19d67d90ca8044895deffaf', '31.952971', '35.915062', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(7, 'ofritsch@gmail.com', 'krystel25@ziemann.info', '$2y$10$a/C4f1DNWFlkqVYL8g7VSei7xlBMvThrYt/gemmi8JZphgAhX7PZO', '0f904b9928f24ef4dee37ab6a89ad2f54e430a48', '2a468a9124b6679b9424fd3b96b8e7894d6c9f7f', '2002-06-08 22:39:31', 'Jacey Larkin', 'Miss Janis Yundt Sr.', '2001-07-25', '0', '457-242-0964', '990-765-0705', '3', '3', 'Et nihil fuga officiis.', '0', 'Officia.', '2', '1', '13db69adbe11c8a7603f8655dfe1938b7b04c56a', '31.969573', '35.910599', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(8, 'schamberger.wade@kreiger.org', 'runte.avery@yahoo.com', '$2y$10$e1kA3JPeGxDgt52yO4wju.mn2e/lOnIsnpG6IM1kjp1Fr6AWRQS8y', '534a8661b8dd36aec96e672ecb2db9b17b472006', 'ccf9f5511a0d2c09b56532b0fb913f54be7a85a6', '1971-03-08 07:42:40', 'Lucy Hettinger', 'Edgar Cremin', '1987-12-11', '2', '327-578-8761', '303.271.7725', '0', '7', 'Deleniti nesciunt quas amet et inventore.', '1', 'Dolore.', '0', '23', 'e31b196b3ede6a750efb864a24f972662c9f2edf', '31.962437', '35.879013', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(9, 'cmueller@gmail.com', 'granville.howell@yahoo.com', '$2y$10$R5F.f9DbXzQ4mzILs5ACr.qVeq7iIQN6kBtKQfBVHAjD6P7COi4Ga', '61b3ac40fe93da0563739fee7d62d982db46a7d5', '37c8a455b0104c69eb8cfb1b1ad059512de0c3f0', '1994-04-20 15:47:34', 'Leanna Renner', 'Mr. Wade Spencer', '1977-06-12', '6', '+1 (573) 915-0398', '247-777-6228 x47590', '1', '7', 'Suscipit eius quo quaerat repellat deleniti.', '0', 'Amet est.', '10', '10', '894371152be73240d088ccafb111f90e168bd68a', '31.950640', '35.916435', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(10, 'robin71@gmail.com', 'bauch.tessie@hotmail.com', '$2y$10$cQ.UvtjVpn6QqbQWIQlE5eTbk.uz4pbKhAjZrinpar1j.X3eCQRqO', '3426c9dc06e5a6e3cf0dee03cb74146c0ad0dfef', '4abc3094d85ecfe09163021953a25ab943408045', '2005-04-25 05:41:44', 'Mr. Mitchel Schowalter', 'Ward Ankunding', '1978-05-05', '2', '331-489-0538', '1-546-248-0008 x509', '3', '5', 'Ipsam est omnis iste est quae tenetur.', '1', 'Dolor.', '4', '25', 'c602d3cb17d539b678d995f51d14dd94f3402157', '31.961418', '35.904762', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(11, 'barton.lillie@treutel.com', 'psimonis@yahoo.com', '$2y$10$GmvL3CltX9ho4/ytT4bqpOV9Ee0nVgBCUce7en7pf/4sE8chENdA.', '95c46e1e702214d13d831dc21bd0c58de0e214a0', 'ef1812cf09dc2223c488b4f3cd13dc21fdec5357', '1980-03-04 19:19:05', 'Mr. Dorthy Lakin', 'Troy Feest', '2001-12-06', '1', '1-223-939-0205 x2397', '+1-680-844-7865', '0', '9', 'Inventore debitis quibusdam itaque cum aut.', '1', 'Atque.', '8', '3', 'cb732f970691b62d1cb328b1efd29ea5ca2b9b8a', '31.953845', '35.874035', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(12, 'pearlie.bruen@hotmail.com', 'ilueilwitz@stamm.com', '$2y$10$djeFqg1ZXoZP/ozWJ24aquPQE0QiaCSfrSvUtgqrwpexC8qWAdkUm', 'bc2b1332a0f84bf6d39ccee8a879ea13fe8876d2', '5242ab1541f7bee1b6167b19f2328eab7c3f4009', '1977-04-03 00:11:23', 'Queen Kertzmann', 'Wilma Christiansen', '2013-10-20', '5', '690.806.7242', '+1-508-313-5781', '1', '8', 'Odio quas quia libero ea.', '1', 'Aliquid.', '7', '12', '6d9c964e63d56361a685a57d099f2fa6dcd3e1f3', '31.944231', '35.903389', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(13, 'williamson.alison@yahoo.com', 'muhammad38@nolan.com', '$2y$10$2VmXO/SyZ7GoQpS/Y/V.7eDcg/NRZrPYZqokbu.rugBtS7hdIRtVq', 'b57a7a1e21d9258f112b52e0150f6204660267bd', '7d5dd6921202c9487f1ed99e8b2462c50db4e661', '2013-09-12 15:58:25', 'Ms. Melody Wisozk', 'Melvina Tromp', '1981-10-16', '3', '(468) 615-7141 x5681', '(730) 526-1048', '0', '5', 'Nesciunt officia eligendi et harum.', '0', 'Autem.', '9', '6', '3ae74b02e8f0843921bd0516e4dc6764569e2f9d', '31.953845', '35.874035', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(14, 'swintheiser@hotmail.com', 'terence.kertzmann@graham.info', '$2y$10$xIR7aj0990Pgron0uu4uK.zMSiycOmLx9J/OIj2MurQCWCqwnZare', '439689ee94048115d49e2ab9b0dc33e5a284b091', 'a406e0cad0e7606766ee90231b938c9fec71becb', '1996-10-03 06:23:30', 'Gonzalo Schroeder', 'Destiney Marquardt', '2014-05-05', '6', '+1-508-430-0971', '375-349-3055', '1', '0', 'Vero enim voluptates magnam.', '1', 'Est non.', '0', '34', '7e1c184b8cb6af40f4c6c750951e589e525c406c', '31.961418', '35.921928', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(15, 'mylene.nicolas@hane.com', 'wstroman@king.net', '$2y$10$WdKnQVGzEzfBHZM0j3jXruD0RPAJ3WbgjVH3I/ExrDipaQt4bmP26', '4e1cfbb30c1694ec28e6d15428b4c4e5348081e4', '97752f2a96116347f44ff19eb03398b5641abe7d', '1993-06-19 17:43:11', 'Shyann Kreiger', 'Darrel Swaniawski Jr.', '2004-10-29', '7', '485-622-1528 x7992', '(556) 928-1937 x68870', '3', '1', 'Ullam sit ipsam quia error praesentium id.', '0', 'Incidunt.', '0', '35', 'ec50d50b30cb792d9112d369086cd8688ef31021', '31.944231', '35.903389', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(16, 'ipouros@rau.com', 'myah.rowe@hotmail.com', '$2y$10$dg5IpGCHlZTzAssgOV.DMu6xCFazBJd34mg/gQG58Z8BbgGVTPF.a', '71eeb2f675fbfcd171d308ff128603d0093b3f93', '3bff46ded3135a2f1f29d96b14a59818ea673cd5', '1977-02-15 02:34:33', 'Pat Runte III', 'Jeremie Labadie Sr.', '1978-02-08', '2', '(804) 668-8053 x91350', '278.684.9973 x566', '4', '6', 'Eius ut molestiae est ratione.', '1', 'Sunt.', '1', '3', '5fa665af42ccb8cb9adf6d4437bfa1b7d98230ab', '31.969573', '35.910599', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(17, 'kurtis.bednar@yahoo.com', 'berry23@yahoo.com', '$2y$10$kCCPAI.dJ2cqR.RN/cfx6.9eE1p8jMySgu5x5.jyGGQ7Ug5r481BG', '9376f5795164fd0d447495bafbb4bf3d21ee3cd1', '3c31aaeb17dcd90b982299077fa8181c04dc5079', '1988-02-20 10:11:53', 'Zella Macejkovic', 'Shanie Heller Jr.', '1974-07-19', '8', '446.233.9347', '(704) 446-7273', '5', '3', 'Mollitia exercitationem nemo sunt atque.', '1', 'Enim.', '6', '39', '026c9a55b01f90622737ee9b8ac43ee1e6237e21', '31.942192', '35.900986', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(18, 'lyda.shields@konopelski.com', 'gmarvin@yahoo.com', '$2y$10$XHA8HjHVGbRp51tmYmf4Z.aTzGOpjcjSUpVsDrSCBMD43r.SwjLS6', 'ec23d403826bee5694de1c3da746248025b61677', 'ea5d99cc5154652e31c59502506113d070c037d1', '1985-09-05 04:06:50', 'Bernhard Pfeffer', 'Kris Mertz', '1992-01-17', '3', '794.440.3201 x0378', '1-459-814-2964 x44501', '0', '2', 'Eum totam necessitatibus incidunt.', '0', 'Adipisci.', '9', '22', '83c8b0174c2d834886ec8683ac1631740fe2c643', '31.970156', '35.874550', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(19, 'isaias.hackett@crooks.com', 'gpacocha@gmail.com', '$2y$10$4ObfMWv.WiScsoSNkasHI.B12IngVuwWaqtOhYFXP49Icw6F02Nsi', '203b4acf50e29f62ea37812bc66d72b99d544a9b', '07d8f3d4b42ea87dbf3bb012574ec76ff1941fff', '1981-04-10 02:24:13', 'Scotty Maggio IV', 'Friedrich Altenwerth', '1978-12-15', '0', '(395) 739-1210 x79599', '(997) 836-8127 x249', '0', '3', 'Ullam reprehenderit ut quia nihil amet.', '1', 'Quas.', '9', '8', '619bdb7dae7d58450f0adbe714a72c7f27a35709', '31.970156', '35.874550', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(20, 'keaton.ziemann@yahoo.com', 'xavier.gutmann@schaden.biz', '$2y$10$C.JC3Z6vYGVT697WZrzid.HP.0NzBqh/x.bmW445/4aw038.2101.', '181b6dd951038a27f3228a7992110ed8f82eaa51', 'b5dc05af408afe2fcf1f6a9a58efbf80dfa7aa60', '2007-08-19 10:28:12', 'Prof. Tobin Wyman DDS', 'Damaris Cormier', '1999-12-01', '4', '(318) 480-1595', '630.924.9864', '1', '3', 'Perferendis sit odit sit occaecati.', '0', 'Tenetur.', '9', '19', 'f2957e4e358eb5e84852a3c60c270cee9ac55143', '31.942192', '35.900986', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(21, 'alva07@schmeler.com', 'laura.considine@keeling.net', '$2y$10$KvvSXtk1W/Ujs6R8cYnRROnYa8XvFoW19EYR3.TNBLHc/l3/Is0F6', 'ba0b3cad54e6e08bc47c942749ce12c6e9f7b1b2', '70a5fea705862b40853991a404445abd3286acbe', '2005-03-31 16:41:30', 'Earnest Senger DVM', 'Clair Schuster', '1989-11-09', '2', '645-722-7761 x4054', '367.347.5237 x977', '7', '5', 'Sed tempore quasi ad quae est laudantium.', '1', 'Nam sint.', '5', '33', 'b893136351584cd6b346a8d924d82870c07d981f', '31.969573', '35.910599', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(22, 'emerson23@hessel.net', 'kboyer@gmail.com', '$2y$10$SmakDcTQtTOBjH9tYL3GzeuBzMx0Sqs9pUXTmo6Xz0GjazUxN1sRK', '6e65f6eb9c1f081ebb3113c21905daaa8c1f14af', 'b72c821a402bf9fa348f972ec359b02c3e5d7b64', '1988-12-19 15:25:38', 'Mr. Dwight Gulgowski V', 'Keenan Keeling', '2013-04-28', '3', '(357) 771-3305 x49291', '1-386-810-9936', '2', '0', 'Libero qui hic veritatis nam vitae.', '0', 'Tenetur.', '6', '14', '97588aac3ecff78b4243579fcdf3e9a8d8bb329f', '31.953845', '35.874035', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(23, 'zgoodwin@dibbert.net', 'murray.horace@crooks.com', '$2y$10$upzAkFU9N7rJl7KZClz55ebYluLLSJtxUSQjyjqbURAnIb.BhxYCC', '889f6aa32af166a5fe576204a76814f3be62548a', '7b8a0104ee25ad8bc36289e38eb63b612c5bb19f', '2008-03-03 04:22:47', 'Annabelle Ryan', 'Nils Swaniawski PhD', '1970-09-24', '5', '965.641.5440 x8235', '307.709.7269 x7499', '6', '7', 'Ut omnis ratione occaecati alias.', '1', 'Itaque.', '8', '4', '7a8a0aee1dfebb2531f9bd9f8ba7be4b4698f452', '31.962437', '35.879013', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(24, 'shanahan.vivien@gmail.com', 'fvolkman@gmail.com', '$2y$10$UjsGaOhKNG1kf7Z.AsI0q.XOOwQLTzRV4Ha5xvj4yzOILoeRy4hGi', '2ed3de686be3cb90764ed7c147969f53cbf69760', '0e9d679de5575a5712c2f0cefdbf20aed9f1e81f', '1972-01-10 10:12:34', 'Tod Little', 'Ms. Queenie DuBuque', '2007-02-11', '8', '+1-729-642-0310', '787-454-5386 x7004', '2', '0', 'Quod dolorem animi cupiditate minus ut maxime.', '1', 'Qui et.', '7', '25', 'a79706417e94eaf255c400d0d7859daf462cf872', '31.963166', '35.906994', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(25, 'demario19@keebler.com', 'ecollier@hotmail.com', '$2y$10$PcA1w1YrtcnsCOoMFukKXem7iyHRhzvL35ezO3lzAowOqhVxS.oGy', 'b90cf7f61c7c9e003b02abadc6db8185277b9adb', '3fb5b94eeda6dac06f4ce9992a03e870b55c098f', '1988-11-15 00:31:54', 'Orval Upton MD', 'Mrs. Rosanna Kub', '2015-11-05', '2', '360.614.3578 x309', '(403) 200-0292', '7', '3', 'Dicta sit odio qui aut dicta.', '0', 'Dolorem.', '9', '28', 'c3be36dba9fcfd3672640c0484244a0b4898297d', '31.961418', '35.904762', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(26, 'rhianna.reynolds@gmail.com', 'daisy61@hotmail.com', '$2y$10$YczGs4xnm37vpOo2Ik.0fOJzEjlNhQ/mnuF75LcvHaZhkZdsccdBa', '3ebced07ddd2b469726f282ecc5a125d6c92a918', '090d8b6d655466789b1ca765b03c486f1c1b218b', '1987-07-25 10:36:50', 'Orland Considine', 'Dr. Salma Ryan PhD', '1976-07-25', '1', '278-724-2653', '870-915-8117 x347', '0', '3', 'Alias eveniet mollitia aperiam at animi.', '1', 'Suscipit.', '3', '25', 'b01b261b0706d6f882bd89a5c0f5f4e8100222a6', '31.969573', '35.910599', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(27, 'ahaley@hotmail.com', 'marquardt.terry@hotmail.com', '$2y$10$BKGo0XzNCemiYX9pSnyGa.0bsUiHx4MJAOcq0Zke8uiP30qDGgo82', 'f9cdd1987193a846fec88e6ce12be7fe570be1bd', 'fef95cf4b51302fb9a21aa5f244dfb507d8a5f87', '1994-01-10 08:20:50', 'Mr. Porter Morissette V', 'Miss Vernie Boyer', '1988-11-10', '7', '(768) 947-5070 x12196', '(554) 232-3211', '4', '5', 'Nisi nemo odit libero similique ut quibusdam.', '0', 'Labore.', '10', '25', '2f9ee2e0806eaa7f3a9258e285f81691c3d755e0', '31.952971', '35.915062', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(28, 'lora.white@yahoo.com', 'barton24@hotmail.com', '$2y$10$qRQOdrStpo3Nbe4nRrOqiOTAA8WVVSArFCAvUmUpnUHH4q/RouF0.', '169c0ad14004f385d3e0288fb3b867b4073a13a5', '842b8e1cb4afbc202f2f8e549d855d2427bbb1e2', '2013-07-12 03:46:14', 'Prof. Carmel Heathcote Jr.', 'Mona Ritchie DVM', '1979-07-25', '2', '(735) 333-6350', '725-850-9403 x12741', '7', '8', 'Est aut perspiciatis voluptates.', '1', 'Tempora.', '3', '23', 'b465915da55ebc041443c4b2a54defcf3eb9da91', '31.963166', '35.906994', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(29, 'ecormier@yahoo.com', 'zcassin@yahoo.com', '$2y$10$/wl.ewR1TSFf0nd.WxRgluhBdGuy.Bzh96vCPqmV1KF9gpCTTvzf.', 'd47d4bf1de1cce62e52f3d8d67a7625788b52438', '71ab770e1ad73a7fb59186923b5f86d8cc082adb', '1993-01-29 13:15:32', 'Tad Leannon Sr.', 'Allie Hackett', '1984-08-03', '0', '571-436-9699 x4687', '(557) 825-2571 x2214', '5', '7', 'Qui autem nemo laborum et explicabo.', '1', 'In.', '3', '18', '3dea49516db9b35f74fd3b9b077a24e925b0b602', '31.961418', '35.921928', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(30, 'ubins@yahoo.com', 'lyda.powlowski@gmail.com', '$2y$10$Dv58rmyi5mc4P7hZMLWXQ.r2SQd6.m3ExNoSzkQflzDAFEZQnM7/O', 'c023e77188ff0bc1b9af018273fac5f0d780f9c1', 'bae4a0bf7e6cb5161102931b24c1b213b39eddb0', '1988-02-03 17:16:59', 'Vivian Tremblay', 'Opal Waters', '1970-07-16', '0', '1-639-669-7986', '676.241.7730', '4', '9', 'Dolor sunt dolorum inventore harum quibusdam.', '0', 'Rerum.', '1', '33', '74f865e8912228a82251e31c2e20b248efb388ec', '31.950640', '35.916435', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(31, 'franz.harber@yahoo.com', 'reinger.kelsie@windler.com', '$2y$10$UcuKX1QFQZzD.Bey6dV4m.fEzP1Tm3IkuUqIEBiIMRKVmBGpDEofy', 'c159bc9942f98ef1f231b2ab614f95c69a91eca5', 'fe8f7dd4a6daf6bf50842206e7ed0edcd19e925e', '2012-12-25 19:45:11', 'Ms. Selena Douglas', 'Ms. Christelle Bahringer DDS', '2009-12-22', '0', '330-345-6905 x96845', '1-676-509-0221 x372', '3', '6', 'Eum quo laborum rerum debitis ratione tempora.', '0', 'Odit.', '2', '0', '1c554df0b6e17bafa1937fc946203901e7feb1a7', '31.950640', '35.916435', NULL, NULL, '2017-12-15 15:23:44', '2017-12-15 15:23:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vue`
--

CREATE TABLE `vue` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `birth_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1985-01-01',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '777777',
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vue`
--

INSERT INTO `vue` (`id`, `email`, `password`, `name`, `birth_day`, `phone`, `gender`, `created_at`, `updated_at`) VALUES
(1, '2', '9', '3', '5', '5', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(2, '8', '2', '3', '1', '5', '3', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(3, '4', '0', '6', '1', '6', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(4, '6', '6', '1', '5', '7', '9', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(5, '5', '3', '1', '7', '1', '5', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(6, '2', '2', '4', '5', '8', '6', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(7, '3', '8', '6', '6', '8', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(8, '5', '3', '4', '3', '8', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(9, '5', '8', '5', '4', '5', '6', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(10, '1', '1', '5', '0', '9', '9', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(11, '1', '8', '5', '2', '8', '0', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(12, '0', '2', '1', '2', '2', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(13, '7', '3', '8', '1', '5', '4', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(14, '9', '5', '6', '8', '1', '5', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(15, '3', '9', '7', '6', '6', '6', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(16, '7', '7', '3', '8', '8', '2', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(17, '4', '9', '8', '3', '1', '5', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(18, '2', '0', '1', '9', '8', '1', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(19, '2', '0', '1', '0', '0', '7', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(20, '8', '9', '1', '7', '5', '1', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(21, '0', '9', '8', '9', '4', '1', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(22, '0', '8', '1', '0', '9', '7', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(23, '0', '3', '7', '4', '7', '3', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(24, '2', '2', '0', '8', '1', '8', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(25, '9', '0', '9', '3', '9', '7', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(26, '5', '3', '9', '3', '6', '7', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(27, '3', '1', '6', '4', '7', '1', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(28, '2', '2', '7', '0', '7', '3', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(29, '5', '6', '9', '8', '6', '0', '2017-12-15 15:23:44', '2017-12-15 15:23:44'),
(30, '6', '1', '7', '3', '4', '9', '2017-12-15 15:23:44', '2017-12-15 15:23:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise`
--
ALTER TABLE `advertise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms`
--
ALTER TABLE `cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication`
--
ALTER TABLE `communication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_type`
--
ALTER TABLE `company_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vue`
--
ALTER TABLE `vue`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise`
--
ALTER TABLE `advertise`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `cms`
--
ALTER TABLE `cms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication`
--
ALTER TABLE `communication`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_type`
--
ALTER TABLE `company_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vue`
--
ALTER TABLE `vue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
