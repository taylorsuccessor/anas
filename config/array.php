<?php

return [

    'user_type'=>['admin','customer','vip customer','driver'],
        'user_type_admin_index'=>0,
    'user_type_customer_index'=>1,
    'user_type_vip_customer_index'=>2,
    'user_type_driver_index'=>3,

    'user_status'=>['active','not Active'],
    'user_status_active_index'=>0,
    'user_status_not_active_index'=>1,

    'user_type_customer_group'=>[1,2],


    'active'=>['activate','deactivate'],
    'active_activate_index'=>0,
    'active_deactivate_index'=>1,

    'product_offer_type'=>['not offer','offer','vip offer'],
    'product_offer_type_not_offer_index'=>0,
    'product_offer_type_offer_index'=>1,
    'product_offer_type_vip_offer_index'=>2,

    'product_special'=>['no','yes'],
    'product_special_no_index'=>0,
    'product_special_yes_index'=>1,


    'order_status'=>['not confirmed','pending','confirmed','reject','assigned','executed'],
    'order_status_not_confirmed_index'=>0,
    'order_status_pending_index'=>1,
    'order_status_confirmed_index'=>2,
    'order_status_reject_index'=>3,
    'order_status_assigned_index'=>4,
    'order_status_executed_index'=>5,
    'order_status_fixed_total_group'=>[1,2,3,4,5],


    'advertise_type'=>['app open','product page'],
    'advertise_type_app_open_index'=>0,
    'advertise_type_product_page_index'=>1,

    'advertise_default'=>['no','yes'],
    'advertise_default_no_index'=>0,
    'advertise_default_yes_index'=>1,



];