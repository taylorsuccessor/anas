<?php
namespace App\module\product\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\product\model\Product;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/product';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Product::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@item_id',$result->item_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->item_id);


        $browser->click('@searchTabButton');

       $browser->type('@name_en',$result->name_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->name_en);


        $browser->click('@searchTabButton');

       $browser->type('@name_ar',$result->name_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->name_ar);


        $browser->click('@searchTabButton');

       $browser->type('@img',$result->img)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->img);


        $browser->click('@searchTabButton');

       $browser->type('@description_en',$result->description_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->description_en);


        $browser->click('@searchTabButton');

       $browser->type('@description_ar',$result->description_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->description_ar);


        $browser->click('@searchTabButton');

       $browser->type('@product_type_id',$result->product_type_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->product_type_id);


        $browser->click('@searchTabButton');

       $browser->type('@category_id',$result->category_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->category_id);


        $browser->click('@searchTabButton');

       $browser->type('@barcode',$result->barcode)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->barcode);


        $browser->click('@searchTabButton');

       $browser->type('@quantity',$result->quantity)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->quantity);


        $browser->click('@searchTabButton');

       $browser->type('@price',$result->price)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->price);


        $browser->click('@searchTabButton');

       $browser->type('@offer_price',$result->offer_price)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->offer_price);


        $browser->click('@searchTabButton');

       $browser->type('@offer_type',$result->offer_type)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->offer_type);


        $browser->click('@searchTabButton');

       $browser->type('@special',$result->special)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->special);


        $browser->click('@searchTabButton');

       $browser->type('@active',$result->active)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->active);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@item_id"=>"[name=item_id]",

    "@name_en"=>"[name=name_en]",

    "@name_ar"=>"[name=name_ar]",

    "@img"=>"[name=img]",

    "@description_en"=>"[name=description_en]",

    "@description_ar"=>"[name=description_ar]",

    "@product_type_id"=>"[name=product_type_id]",

    "@category_id"=>"[name=category_id]",

    "@barcode"=>"[name=barcode]",

    "@quantity"=>"[name=quantity]",

    "@price"=>"[name=price]",

    "@offer_price"=>"[name=offer_price]",

    "@offer_type"=>"[name=offer_type]",

    "@special"=>"[name=special]",

    "@active"=>"[name=active]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
