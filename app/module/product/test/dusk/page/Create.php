<?php
namespace App\module\product\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/product/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@item_id')
    ->click('@submit')
    ->assertSee('item_id field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@name_en')
    ->click('@submit')
    ->assertSee('name_en field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@name_ar')
    ->click('@submit')
    ->assertSee('name_ar field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@img')
    ->click('@submit')
    ->assertSee('img field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@description_en')
    ->click('@submit')
    ->assertSee('description_en field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@description_ar')
    ->click('@submit')
    ->assertSee('description_ar field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@product_type_id')
    ->click('@submit')
    ->assertSee('product_type_id field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@category_id')
    ->click('@submit')
    ->assertSee('category_id field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@barcode')
    ->click('@submit')
    ->assertSee('barcode field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@quantity')
    ->click('@submit')
    ->assertSee('quantity field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@price')
    ->click('@submit')
    ->assertSee('price field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@offer_price')
    ->click('@submit')
    ->assertSee('offer_price field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->clear('@offer_type')
    ->click('@submit')
    ->assertSee('offer_type field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@active','9')
    
    ->clear('@special')
    ->click('@submit')
    ->assertSee('special field is required');



    $this->browser
        ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
    
    ->clear('@active')
    ->click('@submit')
    ->assertSee('active field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@item_id','9')
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@img','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@product_type_id','9')
            ->type('@category_id','9')
            ->type('@barcode','9')
            ->type('@quantity','9')
            ->type('@price','9')
            ->type('@offer_price','9')
            ->type('@offer_type','9')
            ->type('@special','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@item_id"=>"[name=item_id]",

    "@name_en"=>"[name=name_en]",

    "@name_ar"=>"[name=name_ar]",

    "@img"=>"[name=img]",

    "@description_en"=>"[name=description_en]",

    "@description_ar"=>"[name=description_ar]",

    "@product_type_id"=>"[name=product_type_id]",

    "@category_id"=>"[name=category_id]",

    "@barcode"=>"[name=barcode]",

    "@quantity"=>"[name=quantity]",

    "@price"=>"[name=price]",

    "@offer_price"=>"[name=offer_price]",

    "@offer_type"=>"[name=offer_type]",

    "@special"=>"[name=special]",

    "@active"=>"[name=active]",


            '@submit'=>'[type=submit]'
        ];
    }
}
