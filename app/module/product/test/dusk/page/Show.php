<?php
namespace App\module\product\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\product\model\Product;

class Show extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/product/1';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $result=Product::find(1);

    $browser->assertSee($result->item_id);
    $browser->assertSee($result->name_en);
    $browser->assertSee($result->name_ar);
    $browser->assertSee($result->img);
    $browser->assertSee($result->description_en);
    $browser->assertSee($result->description_ar);
    $browser->assertSee($result->product_type_id);
    $browser->assertSee($result->category_id);
    $browser->assertSee($result->barcode);
    $browser->assertSee($result->quantity);
    $browser->assertSee($result->price);
    $browser->assertSee($result->offer_price);
    $browser->assertSee($result->offer_type);
    $browser->assertSee($result->special);
    $browser->assertSee($result->active);


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
