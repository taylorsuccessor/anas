<?php
namespace App\module\product\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/product/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@item_id')
    ->click('@submit')
    ->assertSee('item_id field is required');


    $this->browser
    ->clear('@name_en')
    ->click('@submit')
    ->assertSee('name_en field is required');


    $this->browser
    ->clear('@name_ar')
    ->click('@submit')
    ->assertSee('name_ar field is required');


    $this->browser
    ->clear('@img')
    ->click('@submit')
    ->assertSee('img field is required');


    $this->browser
    ->clear('@description_en')
    ->click('@submit')
    ->assertSee('description_en field is required');


    $this->browser
    ->clear('@description_ar')
    ->click('@submit')
    ->assertSee('description_ar field is required');


    $this->browser
    ->clear('@product_type_id')
    ->click('@submit')
    ->assertSee('product_type_id field is required');


    $this->browser
    ->clear('@category_id')
    ->click('@submit')
    ->assertSee('category_id field is required');


    $this->browser
    ->clear('@barcode')
    ->click('@submit')
    ->assertSee('barcode field is required');


    $this->browser
    ->clear('@quantity')
    ->click('@submit')
    ->assertSee('quantity field is required');


    $this->browser
    ->clear('@price')
    ->click('@submit')
    ->assertSee('price field is required');


    $this->browser
    ->clear('@offer_price')
    ->click('@submit')
    ->assertSee('offer_price field is required');


    $this->browser
    ->clear('@offer_type')
    ->click('@submit')
    ->assertSee('offer_type field is required');


    $this->browser
    ->clear('@special')
    ->click('@submit')
    ->assertSee('special field is required');


    $this->browser
    ->clear('@active')
    ->click('@submit')
    ->assertSee('active field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@item_id')
    ->type('@item_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@name_en')
    ->type('@name_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@name_ar')
    ->type('@name_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@img')
    ->type('@img','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@description_en')
    ->type('@description_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@description_ar')
    ->type('@description_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@product_type_id')
    ->type('@product_type_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@category_id')
    ->type('@category_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@barcode')
    ->type('@barcode','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@quantity')
    ->type('@quantity','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@price')
    ->type('@price','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@offer_price')
    ->type('@offer_price','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@offer_type')
    ->type('@offer_type','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@special')
    ->type('@special','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@active')
    ->type('@active','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@item_id"=>"[name=item_id]",
             
             "@name_en"=>"[name=name_en]",
             
             "@name_ar"=>"[name=name_ar]",
             
             "@img"=>"[name=img]",
             
             "@description_en"=>"[name=description_en]",
             
             "@description_ar"=>"[name=description_ar]",
             
             "@product_type_id"=>"[name=product_type_id]",
             
             "@category_id"=>"[name=category_id]",
             
             "@barcode"=>"[name=barcode]",
             
             "@quantity"=>"[name=quantity]",
             
             "@price"=>"[name=price]",
             
             "@offer_price"=>"[name=offer_price]",
             
             "@offer_type"=>"[name=offer_type]",
             
             "@special"=>"[name=special]",
             
             "@active"=>"[name=active]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
