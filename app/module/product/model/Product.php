<?php namespace App\module\product\model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
       "id","item_id","name_en","name_ar","img","description_en","description_ar","product_type_id","category_id","barcode","qr","quantity","price","offer_price","offer_type","special","active","created_at","updated_at"    ];
    protected $table='product';

    public $timestamps =true ;

    protected $guarded = [];

    public function product_type(){
    return $this->belongsTo('App\module\product_type\model\ProductType');
    }

    public function category(){
    return $this->belongsTo('App\module\category\model\Category');
    }




    public function cart(){
        return $this->hasMany('App\module\cart\model\Cart');
    }

    public function name(){
        return $this->{'name_'.session('locale')};
    }
    public function description(){
        return $this->{'description_'.session('locale')};
    }

    public function product_type_name(){
        return isset($this->product_type->name_en)? $this->product_type->{'name_'.session('locale')}:'';
    }
    public function category_name(){
        return isset($this->category->id)? $this->category->name():'';
    }


    public function price(){
        return $this->price;
    }

    public function userPrice($user_type =1){
        $price=$this->price;

        if($this->offer_type ==config('array.product_offer_type_not_offer_index')){$price= $this->price; }

        if($this->offer_type ==config('array.product_offer_type_offer_index')){$price= $this->offer_price; }

        if($this->offer_type ==config('array.product_offer_type_vip_offer_index')){


            if($user_type ==config('array.user_type_vip_customer_index')){$price= $this->offer_price; }

        }

        return $price;
    }


    public function offer_type(){
        return array_key_exists($this->offer_type,trans('array.product_offer_type'))? trans('array.product_offer_type')[$this->offer_type]:'';
    }

    public function special(){
        return array_key_exists($this->special,trans('array.product_special'))? trans('array.product_special')[$this->special]:'';
    }

    public function active(){
        return array_key_exists($this->active,trans('array.active'))? trans('array.active')[$this->active]:'';
    }



}
