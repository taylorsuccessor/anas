<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');

    $table->string('item_id')->default(0);

    $table->string('name_en')->nullable();

    $table->string('name_ar')->nullable();

    $table->string('img')->nullable();

    $table->string('description_en')->nullable();

    $table->string('description_ar')->nullable();

    $table->string('product_type_id')->default(0);

    $table->string('category_id')->default(0);

            $table->string('barcode')->nullable();
            $table->string('qr')->nullable();

    $table->string('quantity')->default(1);

    $table->string('price')->default(0);

    $table->string('offer_price')->default(0);

    $table->string('offer_type')->default(0);

    $table->string('special')->default(0);

    $table->string('active')->default(0);


$table->timestamps();
        });

//        Schema::table('product', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('product');
    }
}
