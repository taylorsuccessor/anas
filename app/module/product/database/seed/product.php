<?php 
namespace App\module\product\database\seed;

use Illuminate\Database\Seeder;
use App\module\product\model\Product as mProduct;

class product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mProduct::insert([

[


    "item_id"=>'',
    "name_en"=>'',
    "name_ar"=>'',
    "img"=>'',
    "description_en"=>'',
    "description_ar"=>'',
    "product_type_id"=>'',
    "category_id"=>'',
    "barcode"=>'',
    "quantity"=>'',
    "price"=>'',
    "offer_price"=>'',
    "offer_type"=>'',
    "special"=>'',
    "active"=>'',

],
]);
*/


        factory(mProduct::class,30)->create();
    }
}
