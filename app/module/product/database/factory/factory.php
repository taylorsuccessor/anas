<?php


$factory->define(App\module\product\model\Product::class,function (Faker\Generator $faker){

    return [

            "item_id"=>$faker->randomNumber(8),
    "name_en"=>$faker->text(20),
    "name_ar"=>$faker->text(20),
    "img"=>$faker->text(40),
    "description_en"=>$faker->realText(50),
    "description_ar"=>$faker->realText(50),
    "product_type_id"=>$faker->numberBetween(0,50),
    "category_id"=>$faker->numberBetween(0,50),
    "barcode"=>$faker->randomDigit,
    "quantity"=>$faker->randomDigit,
    "price"=>$faker->randomDigit,
    "offer_price"=>$faker->randomDigit,
    "offer_type"=>$faker->randomElement([0,1,2]),
    "special"=>$faker->randomElement([0,1]),
        "active"=>$faker->randomElement([0,1]),
        "qr"=>$faker->randomDigit,

    ];
});