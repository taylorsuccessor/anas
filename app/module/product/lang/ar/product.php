<?php
return [


    'product'=>'المنتجات',
    'productCreate'=>'انشاء منتج ',
    'addproduct'=>'اضافة منتج',
    'editproduct'=>'تعديل منتج',
    'productInfo'=>'معلومات المنتج',

    'productTableHead'=>'المنتجات',
    'productTableDescription'=>'قائمة المنتجات',


    'id'=>'الرقم',


    'item_id'=>'رقم المنتج',


    'name_en'=>'  الاسم العربي  ',


    'name_ar'=>'الاسم العربي',


    'img'=>'الصورة',


    'description_en'=>'تفاصيل المنتج',


    'description_ar'=>'تفاصيل المنتج العربي',


    'product_type_id'=>'نوع المنتج',


    'category_id'=>'الفئة',


    'barcode'=>'الباركود',


    'quantity'=>'الكمية',


    'price'=>'السعر',


    'offer_price'=>'سعر العرض',


    'offer_type'=>'نوع العرض',


    'special'=>'مميز',


    'active'=>'نشط',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',
'qr'=>' ك.ر ',


];
