<?php
return [


'product'=>'Product',
'productCreate'=>'Create Product ',
    'addproduct'=>'Add Product',
    'editproduct'=>'Edit Product',
    'productInfo'=>'Product Info',

    'productTableHead'=>'Product Table Header',
    'productTableDescription'=>'Product Description',


    'id'=>'Id',


    'item_id'=>'Item Id',


    'name_en'=>'Name En',


    'name_ar'=>'Name Ar',


    'img'=>'Img',


    'description_en'=>'Description En',


    'description_ar'=>'Description Ar',


    'product_type_id'=>'Product Type ',


    'category_id'=>'Category Id',


    'barcode'=>'Barcode',


    'quantity'=>'Quantity',


    'price'=>'Price',


    'offer_price'=>'Offer Price',


    'offer_type'=>'Offer Type',


    'special'=>'Special',


    'active'=>'Active',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',

    'qr'=>'QR',



];
