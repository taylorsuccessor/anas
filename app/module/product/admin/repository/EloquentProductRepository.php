<?php namespace App\module\product\admin\repository;

use Session;
use App\module\product\model\Product;
use App\module\product\admin\Repository\ProductContract;

class EloquentProductRepository implements ProductContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Product();


//if(canAccess('admin.product.allData')) {
//
//}elseif(canAccess('admin.product.groupData')){
//$oResults = $oResults->where('product.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.product.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('product.id', '=' , $data['id']);
        }
        if (isset($data['item_id']) && !empty($data['item_id'])) {
            $oResults = $oResults->where('product.item_id', '=' , $data['item_id']);
        }
        if (isset($data['name']) && !empty($data['name'])) {
        $name=str_replace(' ','%',$data['name']);
            $oResults = $oResults->where(function($query)use($name){
            $query->where('product.name_en', 'like' , '%'.$name.'%');
            $query->orWhere('product.name_ar', 'like' , '%'.$name.'%');
            });
        }
        if (isset($data['name_en']) && !empty($data['name_en'])) {
            $oResults = $oResults->where('product.name_en', 'like' , '%'.$data['name_en'].'%');
        }
        if (isset($data['name_ar']) && !empty($data['name_ar'])) {
            $oResults = $oResults->where('product.name_ar', 'like' , '%'.$data['name_ar'].'%');
        }
        if (isset($data['img']) && !empty($data['img'])) {
            $oResults = $oResults->where('product.img', '=' , $data['img']);
        }
        if (isset($data['description_en']) && !empty($data['description_en'])) {
            $oResults = $oResults->where('product.description_en', '=' , $data['description_en']);
        }
        if (isset($data['description_ar']) && !empty($data['description_ar'])) {
            $oResults = $oResults->where('product.description_ar', '=' , $data['description_ar']);
        }
        if (isset($data['product_type_id']) && !empty($data['product_type_id'])) {
            $oResults = $oResults->where('product.product_type_id', '=' , $data['product_type_id']);
        }
        if (isset($data['category_id']) && !empty($data['category_id'])) {
            $oResults = $oResults->where('product.category_id', '=' , $data['category_id']);
        }
        if (isset($data['barcode']) && !empty($data['barcode'])) {
            $oResults = $oResults->where('product.barcode', '=' , $data['barcode']);
        }
        if (isset($data['quantity']) &&  $data['quantity'] !='') {
            $oResults = $oResults->where('product.quantity', '=' , $data['quantity']);
        }
        if (isset($data['price']) && !empty($data['price'])) {
            $oResults = $oResults->where('product.price', '=' , $data['price']);
        }
        if (isset($data['offer_price']) && !empty($data['offer_price'])) {
            $oResults = $oResults->where('product.offer_price', '=' , $data['offer_price']);
        }
        if (isset($data['offer_type']) && !empty($data['offer_type'])) {

        $offer_type=is_array($data['offer_type'])? $data['offer_type']:[$data['offer_type']];
            $oResults = $oResults->whereIn('product.offer_type',$offer_type);

            if (isset($data['ajaxRequest']) ) {
                $oResults = $oResults->where('product.offer_price', '>' , 0);
            }
        }
        if (isset($data['special']) && !empty($data['special'])) {
            $oResults = $oResults->where('product.special', '=' , $data['special']);
        }
        if (isset($data['active']) && $data['active'] !='') {
            $oResults = $oResults->where('product.active', '=' , $data['active']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('product.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('product.updated_at', '=' , $data['updated_at']);
        }
        if (isset($data['ajaxRequest']) ) {
            $oResults = $oResults->where('product.quantity', '>' , 0);
        }



if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('product.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('product.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }elseif(isset($data['ajaxRequest'])){

             $oResults = $oResults->paginate(config('anas.mobile_max_pagination'));
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Product();
        if (isset($data['category_id']) && !empty($data['category_id'])) {
            $oResults = $oResults->where('product.category_id', '=' , $data['category_id']);
        }
          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name();
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}



    public function exportExcel($request,$userType='Users'){

        if(!isset($request->exportExcel)){return false;}

        $request->merge(['getAllRecords'=>1]);
        $oResults=$this->getByFilter($request);

        $excelData=$this->toArray($oResults);


        \Excel::create($userType, function($excel) use($excelData) {

            $excel->sheet('Excel sheet', function($sheet) use($excelData) {

                $sheet->fromArray($excelData);
                $sheet->setOrientation('landscape');

            });

        })->store('html',public_path('excel/exports'))->export(($request->exportExcel =='pdf')?'pdf':'xls');
    }



     public function toArray($oResults){
        $aResults=[[ 'id','name en','name ar','quantity','price','active','type','description arabic','description english','barcode','created at'

]];

        if(count($oResults)){

            foreach($oResults as $oResult){
                $aResults[]=[

                    $oResult->id ,
                    $oResult->name_en,
                    $oResult->name_ar,
                    $oResult->quantity,
                    $oResult->price,
                    $oResult->active(),
                    $oResult->product_type_name(),
                  $oResult->description_ar,
                    $oResult->description_en,
                    $oResult->barcode,
                    $oResult->created_at,
                ];
            }
        }

        return $aResults;
    }


    public function create($data)
    {

        $result = Product::create($data);

        if ($result) {
            Session::flash('flash_message', 'product added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$product = Product::findOrFail($id);

        return $product;
    }

    public function destroy($id)
    {
       $exist= \App\module\cart\model\Cart::where('product_id','=',$id)->get();
if(count($exist)){

    Session::flash('flash_warning', trans('general.youCantDelete'));
    return false;
}
        $result =  Product::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'product deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$product = Product::findOrFail($id);
       $result= $product->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'product updated!');
            return true;
        } else {
            return false;
        }

    }

}
