<?php
namespace App\module\product\admin\request;

use Illuminate\Foundation\Http\FormRequest;
class createRequest extends FormRequest
{
/**
* Determine if the user is authorized to make this request.
*
* @return bool
*/
public function authorize()
{
return true;
}

/**
* Get the validation rules that apply to the request.
*
* @return array
*/
public function rules()
{
return [
    "item_id"=>'required|unique:product',
    "name_en"=>'required',
    "name_ar"=>'required',
    "img"=>'required',
    "description_en"=>'required',
    "description_ar"=>'required',
    "product_type_id"=>'required',
    "category_id"=>'required',
    "barcode"=>'',
    "quantity"=>'required|numeric',
    "price"=>'required|numeric',
    "offer_price"=>'numeric',
    "offer_type"=>'numeric',
    "special"=>'numeric',
    "active"=>'numeric',



];
}
}
