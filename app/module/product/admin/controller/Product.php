<?php

namespace App\module\product\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\product\admin\request\createRequest;
use App\module\product\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\product\model\Product as mProduct;
use App\module\user\model\User as mUser;
use App\module\product\admin\repository\ProductContract as rProduct;


use App\module\product_type\admin\repository\ProductTypeContract as rProductType;
use App\module\category\admin\repository\CategoryContract as rCategory;
use App\module\cart\admin\repository\CartContract as rCart;

class Product extends Controller
{
    private $rProduct;

    public function __construct(rProduct $rProduct)
    {
        $this->rProduct = $rProduct;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request, rCategory $rCategory, rProductType $rProductType)
    {

        $statistic = null;
        $oResults = $this->rProduct->getByFilter($request, $statistic);

        if (!isset($request->ajaxRequest)) {
            $categoryList = $rCategory->getAllList(['sub_category'=>1]);
            $productTypeList = $rProductType->getAllList();
            return view('admin.product::index', compact('oResults', 'request', 'statistic', 'categoryList', 'productTypeList'));
        }


        $aResults = [];

        if (count($oResults)) {
            foreach ($oResults as $oResult) {
                $aResults[] = [

                    'id' => $oResult->id,

                    'item_id' => $oResult->item_id,

                    'name' => $oResult->name(),


                    'img' => $oResult->img,

                    'description' => $oResult->description(),

                    'product_type_id' => $oResult->product_type_id,
                    'product_type' => $oResult->product_type_name(),

                    'category' => $oResult->category_name(),
                    'category_id' => $oResult->category_id,

                    'barcode' => $oResult->barcode,

                    'quantity' => $oResult->quantity,

                    'price' => $oResult->price,

                    'offer_price' => $oResult->offer_price,

                    'offer_type' => $oResult->offer_type(),
                    'offer_type_id' => $oResult->offer_type,

                    'special' => $oResult->special(),
                    'special_id' => $oResult->special_id,

                    'active' => $oResult->active(),
                    'active_id' => $oResult->active_id,

                    'created_at' => $oResult->created_at,

                    'updated_at' => $oResult->updated_at,

                ];
            }
        }

        return new JsonResponse(['data' => $aResults, 'total' => $oResults->total(), 'statistic' => $statistic], 200, [], JSON_UNESCAPED_UNICODE);


    }




 /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function productReport(Request $request, rCategory $rCategory, rProductType $rProductType)
    {

        $statistic = null;
        $oResults = $this->rProduct->getByFilter($request, $statistic);



        $this->rProduct->exportExcel($request,'Product Report');
        if(isset($request->print)){
            return view('admin.product::printExcel',['data'=>$this->rProduct->toArray($this->rProduct->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }


        if (!isset($request->ajaxRequest)) {
            $categoryList = $rCategory->getAllList(['sub_category'=>1]);
            $productTypeList = $rProductType->getAllList();
            return view('admin.product::productReport', compact('oResults', 'request', 'statistic', 'categoryList', 'productTypeList'));
        }




        $aResults = [];

        if (count($oResults)) {
            foreach ($oResults as $oResult) {
                $aResults[] = [

                    'id' => $oResult->id,

                    'item_id' => $oResult->item_id,

                    'name' => $oResult->name(),


                    'img' => $oResult->img,

                    'description' => $oResult->description(),

                    'product_type_id' => $oResult->product_type_id,
                    'product_type' => $oResult->product_type_name(),

                    'category' => $oResult->category_name(),
                    'category_id' => $oResult->category_id,

                    'barcode' => $oResult->barcode,

                    'quantity' => $oResult->quantity,

                    'price' => $oResult->price,

                    'offer_price' => $oResult->offer_price,

                    'offer_type' => $oResult->offer_type(),
                    'offer_type_id' => $oResult->offer_type,

                    'special' => $oResult->special(),
                    'special_id' => $oResult->special_id,

                    'active' => $oResult->active(),
                    'active_id' => $oResult->active_id,

                    'created_at' => $oResult->created_at,

                    'updated_at' => $oResult->updated_at,

                ];
            }
        }

        return new JsonResponse(['data' => $aResults, 'total' => $oResults->total(), 'statistic' => $statistic], 200, [], JSON_UNESCAPED_UNICODE);


    }










    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request, rProductType $rProductType, rCategory $rCategory)
    {

        $productTypeList = $rProductType->getAllList();
        $categoryList = $rCategory->getAllList(['sub_category'=>1]);

        return view('admin.product::create', compact('request', 'productTypeList', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {


        $oResults = $this->rProduct->create($request->all());

        if (!isset($request->ajaxRequest)) {
            return redirect('admin/product');
        }


        return new JsonResponse(['status' => 'success', 'data' => $oResults], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function show($id, Request $request, rCart $rCart)
    {


        $product = $this->rProduct->show($id);


        if (isset($request->ajaxRequest)) {
            return new JsonResponse(['status' => 'success', 'data' => $product], 200, [], JSON_UNESCAPED_UNICODE);
        }


        $request->merge(['product_id' => $id, 'page_name' => 'page']);


        $request->page_name = 'page_cart';
        $oCartResults = $rCart->getByFilter($request);

        return view('admin.product::show', compact('product', 'request', 'oCartResults'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function edit($id, rProductType $rProductType, rCategory $rCategory)
    {


        $product = $this->rProduct->show($id);


        $productTypeList = $rProductType->getAllList();
        $categoryList = $rCategory->getAllList(['sub_category'=>1]);
        return view('admin.product::edit', compact('product', 'productTypeList', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {

        $result = $this->rProduct->update($id, $request);

        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.product.index'));
        }

        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function destroy($id, Request $request)
    {
        $product = $this->rProduct->destroy($id);

        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.product.index'));
        }


        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);
    }


    /**
     * table to change quantity
     *
     * @return void
     */
    public function getChangeQuantity(Request $request)
    {

        $statistic = null;
        $oResults = $this->rProduct->getByFilter($request, $statistic);

        return view('admin.product::changeQuantity', compact('oResults', 'request', 'statistic'));


    }


    /**
     * table to change quantity
     *
     * @return void
     */
    public function postChangeQuantity(Request $request)
    {


        if (isset($request->quantity) && is_array($request->quantity)) {

            foreach ($request->quantity as $id => $quantity) {
                mProduct::where('id', $id)->update(['quantity' => $quantity]);
            }
        }


        if (!isset($request->ajaxRequest)) {
            return Redirect::back();
        }


        return new JsonResponse(['status' => 'success'], 200);


    }

    /**
     * table to change quantity
     *
     * @return void
     */
    public function getAddOffer(Request $request)
    {

        $statistic = null;
        $request->merge(['order'=>'id','sort'=>'asc']);
        $oResults = $this->rProduct->getByFilter($request, $statistic);

        return view('admin.product::addOffer', compact('oResults', 'request', 'statistic'));


    }


    /**
     * table to change quantity
     *
     * @return void
     */
    public function postAddOffer(Request $request)
    {


        if (isset($request->offer_price) && is_array($request->offer_price)) {

            foreach ($request->offer_price as $id => $offer_price) {

                mProduct::where('id', $id)->update(['offer_price' => $offer_price, 'offer_type' => $request->offer_type[$id]]);
                $this->sendMassOffer($id);
            }
        }


        if (!isset($request->ajaxRequest)) {
            return Redirect::back();
        }


        return new JsonResponse(['status' => 'success'], 200);


    }

    public function sendMassOffer($productId)
    {

        $offerTemplate = 'offer';
        $userTypeIndex = config('array.user_type_customer_group');

        $product = mProduct::find($productId);
        if ($product->offer_type == config('array.product_offer_type_not_offer_index')) {
            return false;
        } elseif ($product->offer_type == config('array.product_offer_type_vip_offer_index')) {
            $offerTemplate = 'vip_offer';

            $userTypeIndex = [config('array.user_type_vip_customer_index')];
        }


        $users = mUser::whereIn('type', $userTypeIndex)->get();

        foreach ($users as $user) {

            notification($offerTemplate, ['user' => $user, 'user_email' => $user->email, 'product' => $product]);

        }
    }



}
