<?php
Route::group(['middleware' => ['authorization'],'prefix' => 'admin','as'=>'admin.', 'namespace' => '\App\module\product\admin\controller'], function () {


    Route::get('product/change-quantity','Product@getChangeQuantity')->name('product.changeQuantity');
    Route::post('product/change-quantity','Product@postChangeQuantity');



    Route::get('product/add-offer','Product@getAddOffer')->name('product.addOffer');
    Route::post('product/add-offer','Product@postAddOffer');

    Route::get('product/product-report','Product@productReport')->name('product.productReport');

    Route::resource('product','Product');


});


