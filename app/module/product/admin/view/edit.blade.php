@extends('admin.layout::main')

@section('title', trans('general.product'))
@section('content')


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.product') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.product') }}</a></li>
                        <li class="active">{{ trans('product::product.editproduct') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>



            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('product::product.product') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('product::product.editproduct') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">


                                {!! Form::model($product, [
                                    'method' => 'PATCH',
                                    'url' => ['/admin/product', $product->id],
                                    'class' => 'form-horizontal'
                                ]) !!}







                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('product::product.editproduct') }}</span>

                                        <div class="row">
                                            <div class="imgPreview"><img  id="imgPreview" src="{{$product['img']}}" ></div>
                                        </div>


                                    </div>

                                    <div class="panel-body">





                                        <div class="row">
                                            <div class="form-group {{ $errors->has('item_id') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('item_id', trans('product::product.item_id'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('item_id', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('item_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('name_en') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('name_en', trans('product::product.name_en'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('name_ar', trans('product::product.name_ar'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('img') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('img', trans('product::product.img'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('img', null, ['class' => 'form-control uploadFile']) !!}
                                                    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('description_en') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('description_en', trans('product::product.description_en'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('description_en', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('description_en', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('description_ar') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('description_ar', trans('product::product.description_ar'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('description_ar', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('description_ar', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('product_type_id') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('product_type_id', trans('product::product.product_type_id'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('product_type_id',$productTypeList, null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('product_type_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('category_id', trans('product::product.category_id'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('category_id',$categoryList, null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('barcode') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('barcode', trans('product::product.barcode'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('barcode', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('quantity', trans('product::product.quantity'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('price', trans('product::product.price'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('price', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('special') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('special', trans('product::product.special'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('special',trans('array.product_special'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('special', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row" style="display: none">
                                            <div class="form-group {{ $errors->has('offer_type') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('offer_type', trans('product::product.offer_type'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('offer_type',trans('array.product_offer_type'), 0, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('offer_type', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('offer_price') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('offer_price', trans('product::product.offer_price'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('offer_price', 0, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('offer_price', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('active', trans('product::product.active'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('active',trans('array.active'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                            <div class="form-group {{ $errors->has('qr') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('qr', trans('product::product.qr'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('qr', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('qr', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>








                                            <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                {!! Form::submit(trans('general.edit'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                    </div>
                </div>

            </div>





        </div>
    </div>
@endsection



        @section('script')
            @parent
            <script>
                function disableOfferPrice(selectedValue){

                    if(selectedValue ==0){
                        $('[name=offer_price]').val(0);
                        $('[name=offer_price]').prop('disabled',true);

                    }else{
                        $('[name=offer_price]').prop('disabled',false);}
                }
                $('[name=offer_type]').change(function(){
                    disableOfferPrice($(this).val());
                });

                disableOfferPrice( $('[name=offer_type]').val());
            </script>

@endsection