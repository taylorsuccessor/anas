@extends('admin.layout::main')
@section('title', trans('general.product'))

@section('content')



<?php     $canShow=canAccess('admin.product.show');
    $canEdit=canAccess('admin.product.edit');
    $canDestroy=canAccess('admin.product.destroy');
    $canCreate=canAccess('admin.product.create');


    ?>    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.product') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.product') }}</a></li>
                        <li class="active">{{ trans('general.product') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">



                        @include('admin.layout::partial.messages')

                        <div class=" col-xs-6">
                            <h3 class="box-title m-b-0">{{ trans('product::product.productTableHead') }}</h3>
                            <p class="text-muted m-b-20">{{ trans('product::product.productTableDescription') }}</p>



                        </div>

                             <div class="col-xs-3">

                            <a  href="?exportExcel=1&{{ http_build_query($request->all()) }}"class="btn btn-primary form-control">
                                <i class="fa fa-file-excel-o "></i> EXCEL
                            </a>
                        </div>
                        <div class="col-xs-3">

                            <a   onclick="window.open('?print=1&{{ http_build_query($request->all()) }}','Print','').print()" class="btn btn-primary form-control">
                                <i class="fa fa-print "></i> {{trans('general.print')}}
                            </a>
                        </div>


                        <table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>

                            <thead>
                            <tr>



                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">
                                        {!! th_sort(trans('product::product.item_id'), 'item_id', $oResults) !!}
                                    </th>

                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                    {!! th_sort(trans('general.subCategory'), 'category_id', $oResults) !!}
                                </th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                    {!! th_sort(trans('product::product.name_en'), 'name_en', $oResults) !!}
                                </th>




                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">
                                        {!! th_sort(trans('product::product.quantity'), 'quantity', $oResults) !!}
                                    </th>

                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">
                                    {!! th_sort(trans('product::product.price'), 'price', $oResults) !!}
                                </th>


                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">
                                    {!! th_sort(trans('general.active'), 'active', $oResults) !!}
                                </th>





                            </tr>
                            </thead>
                            <tbody>
                            @if (count($oResults))
                               <?php $i=0; $class=''; ?>                                @foreach($oResults as $oResult)
                            <?php  $class=($i%2==0)? 'gradeA even':'gradeA odd';$i+=1; ?>                                    <tr class='{{ $class }}'>

                                                                                <td>{{ $oResult->item_id }}</td>
                                <td>{{$oResult->category_name()}}</td>

                                                                                <td>{{ $oResult->name() }}</td>





                                                                                <td>{{ $oResult->quantity }}</td>

                                                                                <td>{{ $oResult->price }}</td>

                                                                                <td>{{ $oResult->active() }}</td>




                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if (count($oResults))
                            <div class="row">

                                <div class="col-xs-12 col-sm-6 ">
                                    <span class="text-xs">{{trans('general.showing')}} {{ $oResults->firstItem() }} {{trans('general.to')}} {{ $oResults->lastItem() }} {{trans('general.of')}} {{ $oResults->total() }} {{trans('general.entries')}}</span>
                                </div>


                                <div class="col-xs-12 col-sm-6 ">
                                    {!! str_replace('/?', '?', $oResults->appends($request->all())->render()) !!}
                                </div>
                            </div>
                        @else
                            <div class="noResultDiv" >{{trans('general.noResult')}}</div>

                        @endif
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> {!! trans('general.CopyRights') !!} </footer>
        </div>
        <!-- /#page-wrapper -->
        <!-- .right panel -->
        <div class="right-side-panel">
            <div class="scrollable-right container">
                <!-- .Theme settings -->
                <h3 class="title-heading">{{ trans('general.search') }}</h3>

                {!! Form::model($request,['method'=>'get','id'=>'searchForm', 'class'=>'form-horizontal']) !!}




                

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::text('item_id', null, ['placeholder'=>trans('product::product.item_id'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>

                

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::text('name_en', null, ['placeholder'=>trans('product::product.name_en'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>

                

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::text('name_ar', null, ['placeholder'=>trans('product::product.name_ar'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>

                

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::text('quantity', null, ['placeholder'=>trans('product::product.quantity'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::text('price', null, ['placeholder'=>trans('product::product.price'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::select('category_id',$categoryList, null, ['placeholder'=>trans('product::product.category_id'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::select('product_type_id',$productTypeList, null, ['placeholder'=>trans('product::product.product_type_id'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::select('active',trans('array.active'), null, ['placeholder'=>trans('general.active'),'class'=>'form-control input-sm ']) !!}

                    </div>
                </div>



                


                




                <div class="form-group">
                    <label class="col-md-12"></label>
                    <div class="col-md-12">
                        {!! Form::submit(trans('general.search'), ['class'=>'btn btn-info btn-sm', 'name' => 'search']) !!}
                    </div>
                </div>

                {!! Form::hidden('sort', null) !!}
                {!! Form::hidden('order', null) !!}
                {!! Form::close()!!}
            </div>
        </div>

        @stop
