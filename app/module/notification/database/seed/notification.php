<?php 
namespace App\module\notification\database\seed;

use Illuminate\Database\Seeder;
use App\module\notification\model\Notification as mNotification;

class notification extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /*
     *


`id`, `name`,           `title`,                      `type`, `status`, `to_field`, `to_email`, `language`, `data`, `body`, `created_at`, `updated_at`

(32, 'new_user',        'new user',                   'email', '0', '0', NULL, 'en', NULL, '{{$user->first_name}}\r\n<br>\r\n{{$new_user_email}}\r\n<br>{{$user->last_name}}{{$user->phone}}', '2017-12-14 20:01:38', '2017-12-14 20:14:54'),
(33, 'order_pending',   'User confirm Order',         'email', '0', '0', NULL, 'en', NULL, 'pending order = {{$user_email}} - {{$order->id}} - {{$order->total()}}', '2017-12-14 20:58:31', '2017-12-14 20:58:31'),
(34, 'order_executed',  'Order executed',             'email', '0', '0', NULL, 'en', NULL, 'Order executed = {{$user_email}} - {{$order->id}} - {{$order->total()}}', '2017-12-14 20:59:03', '2017-12-14 20:59:03'),
(35, 'order_assigned',  'Order Assigned',             'email', '0', '0', NULL, 'en', NULL, 'Order assigned= {{$user_email}} - {{$order->id}} - {{$order->total()}} -', '2017-12-14 20:59:49', '2017-12-14 20:59:49'),
(36, 'order_confirmed', 'Order confirmed from admin', 'email', '0', '0', NULL, 'en', NULL, 'Order admin confirm= {{$user_email}} - {{$order->id}} - {{$order->total()}}', '2017-12-14 21:00:32', '2017-12-14 21:00:32'),
(37, 'reset_password',  'reset Password',             'email', '0', '0', NULL, 'en', NULL, 'reset password -\r\n<br>\r\n{{env(\'APP_URL\',\'http://anas.fintolog.com\')}}/admin/password/reset/{{$token}}', '2017-12-14 21:38:15', '2017-12-14 21:55:06');

     */
    public function run()
    {

        mNotification::insert([
            //email
            [


                'name'=>'order_confirmed',
                'title'=>  'Order confirmed from admin',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order admin confirm = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_assigned',
                'title'=>  'Order Assigned',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order Assigned = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_executed',
                'title'=>  'Order executed',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order executed = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_pending',
                'title'=> 'User confirm Order',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=> 'pending order = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],

            [


                'name'=>'new_user',
                'title'=>'new user',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{$user->first_name}}/{{$new_user_email}}/{{$user->last_name}}/{{$user->phone}}',
                'body'=>'{{$user->first_name}}\r\n<br>\r\n{{$new_user_email}}\r\n<br>{{$user->last_name}}{{$user->phone}}'
            ],

            [


                'name'=>'reset_password',
                'title'=>'title reset Password',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{route(\'admin.password.reset\',[$token]) }}/{{$user->email}}',
                'body'=>'my Body notification for (reset_password) with title Title reset_password
<a href="{{route(\'admin.password.reset\',[$token]) }}?email={{$user->email}}">reset password </a>'
            ],



            [


                'name'=>'offer',
                'title'=>'New Offer',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{ $product->name_en }}/{{$user->email}}',
                'body'=>'new offer for you now on ({{ $product->name_en }}) for new price ({{ $product->offer_price }}) not ({{ $product->price }})'
            ],
            [


                'name'=>'vip_offer',
                'title'=>'New Vip Offer',
                'type'=>'email',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{ $product->name_en }}/{{$user->email}}',
                'body'=>'new offer for you now on ({{ $product->name_en }}) for new price ({{ $product->offer_price }}) not ({{ $product->price }})'
            ],













////////////////////////////////////sms

            [


                'name'=>'order_confirmed',
                'title'=>  'Order confirmed from admin',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order admin confirm = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_assigned',
                'title'=>  'Order Assigned',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order Assigned = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_executed',
                'title'=>  'Order executed',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=>'Order executed = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],
            [


                'name'=>'order_pending',
                'title'=> 'User confirm Order',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=>  '{{$user_email}}/{{$order->id}}/{{$order->total()}}',
                'body'=> 'pending order = {{$user_email}} - {{$order->id}} - {{$order->total()}}'
            ],

            [


                'name'=>'new_user',
                'title'=>'new user',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{$user->first_name}}/{{$new_user_email}}/{{$user->last_name}}/{{$user->phone}}',
                'body'=>'{{$user->first_name}}\r\n<br>\r\n{{$new_user_email}}\r\n<br>{{$user->last_name}}{{$user->phone}}'
            ],

            [


                'name'=>'reset_password',
                'title'=>'title reset Password',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{route(\'admin.password.reset\',[$token]) }}/{{$user->email}}',
                'body'=>'my Body notification for (reset_password) with title Title reset_password
<a href="{{route(\'admin.password.reset\',[$token]) }}?email={{$user->email}}">reset password </a>'
            ],



            [


                'name'=>'offer',
                'title'=>'New Offer',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{ $product->name_en }}/{{$user->email}}',
                'body'=>'new offer for you now on ({{ $product->name_en }}) for new price ({{ $product->offer_price }}) not ({{ $product->price }})'
            ],
            [


                'name'=>'vip_offer',
                'title'=>'New Vip Offer',
                'type'=>'sms',
                'status'=>0,
                'to_field'=>'user_email',
                'to_email'=>'',
                'language'=>'en',
                'data'=> '{{ $product->name_en }}/{{$user->email}}',
                'body'=>'new offer for you now on ({{ $product->name_en }}) for new price ({{ $product->offer_price }}) not ({{ $product->price }})'
            ],



        ]);

        factory(mNotification::class,30)->create();
    }
}
