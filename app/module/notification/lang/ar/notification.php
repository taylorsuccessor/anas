<?php
return [


    'notification'=>'التنبيهات',
    'notificationCreate'=>'انشاء تنبيه ',
    'addnotification'=>'اضافة تنبيه',
    'editnotification'=>'تعديل تنبيه',
    'notificationInfo'=>'معلومات التنبيه',

    'notificationTableHead'=>'التنبيهات',
    'notificationTableDescription'=>'قائمة التنبيهات',


    'id'=>'الرقم',


    'name'=>'الاسم',


    'title'=>'العنوان',


    'type'=>'النوع',


    'status'=>'الحالة',


    'to_field'=>'حقل المرسل إليه',


    'to_email'=>'بريد المرسل اليه',


    'language'=>'اللغة',


    'data'=>'المعلومات',


    'body'=>'محتوى التنبيه',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
