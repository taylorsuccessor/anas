<?php


$factory->define(App\module\cart\model\Cart::class,function (Faker\Generator $faker){

    return [

            "order_id"=>$faker->numberBetween(0,50),
    "product_id"=>$faker->numberBetween(0,50),
    "quantity"=>$faker->randomDigit,
    "origin_product_price"=>$faker->numberBetween(0,50),

    ];
});