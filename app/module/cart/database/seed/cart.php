<?php 
namespace App\module\cart\database\seed;

use Illuminate\Database\Seeder;
use App\module\cart\model\Cart as mCart;

class cart extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mCart::insert([

[


    "order_id"=>'',
    "product_id"=>'',
    "quantity"=>'',
    "origin_product_price"=>'',

],
]);
*/


        factory(mCart::class,30)->create();
    }
}
