<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');

    $table->string('order_id')->nullable();

    $table->string('product_id')->nullable();

    $table->string('quantity')->default(1);


$table->timestamps();
$table->float('origin_product_price')->default(0);
        });

//        Schema::table('cart', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('cart');
    }
}
