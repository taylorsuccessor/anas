<?php
return [


'cart'=>'Cart',
'cartCreate'=>'Create Order ',
    'addcart'=>'Add Cart',
    'editcart'=>'Edit Cart',
    'cartInfo'=>'Cart Info',

    'cartTableHead'=>'Cart Table Header',
    'cartTableDescription'=>'Cart Description',


    'id'=>'Id',


    'order_id'=>'Order Id',


    'product_id'=>'Product ',


    'quantity'=>'Quantity',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
