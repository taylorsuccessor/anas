<?php
return [


    'cart'=>'السلة',
    'cartCreate'=>'اضافة للسلة',
    'addcart'=>'اضافة للسلة',
    'editcart'=>'تعديل',
    'cartInfo'=>'معلومات',

    'cartTableHead'=>'السلة',
    'cartTableDescription'=>'قائمة المنتجات في السلة',


    'id'=>'الرقم',


    'order_id'=>'رقم الطلب',


    'product_id'=>'اسم المنتج',


    'quantity'=>'الكمية',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
