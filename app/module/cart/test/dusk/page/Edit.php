<?php
namespace App\module\cart\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/cart/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@order_id')
    ->click('@submit')
    ->assertSee('order_id field is required');


    $this->browser
    ->clear('@product_id')
    ->click('@submit')
    ->assertSee('product_id field is required');


    $this->browser
    ->clear('@quantity')
    ->click('@submit')
    ->assertSee('quantity field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@order_id')
    ->type('@order_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@product_id')
    ->type('@product_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@quantity')
    ->type('@quantity','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@order_id"=>"[name=order_id]",
             
             "@product_id"=>"[name=product_id]",
             
             "@quantity"=>"[name=quantity]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
