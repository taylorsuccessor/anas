<?php
namespace App\module\cart\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/cart/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@product_id','9')
            ->type('@quantity','9')
    
    ->clear('@order_id')
    ->click('@submit')
    ->assertSee('order_id field is required');



    $this->browser
        ->type('@order_id','9')
            ->type('@quantity','9')
    
    ->clear('@product_id')
    ->click('@submit')
    ->assertSee('product_id field is required');



    $this->browser
        ->type('@order_id','9')
            ->type('@product_id','9')
    
    ->clear('@quantity')
    ->click('@submit')
    ->assertSee('quantity field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@order_id','9')
            ->type('@product_id','9')
            ->type('@quantity','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@order_id','9')
            ->type('@product_id','9')
            ->type('@quantity','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@order_id','9')
            ->type('@product_id','9')
            ->type('@quantity','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@order_id"=>"[name=order_id]",

    "@product_id"=>"[name=product_id]",

    "@quantity"=>"[name=quantity]",


            '@submit'=>'[type=submit]'
        ];
    }
}
