<?php
namespace App\module\cart\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\cart\model\Cart;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/cart';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Cart::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@order_id',$result->order_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->order_id);


        $browser->click('@searchTabButton');

       $browser->type('@product_id',$result->product_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->product_id);


        $browser->click('@searchTabButton');

       $browser->type('@quantity',$result->quantity)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->quantity);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@order_id"=>"[name=order_id]",

    "@product_id"=>"[name=product_id]",

    "@quantity"=>"[name=quantity]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
