<?php namespace App\module\cart\model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
       "id","order_id","product_id","quantity","created_at","updated_at","origin_product_price"    ];
    protected $table='cart';

    public $timestamps =true ;

    protected $guarded = [];

    public function order(){
    return $this->belongsTo('App\module\order\model\Order');
    }

    public function product(){
    return $this->belongsTo('App\module\product\model\Product');
    }

    public function product_name(){
        return (isset($this->product->name_en))?$this->product->{'name_'.session('locale')}:'';
    }


    public function product_price(){

if($this->origin_product_price > 0 ){return  $this->origin_product_price; }
               $price=0;
        if(!isset($this->order->user)){return $price;}

        $userType=$this->order->user->type;
        if(!count($this->product)){return $price;}

            $price =  $this->product->userPrice($userType);

        return $price;
          }
    public function product_img(){
        return (isset($this->product->id))?$this->product->img:'';
    }

    public function price(){

            $price = $this->quantity * $this->product_price();

        return $price;

    }







}
