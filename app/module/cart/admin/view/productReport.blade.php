@extends('admin.layout::main')
@section('title', trans('general.cart'))

@section('content')



<?php     $canShow=canAccess('admin.cart.show');
    $canEdit=canAccess('admin.cart.edit');
    $canDestroy=canAccess('admin.cart.destroy');
    $canCreate=canAccess('admin.cart.create');


    ?>    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.cart') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.cart') }}</a></li>
                        <li class="active">{{ trans('general.cart') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">



                        @include('admin.layout::partial.messages')

                        <div class=" col-xs-6">
                            <h3 class="box-title m-b-0">{{ trans('cart::cart.cartTableHead') }}</h3>
                            <p class="text-muted m-b-20">{{ trans('cart::cart.cartTableDescription') }}</p>



                        </div>



                        <div class="col-xs-3">

                            <a  href="?exportExcel=1&{{ http_build_query($request->all()) }}"class="btn btn-primary form-control">
                                <i class="fa fa-file-excel-o "></i> EXCEL
                            </a>
                        </div>
                        <div class="col-xs-3">

                            <a   onclick="window.open('?print=1&{{ http_build_query($request->all()) }}','Print','').print()" class="btn btn-primary form-control">
                                <i class="fa fa-print "></i> {{trans('general.print')}}
                            </a>
                        </div>
                        @if(false && $canCreate)
                        <div class="col-xs-3">
                            <a  href="{{route('admin.cart.create')}}"class="btn btn-primary form-control">
                                + {{trans('cart::cart.cartCreate')}}
                            </a>
                        </div>
                        @endif

                        <table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>


                        <thead>


                        <tr>
                            <td colspan="1" >{{trans('product::product.name_en')}}</td>
                            <td colspan="2" >{{$product->name()}}</td>

                            <td colspan="1" >{{trans('product::product.quantity')}}</td>
                            <td colspan="2" >{{$product->quantity}}</td>


                        </tr>
                        <tr>
                            <td colspan="1" >{{trans('product::product.product_type_id')}}</td>
                            <td colspan="2" >{{$product->product_type_name()}}</td>

                            <td colspan="1" >{{trans('product::product.price')}}</td>
                            <td colspan="2" >{{$product->price}}</td>


                        </tr>



                        <tr>
                            <td colspan="3" >{{trans('general.totalNumber')}}</td>
                            <td colspan="3" >{{$statistic['totalNumber']}}</td>
                        </tr>

                        <tr>
                            <td colspan="3" >{{trans('general.totalPrice')}}</td>
                            <td colspan="3" >{{$statistic['totalPrice']}}</td>
                        </tr>

                        <tr>


                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">
                                        {!! th_sort(trans('cart::cart.id'), 'id', $oResults) !!}
                                    </th>

                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">
                                        {!! th_sort(trans('cart::cart.order_id'), 'order_id', $oResults) !!}
                                    </th>

                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                        {!! th_sort(trans('cart::cart.product_id'), 'product_id', $oResults) !!}
                                    </th>
                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                        {!! trans('category::category.category') !!}
                                    </th>                       <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                        {!! trans('product::product.active') !!}
                                    </th>

                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">
                                        {!! th_sort(trans('cart::cart.quantity'), 'quantity', $oResults) !!}
                                    </th>
                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">
                                    {{trans('product::product.price')}}
                                </th>

                                                                    <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">
                                        {!! th_sort(trans('cart::cart.created_at'), 'created_at', $oResults) !!}
                                    </th>



                            </tr>
                            </thead>
                            <tbody>
                            @if (count($oResults))
                               <?php $i=0; $class=''; ?>                                @foreach($oResults as $oResult)
                            <?php  $class=($i%2==0)? 'gradeA even':'gradeA odd';$i+=1; ?>                                    <tr class='{{ $class }}'>

                                                                                <td>{{ $oResult->id }}</td>

                                                                                <td>{{ $oResult->order_id }}</td>

                                                                                <td>{{ $oResult->product_name() }}</td>
                                                                                <td>{{ $oResult->product->category_name() }}</td>
                                                                                <td>{{ $oResult->product->active() }}</td>

                                                                                <td>{{ $oResult->quantity }}</td>

                                <td>{{ $oResult->price }}</td>
                                                                                <td>{{ $oResult->created_at }}</td>



                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if (count($oResults))
                            <div class="row">

                                <div class="col-xs-12 col-sm-6 ">
                                    <span class="text-xs">{{trans('general.showing')}} {{ $oResults->firstItem() }} {{trans('general.to')}} {{ $oResults->lastItem() }} {{trans('general.of')}} {{ $oResults->total() }} {{trans('general.entries')}}</span>
                                </div>


                                <div class="col-xs-12 col-sm-6 ">
                                    {!! str_replace('/?', '?', $oResults->appends($request->all())->render()) !!}
                                </div>
                            </div>
                        @else
                            <div class="noResultDiv" >{{trans('general.noResult')}}</div>

                        @endif
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> {!! trans('general.CopyRights') !!} </footer>
        </div>
        <!-- /#page-wrapper -->
        <!-- .right panel -->
        <div class="right-side-panel">
            <div class="scrollable-right container">
                <!-- .Theme settings -->
                <h3 class="title-heading">{{ trans('general.search') }}</h3>

                {!! Form::model($request,['method'=>'get','id'=>'searchForm', 'class'=>'form-horizontal']) !!}




                

                <div class="form-group">
                    <div class="col-md-12">
                        {!! Form::select('product_id',$productList, null, ['class'=>'form-control input-sm ']) !!}

                    </div>
                </div>


                




                <div class="form-group">
                    <label class="col-md-12"></label>
                    <div class="col-md-12">
                        {!! Form::submit(trans('general.search'), ['class'=>'btn btn-info btn-sm', 'name' => 'search']) !!}
                    </div>
                </div>

                {!! Form::hidden('sort', null) !!}
                {!! Form::hidden('order', null) !!}
                {!! Form::close()!!}
            </div>
        </div>

        @stop
