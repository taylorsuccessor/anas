@extends('admin.layout::main')

@section('title', trans('general.cart'))
@section('content')


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.cart') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.cart') }}</a></li>
                        <li class="active">{{ trans('cart::cart.editcart') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>



            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('cart::cart.cart') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('cart::cart.editcart') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">







                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('cart::cart.editcart') }}</span>







                                    </div>

                                    <div class="panel-body">
<div class="row">

    {!! Form::model($request, [
        'method' => 'GET',
        'url' => '/admin/cart/'. $cart->id.'/edit',
        'class' => 'form-horizontal',
        'style'=>'margin:0px;padding:0px; ',
        'id'=>'selectCategoryForm'

    ]) !!}


    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}  col-xs-6">
        {!! Form::label('category_id', trans('product::product.category_id'), ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-8">
            {!! Form::select('category_id',$categoryList, $selectedCategoryId, ['class' => 'form-control','onchange'=>'$("#selectCategoryForm").submit();']) !!}
            {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    {!! Form::close() !!}

</div>



                                        {!! Form::model($cart, [
                                            'method' => 'PATCH',
                                            'url' => ['/admin/cart', $cart->id],
                                            'class' => 'form-horizontal'
                                        ]) !!}




                                        <div class="row">
                                        <div class="form-group {{ $errors->has('order_id') ? 'has-error' : ''}}  col-xs-6">
                                            {!! Form::label('order_id', trans('cart::cart.order_id'), ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::text('order_id', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                                                {!! $errors->first('order_id', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>


                                            <div class="form-group {{ $errors->has('product_id') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('product_id', trans('cart::cart.product_id'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('product_id',$productList, null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                        </div>                                        
                                        <div class="row">




                                        <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}  col-xs-6">
                                            {!! Form::label('quantity', trans('cart::cart.quantity'), ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                                                {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                                                                



                                        <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                {!! Form::submit(trans('general.edit'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>

                                        {!! Form::close() !!}




                                </div>


                            </div>

                        </div>
                    </div>
                </div>

            </div>





        </div>
    </div>
@endsection