<?php namespace App\module\cart\admin\repository;

use Session;
use App\module\cart\model\Cart;
use App\module\cart\admin\repository\CartContract;
use App\module\product\model\Product as mProduct;
class EloquentCartRepository implements CartContract
{

    public function getByFilter($data, &$statistic = null)
    {

        $oResults = new Cart();


//if(canAccess('admin.cart.allData')) {
//
//}elseif(canAccess('admin.cart.groupData')){
//$oResults = $oResults->where('cart.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.cart.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('cart.id', '=', $data['id']);
        }
        if (isset($data['order_id']) && !empty($data['order_id'])) {
            $oResults = $oResults->where('cart.order_id', '=', $data['order_id']);
        }
        if (isset($data['product_id']) && !empty($data['product_id'])) {
            $oResults = $oResults->where('cart.product_id', '=', $data['product_id']);
        }
        if (isset($data['quantity']) && !empty($data['quantity'])) {
            $oResults = $oResults->where('cart.quantity', '=', $data['quantity']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('cart.created_at', '=', $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('cart.updated_at', '=', $data['updated_at']);
        }
        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $oResults = $oResults->join('order', 'order.id', '=', 'cart.order_id')
                ->where('order.user_id', '=', $data['user_id'])
                ->select(['cart.*']);
        }


        if ($statistic !== null) {

            $oResults = $oResults->join('product', 'product.id', '=', 'cart.product_id')
                ->select(['cart.*', 'product.price']);

            $statistic = $this->getStatistic(clone $oResults);
        }

        if (isset($data['order']) && !empty($data['order']) && !is_numeric($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('cart.' . $data['order'], $sort);
        } else {
            $oResults = $oResults->orderBy('cart.id', 'desc');
        }


        $pagination_size = (isset($data['pagination_size']) && !empty($data['pagination_size'])) ? $data['pagination_size'] : config('anas.pagination_size');

        if (isset($data['getAllRecords']) && !empty($data['getAllRecords'])) {
            $oResults = $oResults->get();
        } elseif (isset($data['page_name']) && !empty($data['page_name'])) {
            $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        } else {
            $oResults = $oResults->paginate($pagination_size);
        }
        return $oResults;
    }

    public function getAllList($data = [])
    {

        $oResults = new Cart();

        $oResults = $oResults->get();

        $aResults = [];

        foreach ($oResults as $result) {
            $aResults[$result->id] = $result->name;
        }
        return $aResults;
    }






    public function exportExcel($request,$userType='Users'){

        if(!isset($request->exportExcel)){return false;}

        $request->merge(['getAllRecords'=>1]);
        $oResults=$this->getByFilter($request);

        $excelData=$this->toArray($oResults);


        \Excel::create($userType, function($excel) use($excelData) {

            $excel->sheet('Excel sheet', function($sheet) use($excelData) {

                $sheet->fromArray($excelData);
                $sheet->setOrientation('landscape');

            });

        })->store('html',public_path('excel/exports'))->export(($request->exportExcel =='pdf')?'pdf':'xls');
    }


    public function toArray($oResults){
        $aResults=[[ trans('cart::cart.id') ,trans('cart::cart.order_id'),trans('cart::cart.product_id'),
        trans('cart::cart.quantity')
        ,trans('product::product.price')
                               ,trans('cart::cart.created_at')

]];

        if(count($oResults)){

            foreach($oResults as $oResult){
                $aResults[]=[

                    $oResult->id ,
                    $oResult->order_id,
                    $oResult->product_name(),
                    $oResult->quantity,
                    $oResult->price(),
                    $oResult->created_at
                ];
            }
        }

        return $aResults;
    }





    public function getStatistic($oResults)
    {
        $oTotalResults = clone $oResults;
        $oTotalPrice = clone $oResults;

        $current_month = gmdate('Y-m');

        $totalResults = $oTotalResults->sum('cart.quantity');
        $totalPrice = $oTotalResults->sum('product.price');
        return ['totalNumber' => $totalResults, 'totalPrice' => $totalPrice];
    }


    public function create($data)
    {

        $result = Cart::create($data);

        $order=$result->order;

        if($order->status > config('array.order_status_not_confirmed_index')){
        $product=mProduct::find($data['product_id']);

        $product->quantity= $product->quantity-$data['quantity'];
        $product->save();
        }
        if ($result) {
            Session::flash('flash_message', trans('general.added'));
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

        $cart = Cart::findOrFail($id);

        return $cart;
    }

    public function destroy($id)
    {




        $cart = Cart::findOrFail($id);


        $order=$cart->order;

        if($order && $order->status > config('array.order_status_not_confirmed_index')){
        $product=mProduct::find($cart->product_id);

        $product->quantity= $product->quantity+ $cart->quantity;
        $product->save();
        }


        $result = Cart::destroy($id);


        if ($result) {
            Session::flash('flash_message', trans('general.deleted'));
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $cart = Cart::findOrFail($id);


        $order=$cart->order;

        if($order->status > config('array.order_status_not_confirmed_index')){
        $product=mProduct::find($cart['product_id']);
        $product->update(['quantity'=>$product->quantity + $cart->quantity - $data['quantity']]);
}



        $result = $cart->update(is_array($data) ? $data : $data->all());


        if ($result) {
            Session::flash('flash_message', trans('general.updated'));
            return $cart;
        } else {
            return false;
        }

    }


    public function return_quantity_to_product($id)
    {




        $cart = Cart::findOrFail($id);
        $product=mProduct::find($cart->product_id);

        $product->quantity= $product->quantity+ $cart->quantity;
        $product->save();


    }
    public function sub_quantity_from_product($id)
    {




        $cart = Cart::findOrFail($id);
        $product=mProduct::find($cart->product_id);

        $product->quantity= $product->quantity- $cart->quantity;
        $product->save();


    }

}
