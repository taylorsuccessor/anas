<?php
Route::group(['middleware' => ['authorization'],'prefix' => 'admin','as'=>'admin.', 'namespace' => '\App\module\cart\admin\controller'], function () {

    Route::get('cart/user-report','Cart@cartUserReport')->name('cart.cartUserReport');
    Route::get('cart/product-report','Cart@cartProductReport')->name('cart.cartProductReport');
    Route::resource('cart','Cart');


});


