<?php
namespace App\module\cart\admin\request;

use Illuminate\Foundation\Http\FormRequest;
use App\module\product\model\Product as mProduct;
class editRequest extends FormRequest
{
/**
* Determine if the user is authorized to make this request.
*
* @return bool
*/
public function authorize()
{
return true;
}

/**
* Get the validation rules that apply to the request.
*
* @return array
*/
public function rules()
{
    $product_quantity=0;
    if(isset($this->product_id)){

    $cartQuantity=(isset($this->quantity))? $this->quantity:0;
        $product= mProduct::find($this->product_id);
        $product_quantity=$product->quantity;
        $product_quantity+=$cartQuantity;

        }
    return [
//    "order_id"=>'required',
        "product_id"=>'required',
        "quantity"=>'required|numeric|max:'.$product_quantity,



    ];
}
}
