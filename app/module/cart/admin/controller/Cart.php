<?php

namespace App\module\cart\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\cart\admin\request\createRequest;
use App\module\cart\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\cart\model\Cart as mCart;
use App\module\cart\admin\repository\CartContract as rCart;

use App\module\order\model\Order as mOrder;

use App\module\order\admin\repository\OrderContract as rOrder;
use App\module\product\admin\repository\ProductContract as rProduct;
use App\module\category\admin\repository\CategoryContract as rCategory;


use App\module\user\admin\repository\UserContract as rUser;

class Cart extends Controller
{
    private $rCart;

    public function __construct(rCart $rCart)
    {
        $this->rCart = $rCart;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request, rProduct $rProduct)
    {

        $statistic = null;
        $oResults = $this->rCart->getByFilter($request, $statistic);

        $productList = $rProduct->getAllList();
        if (!isset($request->ajaxRequest)) {
            return view('admin.cart::index', compact('oResults', 'request', 'productList', 'statistic'));
        }


        $aResults = [];

        if (count($oResults)) {
            foreach ($oResults as $oResult) {
                $aResults[] = [

                    'id' => $oResult->id,

                    'order_id' => $oResult->order_id,

                    'product_id' => $oResult->product_id,
                    'product' => $oResult->product_name(),

                    'quantity' => $oResult->quantity,

                    'created_at' => $oResult->created_at,

                    'updated_at' => $oResult->updated_at,

                ];
            }
        }

        return new JsonResponse(['data' => $aResults, 'total' => $oResults->total(), 'statistic' => $statistic], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function cartUserReport(Request $request, rProduct $rProduct, rUser $rUser)
    {
        $userList = $rUser->getAllList(['type' => config('array.user_type_customer_group')]);

        $user = [];
        if (!isset($request->user_id)) {
            $user_id = 0;
            foreach ($userList as $userId => $user_name) {
                $user_id = $userId;
                break;
            }

            $request->merge(['user_id' => $user_id]);


        }

        $user = $rUser->show($request->user_id);



        $this->rCart->exportExcel($request,'User Sale');
        if(isset($request->print)){
            return view('admin.user::printExcel',['data'=>$this->rCart->toArray($this->rCart->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }

        $statistic = [];
        $oResults = $this->rCart->getByFilter($request, $statistic);

        $productList = $rProduct->getAllList();
        return view('admin.cart::cartUserReport', compact('oResults', 'request', 'productList', 'statistic', 'user','userList'));


    }
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function cartProductReport(Request $request, rProduct $rProduct)
    {
        $productList = $rProduct->getAllList(['type' => config('array.user_type_customer_group')]);

        $product = [];
        if (!isset($request->product_id)) {
            $product_id = 0;
            foreach ($productList as $productId => $productName) {
                $product_id = $productId;
                break;
            }

            $request->merge(['product_id' => $product_id]);


        }

        $product = $rProduct->show($request->product_id);




        $this->rCart->exportExcel($request,'Product Report');
        if(isset($request->print)){
            return view('admin.cart::printExcel',['data'=>$this->rCart->toArray($this->rCart->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }

        $statistic = [];
        $oResults = $this->rCart->getByFilter($request, $statistic);

        return view('admin.cart::cartProductReport', compact('oResults', 'request', 'statistic', 'product','productList'));


    }
 public function productReport(Request $request, rProduct $rProduct)
    {
        $productList = $rProduct->getAllList(['type' => config('array.user_type_customer_group')]);

        $product = [];
        if (!isset($request->product_id)) {
            $product_id = 0;
            foreach ($productList as $productId => $productName) {
                $product_id = $productId;
                break;
            }

            $request->merge(['product_id' => $product_id]);


        }

        $product = $rProduct->show($request->product_id);




        $this->rCart->exportExcel($request,'Product Report');
        if(isset($request->print)){
            return view('admin.cart::printExcel',['data'=>$this->rCart->toArray($this->rCart->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }

        $statistic = [];
        $oResults = $this->rCart->getByFilter($request, $statistic);

        return view('admin.cart::productReport', compact('oResults', 'request', 'statistic', 'product','productList'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request, rOrder $rOrder, rProduct $rProduct)
    {

        $orderList = $rOrder->getAllList();
        $productList = $rProduct->getAllList();

        return view('admin.cart::create', compact('request', 'orderList', 'productList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {

        if (!isset($request->order_id)) {
            $request->merge(['order_id' => $this->getCurrentOrderId($request)]);
        }

        $cartExist = $this->getCurrentProductCart($request);

        if (!isset($request->ajaxRequest)) {
            return redirect('admin/order/' . $request->order_id);
        }


        return new JsonResponse(['status' => 'success', 'data' => $cartExist], 200, [], JSON_UNESCAPED_UNICODE);

    }

    public function getCurrentProductCart($request)
    {
        $cartExist = mCart::where([
            'order_id' => $request->order_id,
            'product_id' => $request->product_id
        ])->first();

        if (!count($cartExist)) {
            $cartExist = $this->rCart->create($request->all());
        } else {


            $result = $this->rCart->update($cartExist->id, ['product_id'=>$cartExist->product_id,'quantity'=>$cartExist->quantity +  $request->quantity]);

        }
        return $cartExist;
    }

    public function getCurrentOrderId($request)
    {
        $orderFilter = [
            'user_id' => \auth::user()->id,
            'status' => config('array.order_status_not_confirmed_index')
        ];
        $orderId = 0;
        $mOrder = mOrder::where($orderFilter)->first();

        if (count($mOrder)) {
            $orderId = $mOrder->id;
        } else {
            $newOrder = mOrder::create($orderFilter);
            $orderId = $newOrder->id;
        }
        return $orderId;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function show($id, Request $request)
    {


        $cart = $this->rCart->show($id);


        if (isset($request->ajaxRequest)) {
            return new JsonResponse(['status' => 'success', 'data' => $cart], 200, [], JSON_UNESCAPED_UNICODE);
        }


        $request->merge(['cart_id' => $id, 'page_name' => 'page']);


        return view('admin.cart::show', compact('cart', 'request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function edit($id, rOrder $rOrder,Request $request, rProduct $rProduct,rCategory $rCategory )
    {


        $cart = $this->rCart->show($id);


        $orderList = $rOrder->getAllList();

        $selectedCategoryId=isset($request->category_id)?$request->category_id:(isset($cart->product->category_id)? $cart->product->category_id:0);
        $productList = $rProduct->getAllList(['category_id'=>$selectedCategoryId]);
        $categoryList  = $rCategory ->getAllList(['sub_category'=>1]);
        return view('admin.cart::edit', compact('cart', 'orderList','request', 'productList','categoryList','selectedCategoryId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {

        $result = $this->rCart->update($id, $request);

        if (!isset($request->ajaxRequest)) {

            return redirect('admin/order/' . $result->order_id);
//            return redirect(route('admin.cart.index'));
        }

        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function destroy($id, Request $request)
    {

        $cart = $this->rCart->show($id);
        $this->rCart->destroy($id);

        if (!isset($request->ajaxRequest)) {
        if(isset($cart->order->id)){

            return redirect('admin/order/' . $cart->order_id);
        }
           return redirect(route('admin.cart.index'));
        }


        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);
    }


}
