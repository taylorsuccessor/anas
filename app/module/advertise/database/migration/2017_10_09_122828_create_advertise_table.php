<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertiseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('advertise', function (Blueprint $table) {
            $table->increments('id');

    $table->string('title_en')->nullable();

    $table->string('title_ar')->nullable();

    $table->string('description_en')->nullable();

    $table->string('description_ar')->nullable();

    $table->string('img')->nullable();

    $table->string('order')->default(0);

    $table->string('type')->default(0);

    $table->string('default')->default(0);

    $table->string('active')->default(0);

    $table->string('available_from')->default('2017-01-01');

    $table->string('available_to')->default('2019-01-01');


$table->timestamps();
        });

//        Schema::table('advertise', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('advertise');
    }
}
