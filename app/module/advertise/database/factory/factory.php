<?php


$factory->define(App\module\advertise\model\Advertise::class,function (Faker\Generator $faker){

    return [

            "title_en"=>$faker->realtext(10),
    "title_ar"=>$faker->realtext(10),
    "description_en"=>$faker->realtext(50),
    "description_ar"=>$faker->realtext(50),
    "img"=>$faker->text(40),
    "order"=>$faker->randomDigit,
    "type"=>$faker->randomElement([0,1]),
    "default"=>$faker->randomElement([0,1]),
    "active"=>$faker->randomElement([0,1]),
    "available_from"=>$faker->dateTimeBetween('-3 years','3 years'),
    "available_to"=>$faker->dateTimeBetween('-3 years','3 years'),

    ];
});