<?php namespace App\module\advertise\model;

use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
    protected $fillable = [
       "id","title_en","title_ar","description_en","description_ar","img","order","type","default","active","available_from","available_to","created_at","updated_at"    ];
    protected $table='advertise';

    public $timestamps =true ;

    protected $guarded = [];



    public function title(){
        return $this->{'title_'.session('locale')};
    }
    public function description(){
        return $this->{'description_'.session('locale')};
    }
    public function type(){
        return array_key_exists($this->type,trans('array.advertise_type'))? trans('array.advertise_type')[$this->type]:'';
    }
    public function defaultValue(){
        return array_key_exists($this->default,trans('array.advertise_default'))? trans('array.advertise_default')[$this->default]:'';
    }
    public function active(){
        return array_key_exists($this->active,trans('array.active'))? trans('array.active')[$this->active]:'';
    }




}
