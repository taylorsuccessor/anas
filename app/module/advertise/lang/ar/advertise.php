<?php
return [


    'advertise'=>' الإعلانات ',
    'advertiseCreate'=>' انشاء اعلان ',
    'addadvertise'=>' اضافة اعلان ',
    'editadvertise'=>'تعديل اعلان',
    'advertiseInfo'=>'معلومات الإعلان',

    'advertiseTableHead'=>'الاعلانات',
    'advertiseTableDescription'=>'قائمة الإعلانات',


    'id'=>'الرقم',


    'title_en'=>'العنوان',


    'title_ar'=>'العنوان العربي',


    'description_en'=>'تفاصيل الاعلان',


    'description_ar'=>'تفاصيل الاعلان العربي',


    'img'=>'الصورة',


    'order'=>'الترتيب',


    'type'=>'النوع',


    'default'=>'مفضل',


    'active'=>'نشط',


    'available_from'=>'متاحة من تاريخ',


    'available_to'=>'متاحة حتى تاريخ',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',

'edit'=>' تعديل ',
    'delete'=>' جذف ',


];
