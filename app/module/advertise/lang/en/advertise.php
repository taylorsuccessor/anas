<?php
return [


'advertise'=>'Advertise',
'advertiseCreate'=>'Create Advertise ',
    'addadvertise'=>'Add Advertise',
    'editadvertise'=>'Edit Advertise',
    'advertiseInfo'=>'Advertise Info',

    'advertiseTableHead'=>'Advertise Table Header',
    'advertiseTableDescription'=>'Advertise Description',


    'id'=>'Id',


    'title_en'=>'Title En',


    'title_ar'=>'Title Ar',


    'description_en'=>'Description En',


    'description_ar'=>'Description Ar',


    'img'=>'Img',


    'order'=>'Order',


    'type'=>'Type',


    'default'=>'Default',


    'active'=>'Active',


    'available_from'=>'Available From',


    'available_to'=>'Available To',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',
    'edit'=>'Edit',
    'delete'=>'Delete'



];
