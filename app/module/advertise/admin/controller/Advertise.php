<?php

namespace App\module\advertise\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\advertise\admin\request\createRequest;
use App\module\advertise\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\advertise\model\Advertise as mAdvertise;
use App\module\advertise\admin\repository\AdvertiseContract as rAdvertise;



class Advertise extends Controller
{
private $rAdvertise;

public function __construct(rAdvertise $rAdvertise)
{
$this->rAdvertise=$rAdvertise;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rAdvertise->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.advertise::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'title'=>  $oResult->title(),
    'description'=>  $oResult->description(),


    'img'=>  $oResult->img,

    'order'=>  $oResult->order,

    'type'=>  $oResult->type(),

    'default'=>  $oResult->defaultValue(),

    'active'=>  $oResult->active(),

    'available_from'=>  $oResult->available_from,

    'available_to'=>  $oResult->available_to,

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.advertise::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rAdvertise->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/advertise');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request)
{


$advertise=$this->rAdvertise->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$advertise],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['advertise_id'=>$id,'page_name'=>'page']);



return view('admin.advertise::show', compact('advertise','request'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$advertise=$this->rAdvertise->show($id);


return view('admin.advertise::edit', compact('advertise'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rAdvertise->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.advertise.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$advertise=$this->rAdvertise->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.advertise.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
