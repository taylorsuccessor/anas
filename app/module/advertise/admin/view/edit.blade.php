@extends('admin.layout::main')

@section('title', trans('general.advertise'))
@section('content')


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.advertise') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.advertise') }}</a></li>
                        <li class="active">{{ trans('advertise::advertise.editadvertise') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>



            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('advertise::advertise.advertise') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('advertise::advertise.editadvertise') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">


                                {!! Form::model($advertise, [
                                    'method' => 'PATCH',
                                    'url' => ['/admin/advertise', $advertise->id],
                                    'class' => 'form-horizontal'
                                ]) !!}







                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('advertise::advertise.editadvertise') }}</span>

                                        <div class="row">
                                            <div class="imgPreview"><img  id="imgPreview" src="{{$advertise['img']}}" ></div>
                                        </div>

                                    </div>

                                    <div class="panel-body">








                                        <div class="row">
                                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('title_en', trans('advertise::advertise.title_en'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('title_en', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('title_ar') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('title_ar', trans('advertise::advertise.title_ar'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('title_ar', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('title_ar', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('description_en') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('description_en', trans('advertise::advertise.description_en'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::textarea('description_en', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('description_en', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('description_ar') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('description_ar', trans('advertise::advertise.description_ar'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::textarea('description_ar', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('description_ar', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('img') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('img', trans('advertise::advertise.img'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('img', null, ['class' => 'form-control uploadFile']) !!}
                                                    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('order') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('order', trans('advertise::advertise.order'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('order', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('type', trans('advertise::advertise.type'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('type',trans('array.advertise_type'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('default') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('default', trans('advertise::advertise.default'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('default',trans('array.advertise_default'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('default', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('active', trans('advertise::advertise.active'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('active',trans('array.active'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('available_from') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('available_from', trans('advertise::advertise.available_from'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('available_from', null, ['class' => 'form-control mydatepicker']) !!}
                                                    {!! $errors->first('available_from', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('available_to') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('available_to', trans('advertise::advertise.available_to'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('available_to', null, ['class' => 'form-control mydatepicker']) !!}
                                                    {!! $errors->first('available_to', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>







                                            <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                {!! Form::submit(trans('general.edit'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                    </div>
                </div>

            </div>





        </div>
    </div>
@endsection