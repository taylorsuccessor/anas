<?php namespace App\module\advertise\admin\repository;

use Session;
use App\module\advertise\model\Advertise;
use App\module\advertise\admin\Repository\AdvertiseContract;

class EloquentAdvertiseRepository implements AdvertiseContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Advertise();


//if(canAccess('admin.advertise.allData')) {
//
//}elseif(canAccess('admin.advertise.groupData')){
//$oResults = $oResults->where('advertise.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.advertise.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('advertise.id', '=' , $data['id']);
        }
        if (isset($data['title_en']) && !empty($data['title_en'])) {
            $oResults = $oResults->where('advertise.title_en', '=' , $data['title_en']);
        }
        if (isset($data['title_ar']) && !empty($data['title_ar'])) {
            $oResults = $oResults->where('advertise.title_ar', '=' , $data['title_ar']);
        }
        if (isset($data['description_en']) && !empty($data['description_en'])) {
            $oResults = $oResults->where('advertise.description_en', '=' , $data['description_en']);
        }
        if (isset($data['description_ar']) && !empty($data['description_ar'])) {
            $oResults = $oResults->where('advertise.description_ar', '=' , $data['description_ar']);
        }
        if (isset($data['img']) && !empty($data['img'])) {
            $oResults = $oResults->where('advertise.img', '=' , $data['img']);
        }
        if (isset($data['orderField']) &&  $data['orderField'] !='') {
            $oResults = $oResults->where('advertise.order', '=' , $data['orderField']);
        }
        if (isset($data['type']) &&  $data['type']!='') {
            $oResults = $oResults->where('advertise.type', '=' , $data['type']);
        }
        if (isset($data['default']) && $data['default'] !='') {
            $oResults = $oResults->where('advertise.default', '=' , $data['default']);
        }
        if (isset($data['active']) &&  $data['active']!='') {
            $oResults = $oResults->where('advertise.active', '=' , $data['active']);
        }
        if (isset($data['available_from']) && !empty($data['available_from'])) {
            $oResults = $oResults->where('advertise.available_from', '=' , $data['available_from']);
        }
        if (isset($data['available_to']) && !empty($data['available_to'])) {
            $oResults = $oResults->where('advertise.available_to', '=' , $data['available_to']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('advertise.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('advertise.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('advertise.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('advertise.order', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }elseif(isset($data['ajaxRequest'])){

             $oResults = $oResults->paginate(config('anas.mobile_max_pagination'));
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Advertise();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name;
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Advertise::create($data);

        if ($result) {
            Session::flash('flash_message', 'advertise added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$advertise = Advertise::findOrFail($id);

        return $advertise;
    }

    public function destroy($id)
    {

        $result =  Advertise::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'advertise deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$advertise = Advertise::findOrFail($id);
       $result= $advertise->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'advertise updated!');
            return true;
        } else {
            return false;
        }

    }

}
