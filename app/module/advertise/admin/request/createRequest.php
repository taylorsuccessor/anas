<?php
namespace App\module\advertise\admin\request;

use Illuminate\Foundation\Http\FormRequest;
class createRequest extends FormRequest
{
/**
* Determine if the user is authorized to make this request.
*
* @return bool
*/
public function authorize()
{
return true;
}

/**
* Get the validation rules that apply to the request.
*
* @return array
*/
public function rules()
{
    $minAvailableTo=isset($this->available_from)? $this->available_from:gmdate('Y-m-d');
return [
    "title_en"=>'required',
    "title_ar"=>'required',
    "description_en"=>'required',
    "description_ar"=>'required',
    "img"=>'required',
    "order"=>'required',
    "type"=>'required',
    "default"=>'required',
    "active"=>'required',
    "available_from"=>'required',
    "available_to"=>'required|after:'.$minAvailableTo,



];
}
}
