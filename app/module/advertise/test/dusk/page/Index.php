<?php
namespace App\module\advertise\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\advertise\model\Advertise;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/advertise';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Advertise::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@title_en',$result->title_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->title_en);


        $browser->click('@searchTabButton');

       $browser->type('@title_ar',$result->title_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->title_ar);


        $browser->click('@searchTabButton');

       $browser->type('@description_en',$result->description_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->description_en);


        $browser->click('@searchTabButton');

       $browser->type('@description_ar',$result->description_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->description_ar);


        $browser->click('@searchTabButton');

       $browser->type('@img',$result->img)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->img);


        $browser->click('@searchTabButton');

       $browser->type('@order',$result->order)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->order);


        $browser->click('@searchTabButton');

       $browser->type('@type',$result->type)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->type);


        $browser->click('@searchTabButton');

       $browser->type('@default',$result->default)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->default);


        $browser->click('@searchTabButton');

       $browser->type('@active',$result->active)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->active);


        $browser->click('@searchTabButton');

       $browser->type('@available_from',$result->available_from)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->available_from);


        $browser->click('@searchTabButton');

       $browser->type('@available_to',$result->available_to)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->available_to);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@title_en"=>"[name=title_en]",

    "@title_ar"=>"[name=title_ar]",

    "@description_en"=>"[name=description_en]",

    "@description_ar"=>"[name=description_ar]",

    "@img"=>"[name=img]",

    "@order"=>"[name=order]",

    "@type"=>"[name=type]",

    "@default"=>"[name=default]",

    "@active"=>"[name=active]",

    "@available_from"=>"[name=available_from]",

    "@available_to"=>"[name=available_to]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
