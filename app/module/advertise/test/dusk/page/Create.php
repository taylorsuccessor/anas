<?php
namespace App\module\advertise\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/advertise/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@title_en')
    ->click('@submit')
    ->assertSee('title_en field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@title_ar')
    ->click('@submit')
    ->assertSee('title_ar field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@description_en')
    ->click('@submit')
    ->assertSee('description_en field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@description_ar')
    ->click('@submit')
    ->assertSee('description_ar field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@img')
    ->click('@submit')
    ->assertSee('img field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@order')
    ->click('@submit')
    ->assertSee('order field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@type')
    ->click('@submit')
    ->assertSee('type field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@default')
    ->click('@submit')
    ->assertSee('default field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->clear('@active')
    ->click('@submit')
    ->assertSee('active field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_to','9')
    
    ->clear('@available_from')
    ->click('@submit')
    ->assertSee('available_from field is required');



    $this->browser
        ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
    
    ->clear('@available_to')
    ->click('@submit')
    ->assertSee('available_to field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@title_en','9')
            ->type('@title_ar','9')
            ->type('@description_en','9')
            ->type('@description_ar','9')
            ->type('@img','9')
            ->type('@order','9')
            ->type('@type','9')
            ->type('@default','9')
            ->type('@active','9')
            ->type('@available_from','9')
            ->type('@available_to','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@title_en"=>"[name=title_en]",

    "@title_ar"=>"[name=title_ar]",

    "@description_en"=>"[name=description_en]",

    "@description_ar"=>"[name=description_ar]",

    "@img"=>"[name=img]",

    "@order"=>"[name=order]",

    "@type"=>"[name=type]",

    "@default"=>"[name=default]",

    "@active"=>"[name=active]",

    "@available_from"=>"[name=available_from]",

    "@available_to"=>"[name=available_to]",


            '@submit'=>'[type=submit]'
        ];
    }
}
