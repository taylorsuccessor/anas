<?php
namespace App\module\advertise\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\advertise\model\Advertise;

class Show extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/advertise/1';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
        $result=Advertise::find(1);

    $browser->assertSee($result->title_en);
    $browser->assertSee($result->title_ar);
    $browser->assertSee($result->description_en);
    $browser->assertSee($result->description_ar);
    $browser->assertSee($result->img);
    $browser->assertSee($result->order);
    $browser->assertSee($result->type);
    $browser->assertSee($result->default);
    $browser->assertSee($result->active);
    $browser->assertSee($result->available_from);
    $browser->assertSee($result->available_to);


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
