<?php
namespace App\module\authorization\middleware;

use Closure, Redirect,App,Session;



class Authorization
{
    public function handle($oRequest, Closure $fNext)
    {

        if(isset($oRequest->token)) {
            $this->loginByToken($oRequest->token);

        }

        if(!canAccess(\Request::route()->getName()) && !isset($oRequest->ajaxRequest)){

            return redirect($this->redirectTo())->withErrors([trans('general.invalidRole')]);
        }


        return $fNext($oRequest);


    }

    private function redirectTo(){

        return 'admin/login';
    }



    public function loginByToken($token){

        $oUser=App\module\user\model\User::where('token','=',$token)->first();

        if($oUser){
            \Auth::login($oUser);



            $allow_permission='';
            $deny_permission='';
            foreach($oUser->role as $role){
                $allow_permission.=$role->allow_permission;
                $deny_permission.=$role->deny_permissions;
            }

            session([
                'allow_permission'=>$allow_permission,
                'deny_permission'=>$deny_permission
            ]);
        }
    }


}
