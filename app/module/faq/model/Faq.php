<?php namespace App\module\faq\model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
       "id","question_en","question_ar","answer_en","answer_ar","created_at","updated_at"    ];
    protected $table='faq';

    public $timestamps =true ;

    protected $guarded = [];


public function question(){
    return $this->{'question_'.session('locale')};
}


    public function answer(){
        return $this->{'answer_'.session('locale')};
    }





}
