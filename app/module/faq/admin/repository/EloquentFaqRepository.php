<?php namespace App\module\faq\admin\repository;

use Session;
use App\module\faq\model\Faq;
use App\module\faq\admin\Repository\FaqContract;

class EloquentFaqRepository implements FaqContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Faq();


//if(canAccess('admin.faq.allData')) {
//
//}elseif(canAccess('admin.faq.groupData')){
//$oResults = $oResults->where('faq.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.faq.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('faq.id', '=' , $data['id']);
        }
        if (isset($data['question_en']) && !empty($data['question_en'])) {
            $oResults = $oResults->where('faq.question_en', '=' , $data['question_en']);
        }
        if (isset($data['question_ar']) && !empty($data['question_ar'])) {
            $oResults = $oResults->where('faq.question_ar', '=' , $data['question_ar']);
        }
        if (isset($data['answer_en']) && !empty($data['answer_en'])) {
            $oResults = $oResults->where('faq.answer_en', '=' , $data['answer_en']);
        }
        if (isset($data['answer_ar']) && !empty($data['answer_ar'])) {
            $oResults = $oResults->where('faq.answer_ar', '=' , $data['answer_ar']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('faq.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('faq.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('faq.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('faq.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }elseif(isset($data['ajaxRequest'])){

             $oResults = $oResults->paginate(config('anas.mobile_max_pagination'));
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Faq();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name;
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Faq::create($data);

        if ($result) {
            Session::flash('flash_message', 'faq added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$faq = Faq::findOrFail($id);

        return $faq;
    }

    public function destroy($id)
    {

        $result =  Faq::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'faq deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$faq = Faq::findOrFail($id);
       $result= $faq->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'faq updated!');
            return true;
        } else {
            return false;
        }

    }

}
