<?php

namespace App\module\faq\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\faq\admin\request\createRequest;
use App\module\faq\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\faq\model\Faq as mFaq;
use App\module\faq\admin\repository\FaqContract as rFaq;



class Faq extends Controller
{
private $rFaq;

public function __construct(rFaq $rFaq)
{
$this->rFaq=$rFaq;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rFaq->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.faq::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'question'=>  $oResult->question(),

    'answer'=>  $oResult->answer(),


    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.faq::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rFaq->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/faq');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request)
{


$faq=$this->rFaq->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$faq],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['faq_id'=>$id,'page_name'=>'page']);



return view('admin.faq::show', compact('faq','request'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$faq=$this->rFaq->show($id);


return view('admin.faq::edit', compact('faq'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rFaq->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.faq.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$faq=$this->rFaq->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.faq.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
