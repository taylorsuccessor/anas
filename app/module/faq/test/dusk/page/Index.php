<?php
namespace App\module\faq\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\faq\model\Faq;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/faq';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Faq::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@question_en',$result->question_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->question_en);


        $browser->click('@searchTabButton');

       $browser->type('@question_ar',$result->question_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->question_ar);


        $browser->click('@searchTabButton');

       $browser->type('@answer_en',$result->answer_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->answer_en);


        $browser->click('@searchTabButton');

       $browser->type('@answer_ar',$result->answer_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->answer_ar);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@question_en"=>"[name=question_en]",

    "@question_ar"=>"[name=question_ar]",

    "@answer_en"=>"[name=answer_en]",

    "@answer_ar"=>"[name=answer_ar]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
