<?php
namespace App\module\faq\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/faq/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@question_en')
    ->click('@submit')
    ->assertSee('question_en field is required');


    $this->browser
    ->clear('@question_ar')
    ->click('@submit')
    ->assertSee('question_ar field is required');


    $this->browser
    ->clear('@answer_en')
    ->click('@submit')
    ->assertSee('answer_en field is required');


    $this->browser
    ->clear('@answer_ar')
    ->click('@submit')
    ->assertSee('answer_ar field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@question_en')
    ->type('@question_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@question_ar')
    ->type('@question_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@answer_en')
    ->type('@answer_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@answer_ar')
    ->type('@answer_ar','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@question_en"=>"[name=question_en]",
             
             "@question_ar"=>"[name=question_ar]",
             
             "@answer_en"=>"[name=answer_en]",
             
             "@answer_ar"=>"[name=answer_ar]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
