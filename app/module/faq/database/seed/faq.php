<?php 
namespace App\module\faq\database\seed;

use Illuminate\Database\Seeder;
use App\module\faq\model\Faq as mFaq;

class faq extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mFaq::insert([

[


    "question_en"=>'',
    "question_ar"=>'',
    "answer_en"=>'',
    "answer_ar"=>'',

],
]);
*/


        factory(mFaq::class,30)->create();
    }
}
