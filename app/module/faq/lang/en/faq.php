<?php
return [


'faq'=>'Faq',
'faqCreate'=>'Create Faq ',
    'addfaq'=>'Add Faq',
    'editfaq'=>'Edit Faq',
    'faqInfo'=>'Faq Info',

    'faqTableHead'=>'Faq Table Header',
    'faqTableDescription'=>'Faq Description',


    'id'=>'Id',


    'question_en'=>'Question En',


    'question_ar'=>'Question Ar',


    'answer_en'=>'Answer En',


    'answer_ar'=>'Answer Ar',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
