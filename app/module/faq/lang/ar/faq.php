<?php
return [


    'faq'=>'الأسئلة الشائعة',
    'faqCreate'=>'انشاء سؤال ',
    'addfaq'=>'اضافة سؤال',
    'editfaq'=>'تعديل سؤال',
    'faqInfo'=>'معلومات السؤال',

    'faqTableHead'=>'الأسئلة الأكثر شيوعا',
    'faqTableDescription'=>'قائمة الأسئلة الأكثر شيوعا',


    'id'=>'الرقم',


    'question_en'=>'السؤال',


    'question_ar'=>'السؤال العربي',


    'answer_en'=>'الجواب',


    'answer_ar'=>'الجواب العربي',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
