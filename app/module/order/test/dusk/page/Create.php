<?php
namespace App\module\order\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/order/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->clear('@user_id')
    ->click('@submit')
    ->assertSee('user_id field is required');



    $this->browser
        ->type('@user_id','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->clear('@total')
    ->click('@submit')
    ->assertSee('total field is required');



    $this->browser
        ->type('@user_id','9')
            ->type('@total','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->clear('@driver_id')
    ->click('@submit')
    ->assertSee('driver_id field is required');



    $this->browser
        ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@status','9')
    
    ->clear('@area_id')
    ->click('@submit')
    ->assertSee('area_id field is required');



    $this->browser
        ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
    
    ->clear('@status')
    ->click('@submit')
    ->assertSee('status field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_id','9')
            ->type('@total','9')
            ->type('@driver_id','9')
            ->type('@area_id','9')
            ->type('@status','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@user_id"=>"[name=user_id]",

    "@total"=>"[name=total]",

    "@driver_id"=>"[name=driver_id]",

    "@area_id"=>"[name=area_id]",

    "@status"=>"[name=status]",


            '@submit'=>'[type=submit]'
        ];
    }
}
