<?php
namespace App\module\order\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\order\model\Order;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/order';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Order::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@user_id',$result->user_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->user_id);


        $browser->click('@searchTabButton');

       $browser->type('@total',$result->total)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->total);


        $browser->click('@searchTabButton');

       $browser->type('@driver_id',$result->driver_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->driver_id);


        $browser->click('@searchTabButton');

       $browser->type('@area_id',$result->area_id)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->area_id);


        $browser->click('@searchTabButton');

       $browser->type('@status',$result->status)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->status);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@user_id"=>"[name=user_id]",

    "@total"=>"[name=total]",

    "@driver_id"=>"[name=driver_id]",

    "@area_id"=>"[name=area_id]",

    "@status"=>"[name=status]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
