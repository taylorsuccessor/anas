<?php
namespace App\module\order\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/order/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@user_id')
    ->click('@submit')
    ->assertSee('user_id field is required');


    $this->browser
    ->clear('@total')
    ->click('@submit')
    ->assertSee('total field is required');


    $this->browser
    ->clear('@driver_id')
    ->click('@submit')
    ->assertSee('driver_id field is required');


    $this->browser
    ->clear('@area_id')
    ->click('@submit')
    ->assertSee('area_id field is required');


    $this->browser
    ->clear('@status')
    ->click('@submit')
    ->assertSee('status field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@user_id')
    ->type('@user_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@total')
    ->type('@total','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@driver_id')
    ->type('@driver_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@area_id')
    ->type('@area_id','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@status')
    ->type('@status','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@user_id"=>"[name=user_id]",
             
             "@total"=>"[name=total]",
             
             "@driver_id"=>"[name=driver_id]",
             
             "@area_id"=>"[name=area_id]",
             
             "@status"=>"[name=status]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
