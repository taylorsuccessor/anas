<?php namespace App\module\order\admin\repository;

use Session;
use App\module\order\model\Order;
use App\module\cart\model\Cart;
use App\module\order\admin\repository\OrderContract;
use App\module\cart\admin\repository\EloquentCartRepository;

class EloquentOrderRepository implements OrderContract
{

    public function getByFilter($data, &$statistic = null)
    {

        $oResults = new Order();


        if (isset($data['ajaxRequest']) && !isset($data['user_id']) && !isset($data['driver_id'])) {

            $oResults = $oResults->where('order.user_id', '=', \Auth::user()->id);
        }
//if(canAccess('admin.order.allData')) {
//
//}elseif(canAccess('admin.order.groupData')){
//$oResults = $oResults->where('order.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.order.userData')){
//
//}else{return [];}


        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('order.id', '=', $data['id']);
        }
        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $oResults = $oResults->where('order.user_id', '=', $data['user_id']);
        }
        if (isset($data['user_phone']) && !empty($data['user_phone'])) {
            $oResults=$oResults->join('user','user.id','=','order.user_id')->select(['order.*']);
            $oResults = $oResults->where('user.phone', 'like', '%'.$data['user_phone'].'%');
        }
        if (isset($data['total']) && !empty($data['total'])) {
            $oResults = $oResults->where('order.total', '=', $data['total']);
        }
        if (isset($data['driver_id']) && !empty($data['driver_id'])) {
            $oResults = $oResults->where('order.driver_id', '=', $data['driver_id']);
        }
        if (isset($data['area_id']) && !empty($data['area_id'])) {
            $oResults = $oResults->where('order.area_id', '=', $data['area_id']);
        }

//        $oResults = $oResults->where('order.status', '!=',0);

        if (isset($data['status']) && $data['status'] != '') {

            $status = is_array($data['status']) ? $data['status'] : [$data['status']];
            $oResults = $oResults->whereIn('order.status', $status);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('order.created_at', '=', $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('order.updated_at', '=', $data['updated_at']);
        }


        if ($statistic !== null) {
            $statistic = $this->getStatistic(clone $oResults);
        }

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('order.' . $data['order'], $sort);
        } else {
            $oResults = $oResults->orderBy('order.id', 'desc');
        }


        if (isset($data['getAllRecords']) && !empty($data['getAllRecords'])) {
            $oResults = $oResults->get();
        } elseif (isset($data['page_name']) && !empty($data['page_name'])) {
            $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        } elseif(isset($data['ajaxRequest'])){

             $oResults = $oResults->paginate(config('anas.mobile_max_pagination'));
        }else {
            $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data = [])
    {

        $oResults = new Order();
        if (isset($data['status']) && $data['status'] != '') {

            $status = is_array($data['status']) ? $data['status'] : [$data['status']];
            $oResults = $oResults->whereIn('order.status', '=', $status);
        }
        $oResults = $oResults->get();

        $aResults = [];

        foreach ($oResults as $result) {
            $aResults[$result->id] = $result->id;
        }
        return $aResults;
    }


    public function getStatistic($oResults)
    {
        $oTotalResults = clone $oResults;

        $current_month = gmdate('Y-m');

        $totalResults = $oTotalResults->count();
        return ['total' => $totalResults];
    }


    public function create($data)
    {

        $result = Order::create($data);

        if ($result) {
            Session::flash('flash_message', 'order added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

        $order = Order::findOrFail($id);

        return $order;
    }

    public function destroy($id)
    {

        $result = Order::find($id);

        if ($result) {

        if( $result->status > config('array.order_status_not_confirmed_index')){
             $cartEloquent=new EloquentCartRepository();
             $cartList=Cart::where('order_id',$id)->get();

                     if(count($cartList)){
                         foreach($cartList as $oneCart){
                             $cartEloquent->return_quantity_to_product($oneCart->id);
                             $oneCart->delete();
                         }
                     }

         }
Cart::where('order_id',$id)->delete();
        $result = Order::destroy($id);
            Session::flash('flash_message', 'order deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $data = is_array($data) ? $data : $data->all();
        $order = Order::findOrFail($id);
        /*
         * fix the total after confirm
         */




        if (isset($data['status']) &&
$order->status == config('array.order_status_not_confirmed_index') &&
        $order->status != $data['status'] &&
             in_array($data['status'] ,config('array.order_status_fixed_total_group'))

        ) {

        if(!$this->check_product_amount($order)){
            Session::flash('flash_warning', trans('order::order.no_enough_quantity'));return false;}


            $data['total'] = $order->total();
            $cartList=Cart::where('order_id',$id)->get();
            foreach($cartList as $oneCart){
            $oneCart->origin_product_price=$oneCart->product_price();
            $oneCart->save();
            }

        }

        if(isset($data['status']) && $order->status == config('array.order_status_not_confirmed_index')
         && $data['status'] == config('array.order_status_pending_index')){
             $cartEloquent=new EloquentCartRepository();
             $cartList=Cart::where('order_id',$id)->get();

                     if(count($cartList)){
                         foreach($cartList as $oneCart){
                             $cartEloquent->sub_quantity_from_product($oneCart->id);
                         }
                     }

         }



        $result = $order->update($data);
        if ($result) {
            Session::flash('flash_message', 'order updated!');
            return true;
        } else {
            return false;
        }

    }

    public function check_product_amount($order){

       $cartList=Cart::where('order_id',$order->id)->get();

                     if(count($cartList)){
                         foreach($cartList as $oneCart){
                             if($oneCart->product->quantity < $oneCart->quantity){$oneCart->quantity=0;$oneCart->save();return false;}
                         }
                     }

    return true;

    }


}
