<?php
namespace App\module\order\admin\request;

use Illuminate\Foundation\Http\FormRequest;

class editRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset($this->status) && $this->status ==config('array.order_status_assigned_index') ){
        return [

            'driver_id'=>'required',
        ];
    }
        return [
//            "user_id" => 'required',
        ];
    }
}
