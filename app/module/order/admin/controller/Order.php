<?php

namespace App\module\order\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\order\admin\request\createRequest;
use App\module\order\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\order\model\Order as mOrder;
use App\module\order\admin\repository\OrderContract as rOrder;



use App\module\user\admin\repository\UserContract as rUser;
use App\module\area\admin\repository\AreaContract as rArea;
use App\module\cart\admin\repository\CartContract as rCart;
use App\module\category\admin\repository\CategoryContract as rCategory;
use App\module\product\admin\repository\ProductContract as rProduct;

class Order extends Controller
{
    private $rOrder;

    public function __construct(rOrder $rOrder)
    {
        $this->rOrder = $rOrder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request, rUser $rUser, rArea $rArea)
    {

        $statistic = null;
        $oResults = $this->rOrder->getByFilter($request, $statistic);


        if (!isset($request->ajaxRequest)) {


            $userList = $rUser->getAllList(['type' => config('array.user_type_customer_group')]);
            $driverList = $rUser->getAllList(['type' => config('array.user_type_driver_index')]);
            $areaList = $rArea->getAllList();
            return view('admin.order::index', compact('oResults', 'request', 'userList', 'driverList', 'areaList', 'statistic'));
        }


        $aResults = [];

        if (count($oResults)) {
            foreach ($oResults as $oResult) {
                $row = [

                    'id' => $oResult->id,

                    'user' => $oResult->user_name(),
                    'user_id' => $oResult->user_id,
                    'user_info'=>$oResult->user,

                    'total' => $oResult->total(),

                    'driver_id' => $oResult->driver_id,
                    'driver' => $oResult->driver_name(),

                                        'area' => $oResult->area_name(),
                                        'area_id' => $oResult->area_id,
                                        'status' => $oResult->status(),
                                        'status_id' => $oResult->status,

                                        'created_at' => $oResult->created_at,

                                        'updated_at' => $oResult->updated_at,

                                    ];
    $row['cart']=[];


    if(count($oResult->cart)){
        foreach($oResult->cart as $cart){
            $row['cart'][]=[
                'id'=>$cart->id,
                'product'=>$cart->product_name(),
                'product_price'=>$cart->product_price(),
                'product_img'=>$cart->product_img(),

                'product_id'=>$cart->product_id,
                'quantity'=>$cart->quantity,
                'total_price'=>$cart->price(),
            ];
        }
    }
    $aResults[]=$row;





            }
        }

        return new JsonResponse(['data' => $aResults, 'total' => $oResults->total(), 'statistic' => $statistic], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request, rUser $rUser, rArea $rArea)
    {

        $userList = $rUser->getAllList(['type' => config('array.user_type_customer_group'),'status'=>config('array.user_status_active_index'),'area_id'=>(isset($request->area_id))?$request->area_id:'']);
        $driverList = $rUser->getAllList(['type' => config('array.user_type_driver_index'),'status'=>config('array.user_status_active_index')]);
        $areaList = $rArea->getAllList();

        return view('admin.order::create', compact('request', 'userList', 'areaList', 'driverList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {


        $oResults = $this->rOrder->create($request->all());

        if (!isset($request->ajaxRequest)) {
            return redirect('admin/order/'.$oResults->id);
        }


        return new JsonResponse(['status' => 'success', 'data' => $oResults], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function show($id, Request $request, rCart $rCart, rProduct $rProduct,rCategory $rCategory)
    {


        $order = $this->rOrder->show($id);


        if (isset($request->ajaxRequest)) {
            return new JsonResponse(['status' => 'success', 'data' => $order], 200, [], JSON_UNESCAPED_UNICODE);
        }


        $request->merge(['order_id' => $id, 'page_name' => 'page']);

        $request->page_name = 'page_cart';

        $oCartResults = $rCart->getByFilter($request);


        $selectedCategoryId=isset($request->category_id)?$request->category_id:0;
        $productList = $rProduct->getAllList(['category_id'=>$selectedCategoryId]);
        $categoryList  = $rCategory ->getAllList(['sub_category'=>1]);


        return view('admin.order::show', compact('order', 'request', 'oCartResults', 'productList','categoryList'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function edit($id, rUser $rUser, rArea $rArea)
    {

        $order = $this->rOrder->show($id);


        $userList = $rUser->getAllList(['type' => config('array.user_type_customer_group')]);
        $driverList = $rUser->getAllList(['type' => config('array.user_type_driver_index')]);
        $areaList = $rArea->getAllList();


        return view('admin.order::edit2', compact('order', 'userList', 'areaList', 'driverList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {

        $oldRecord = $this->rOrder->show($id);
        $result = $this->rOrder->update($id, $request);
        $this->notification($oldRecord, $request);
        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.order.index'));
        }

        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);


    }

    public function notification($oldRecord, $request)
    {


        if (!isset($request->status) || (isset($request->status) && $request->status == $oldRecord->status)) {
            return false;
        }
        $newRecord = $this->rOrder->show($oldRecord->id);


        $notificationTemplateName = 'order_confirmed';
        if ($request->status == config('array.order_status_confirmed_index')) {
            $notificationTemplateName = 'order_confirmed';
        } else if ($request->status == config('array.order_status_assigned_index')) {
            $notificationTemplateName = 'order_assigned';
        } else if ($request->status == config('array.order_status_pending_index')) {
            $notificationTemplateName = 'order_pending';
        } else if ($request->status == config('array.order_status_executed_index')) {
            $notificationTemplateName = 'order_executed';
        } else {
            return false;
        }


        $user = $oldRecord->user;
        $driver = $oldRecord->driver;


        notification($notificationTemplateName, ['user' => $user, 'user_email' => $user->email, 'order' => $newRecord]);

        if (isset($driver->id)) {

            notification($notificationTemplateName, ['user' => $driver, 'user_email' => $user->email, 'order' => $newRecord]);
        }

        $replaceUserByAdminEmail = clone $user;
        $replaceUserByAdminEmail->email = config('module.admin_email');
        notification($notificationTemplateName, ['user' => $replaceUserByAdminEmail, 'user_email' => $user->email, 'order' => $newRecord]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function destroy($id, Request $request)
    {
        $order = $this->rOrder->destroy($id);

        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.order.index'));
        }


        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);
    }

    /*
     * show the invoice
     *
     * @param
     *
     * @return
     */
    public function invoice($id, Request $request, rCart $rCart, rProduct $rProduct)
    {


        $order = $this->rOrder->show($id);



        $request->merge(['order_id' => $id, 'page_name' => 'page','getAllRecords'=>1]);

        $request->page_name = 'page_cart';

        $oCartResults = $rCart->getByFilter(['order_id'=>$id]);

        return view('admin.order::invoice', compact('order', 'request', 'oCartResults'));
    }



}
