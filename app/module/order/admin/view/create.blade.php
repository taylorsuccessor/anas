@extends('admin.layout::main')

@section('title', trans('general.order'))
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.order') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.order') }}</a></li>
                        <li class="active">{{ trans('order::order.orderCreate') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" method="get" action="" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>









            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('order::order.order') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('order::order.addorder') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">





                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('order::order.addorder') }}</span>
                                    </div>


                                    {!! Form::model($request, [
                                        'method' => 'GET',
                                        'url' => '/admin/order/create',
                                        'class' => 'form-horizontal',
                                        'style'=>'margin:0px;padding:0px; ',
                                        'id'=>'selectCategoryForm'

                                    ]) !!}


                                    <div class="form-group {{ $errors->has('area_id') ? 'has-error' : ''}}   col-xs-6">

                                        {!! Form::label('driver_id', trans('order::order.area_id'), ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                            {!! Form::select('area_id',$areaList, null, ['placeholder'=>trans('order::order.area_id'),'class' => 'form-control','onchange'=>"$('#selectCategoryForm').submit();" ]) !!}
                                            {!! $errors->first('area_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    {!! Form::close() !!}

                                    <div class="clearfix" style="clear:both"></div>
                                    <div class="panel-body">

                                        {!! Form::model($request,['url' => '/admin/order', 'class' => 'form-horizontal']) !!}



<div class="clearfix" style="clear:both"></div>
                                        <div class="row">
                                        <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}  col-xs-6">
                                            {!! Form::label('user_id', trans('order::order.user_id'), ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::select('user_id',$userList, null, ['class' => 'form-control']) !!}
                                                {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                                                                
                                        
                                        <div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}  col-xs-6" style="display:none;">
                                            {!! Form::label('total', trans('order::order.total'), ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::text('total', null, ['class' => 'form-control']) !!}
                                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>

                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('status', trans('order::order.status'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('status',trans('array.order_status'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                        </div>                                        
                                        <div class="row">
                                        <div class="form-group {{ $errors->has('driver_id') ? 'has-error' : ''}}  col-xs-6">
                                            {!! Form::label('driver_id', trans('order::order.driver_id'), ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="col-sm-8">
                                                {!! Form::select('driver_id',$driverList ,null, ['placeholder'=>trans('general.driver'),'class' => 'form-control']) !!}
                                                {!! $errors->first('driver_id', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                                                                







                                        </div>                                        
                                        <div class="row">


                                                                                






                                        <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                {!! Form::submit(trans('general.create'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}

                            </div>

                        </div>
                    </div>
                </div>

            </div>







        </div>
    </div>
@endsection