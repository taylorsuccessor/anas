@extends('admin.layout::main')
@section('title', trans('order::order.order'))
@section('content')


    <?php
    $canEdit=canAccess('admin.order.edit');
    $canDestroy=canAccess('admin.order.destroy');

    $canCartRelation=canAccess('admin.cart.relation');
    $canCartEdit=canAccess('admin.cart.edit');
    $canCartDestroy=canAccess('admin.cart.destroy');
    $canCartCreate=canAccess('admin.cart.create');
    $canCartShow=canAccess('admin.cart.show');


   ?>




    <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                    <div class="col-lg-12">
                        <h4 class="page-title">{{ trans('general.order') }}</h4>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <ol class="breadcrumb pull-left">
                            <li><a href="#">{{ trans('general.order') }}</a></li>
                            <li class="active">{{ trans('general.details') }}</li>
                        </ol>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <form role="search" class="app-search hidden-xs pull-right">
                            <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                            <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                        </form>
                    </div>
                </div>







                <div class="row">


                    <div class="col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">{{ trans('general.order') }}</h3>
                            <p class="text-muted m-b-40">{{ trans('general.details') }}</p>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">


                                <li role="presentation" class="active">
                                    <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.detail')}}</span></a>
                                </li>

                                
                                <li role="presentation" >
                                    <a href="#cart" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-list"></i>{{trans('general.cart')}}</span></a>
                                </li>

                                

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">


                                <div role="tabpanel" class="tab-pane active" id="idetail">


                                    <div class="panel">
                                        <div class="panel-heading">
                                            <span class="panel-title">{{ trans('order::order.orderInfo') }}</span>
                                        </div>

                                        <div class="panel-body">


                                                                                        <div class="row">                                            <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.user_id') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label"><a href="/admin/user/{{$order['user_id']}}/edit">
                                                    @if(isset($order->user->id))
                                                    {{$order->user_name() }}
                                                    -
                                                    {{$order->user->last_name}}
                                                    -
                                                    {{$order->user->type()}}


                                                    @endif

                                                    </a></label>
                                                </div>
                                            </div>

                                            
                                                                                                                                    <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.total') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{$order['total'] }}</label>
                                                </div>
                                            </div>

                                            </div>
                                                                                        <div class="row">                                            <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.driver_id') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label"><a href="/admin/user/{{$order['driver_id']}}/edit"> {{$order->driver_name() }}</a></label>
                                                </div>
                                            </div>

                                            
                                                                                                                                    <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.area_id') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{$order->area_name()}}</label>
                                                </div>
                                            </div>

                                            </div>
                                                                                        <div class="row">                                            <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.status') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{$order->status() }}</label>
                                                </div>
                                            </div>

                                            
                                                                                                                                    <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.created_at') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{$order['created_at'] }}</label>
                                                </div>
                                            </div>

                                            </div>
                                                                                        <div class="row">                                            <div class="col-sm-2 text-right">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{ trans('order::order.updated_at') }}  </label>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 text-left">
                                                <div class="form-group no-margin-hr">
                                                    <label class="control-label">{{$order['updated_at'] }}</label>
                                                </div>
                                            </div>

                                            
                                            



                                            <div class="row">

                                                <div class="col-xs-offset-6 col-xs-3">

                                                    @if($canEdit)
                                                        <a href="/admin/order/{{ $order['id'] }}/edit"
                                                           class="fa fa-edit btn btn-primary form-control"> <span style="font-family:'Poppins', sans-serif;"> {{trans('general.edit')}}</span></a>
                                                    @endif
                                                </div>
                                                <div class=" col-xs-3">
                                                    @if($canDestroy)
                                                        {!! Form::open(['method' => 'DELETE',
                                                'url' => ['/admin/order',$order['id']]]) !!}
                                                        <button type="submit" name="Delete" class="deleteRow  btn btn-danger form-control" >
                                                            <i class="fa fa-trash"></i>
                                                            {{trans('general.delete')}}
                                                        </button>
                                                        {!! Form::close() !!}
                                                    @endif
                                                </div>

                                            </div>


                                        </div>
                                        <!-- row -->
                                    </div>


                                </div>




                                                                @if( $canCartRelation)
                                <div role="tabpanel" class="tab-pane active" id="cart">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="white-box">



                                                    @include('admin.layout::partial.messages')

                                                    <div class=" col-xs-9">
                                                        <h3 class="box-title m-b-0">{{ trans('cart::cart.cartTableHead') }}</h3>
                                                        <p class="text-muted m-b-20">{{ trans('cart::cart.cartTableDescription') }}</p>



                                                    </div>



                                                    <div class="col-xs-3">



                                                        {!! Form::model($request, [
                                                            'method' => 'GET',
                                                            'url' => '/admin/order/'.$order['id'] ,
                                                            'class' => 'form-horizontal',
                                                            'style'=>'margin:0px;padding:0px; ',
                                                            'id'=>'selectCategoryForm'

                                                        ]) !!}


                                                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                                                            <div class="col-sm-12">
                                                                {!! Form::select('category_id',$categoryList, null, ['placeholder'=>trans('general.subCategory'),'class' => 'form-control','onchange'=>'$("#selectCategoryForm").submit();']) !!}
                                                                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                                                            </div>
                                                        </div>

                                                        {!! Form::close() !!}


                                                    </div>


                                                    @if( $canCartCreate)
                                                        <div class="col-xs-3" style="display: none;">
                                                            <a  href="{{route('admin.cart.create')}}?order_id={{$order['id'] }}"class="btn btn-primary form-control">
                                                                + {{trans('cart::cart.cartCreate')}}
                                                            </a>
                                                        </div>
                                                    @endif


                                                    <table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>

                                                        <thead>
                                                        <tr>


                                                                                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">
                                                                {!! th_sort(trans('cart::cart.id'), 'id', $oCartResults) !!}
                                                            </th>



                                                                                                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                                                                {!! th_sort(trans('cart::cart.product_id'), 'product_id', $oCartResults) !!}
                                                            </th>

                                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">
                                                                {!! th_sort(trans('cart::cart.quantity'), 'quantity', $oCartResults) !!}
                                                            </th>
                                                            <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">
                                                                {!!  trans('product::product.price')  !!}
                                                            </th>


                                                                                                                        @if($canCartShow
                                                            || $canCartEdit
                                                            || $canCartDestroy
                                                            )
                                                                <th class="actionHeader"><i class="fa fa-cog"></i> </th>
                                                            @endif
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $totalPrice=0;?>
                                                        @if (count($oCartResults))
                                                         <?php $i=0; $class='';  ?>
                                                         @foreach($oCartResults as $oResult)
                                                             <?php $price=$oResult->price(); $totalPrice+=$price;?>
                                                                {{-- */$class=($i%2==0)? 'gradeA even':'gradeA odd';$i+=1;/* --}}
                                                                <tr class='{{ $class }}'>

                                                                                                                                        <td>{{ $oResult->id }}</td>

                                                                                                                                        <td>{{ $oResult->product_name() }}</td>

                                                                    <td>{{ $oResult->quantity }}</td>
                                                                    <td>{{ $price }}</td>

                                                                    
                                                                    <td>

                                                                        <div class="tableActionsMenuDiv">
                                                                            <div class="innerContainer">
                                                                                <i class="fa fa-list menuIconList"></i>

                                                                                @if($canCartDestroy )
                                                                                    {!! Form::open(['method' => 'DELETE',
                                                                                    'url' => ['/admin/cart',$oResult->id]]) !!}
                                                                                    <button type="submit" name="Delete" class="deleteRow" >
                                                                                        <i class="fa fa-trash"></i>
                                                                                    </button>
                                                                                    {!! Form::close() !!}
                                                                                @endif

                                                                                @if( $canCartEdit)
                                                                                    <a href="/admin/cart/{{ $oResult->id }}/edit"
                                                                                       class="fa fa-edit"></a>
                                                                                @endif
                                                                                @if($canCartShow )
                                                                                    <a href="/admin/cart/{{ $oResult->id }}"
                                                                                       class="fa fa-file-text"></a>

                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                        <tfoot>



                                                        {!! Form::model($request,['url' => '/admin/cart', 'class' => 'form-horizontal']) !!}







                                                        <tr>
                                                        <td></td>

                                                        <td>


                                                            <div class="form-group {{ $errors->has('product_id') ? 'has-error' : ''}}  col-xs-12">
                                                                <div class="col-sm-12">
                                                                    {!! Form::select('product_id',$productList, null, ['class' => 'form-control']) !!}
                                                                    {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
                                                                </div>
                                                            </div>



                                                        </td>


                                                        <td>

                                                            <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}  col-xs-12">
                                                                <div class="col-sm-12">
                                                                    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                                                                    {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
                                                                </div>
                                                            </div>
                                                        </td>
                                                            <td>
                                                                {{$totalPrice}}
                                                            </td>
                                                        <td>



                                                            {!! Form::hidden('order_id', $order['id'], ['class' => 'form-control']) !!}
                                                            {!! Form::submit(trans('general.add'), ['class' => 'btn btn-primary form-control']) !!}


                                                        </td>

</tr>












                                                            {!! Form::close() !!}



























                                                        </tfoot>
                                                    </table>
                                                    @if (count($oCartResults))
                                                        <div class="row">

                                                            <div class="col-xs-12 col-sm-6 ">


                                                                @include('admin.layout::partial.paginationSize',compact($request))
                                                                <span class="text-xs">{{trans('general.showing')}} {{ $oCartResults->firstItem() }} {{trans('general.to')}} {{ $oCartResults->lastItem() }} {{trans('general.of')}} {{ $oCartResults->total() }} {{trans('general.entries')}}</span>
                                                            </div>


                                                            <div class="col-xs-12 col-sm-6 ">
                                                                {!! str_replace('/?', '?', $oCartResults->appends($request->except('page_cart'))->render()) !!}
                                                            </div>
                                                        </div>
                                                    @else
                                                        {{--<div class="noResultDiv" >{{trans('general.noResult')}}</div>--}}

                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                               </div>
                                @endif
                                

                            </div>
                        </div>
                    </div>

                </div>







            </div>
        </div>



@stop