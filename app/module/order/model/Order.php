<?php namespace App\module\order\model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "id", "user_id", "total", "driver_id", "area_id", "status", "created_at", "updated_at"];
    protected $table = 'order';

    public $timestamps = true;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\module\user\model\User');
    }

    public function driver()
    {
        return $this->belongsTo('App\module\user\model\User', 'driver_id');
    }

    public function area()
    {
        return $this->belongsTo('App\module\area\model\Area');
    }


    public function cart()
    {
        return $this->hasMany('App\module\cart\model\Cart');
    }



    public function user_name()
    {
        return (isset($this->user->id)) ? $this->user->first_name : '';
    }

    public function driver_name()
    {
        return (isset($this->driver->id)) ? $this->driver->first_name : '';
    }

    public function area_name()
    {
    return (isset($this->user->id)) ? $this->user->area_name() : '';
        return (isset($this->area->id)) ? $this->area->name() : '';
    }

    public function status()
    {
        return array_key_exists($this->status, trans('array.order_status')) ? trans('array.order_status')[$this->status] : '';
    }


    public function total(){

        $price=0;
        if(in_array($this->status,config('array.order_status_fixed_total_group'))){return $this->total;}
        if(!count($this->cart)){return $price;}

        foreach($this->cart as $cart){
            $price+=$cart->price();
        }
        return round($price,2);
    }


}
