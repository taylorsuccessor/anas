<?php
return [


'order'=>'Order',
'orderCreate'=>'Create Order ',
    'addorder'=>'Add Order',
    'editorder'=>'Edit Order',
    'orderInfo'=>'Order Info',

    'orderTableHead'=>'Order Table Header',
    'orderTableDescription'=>'Order Description',


    'id'=>'Id',


    'user_id'=>'User Id',


    'total'=>'Total',


    'driver_id'=>'Driver',


    'area_id'=>'Area',


    'status'=>'Status',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',
'no_enough_quantity'=>'no enough product quantity',


];
