<?php
return [


    'order'=>'الفواتير',
    'orderCreate'=>'انشاء فاتورة',
    'addorder'=>'اضافة فاتورة',
    'editorder'=>'تعديل الفاتورة',
    'orderInfo'=>'معلومات الفاتورة',

    'orderTableHead'=>'الفواتير',
    'orderTableDescription'=>'قائمة الفواتير',


    'id'=>'الرقم',


    'user_id'=>'المستخدم',


    'total'=>'المبلغ',


    'driver_id'=>'السائق',


    'area_id'=>'المنطقة',


    'status'=>'الحالة',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',

'no_enough_quantity'=>' لا توجد كمية كافية للمنتجات ',


];
