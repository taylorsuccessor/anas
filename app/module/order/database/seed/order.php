<?php 
namespace App\module\order\database\seed;

use Illuminate\Database\Seeder;
use App\module\order\model\Order as mOrder;

class order extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mOrder::insert([

[


    "user_id"=>'',
    "total"=>'',
    "driver_id"=>'',
    "area_id"=>'',
    "status"=>'',

],
]);
*/


        factory(mOrder::class,30)->create();
    }
}
