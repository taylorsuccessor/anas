<?php


$factory->define(App\module\order\model\Order::class,function (Faker\Generator $faker){

    return [

            "user_id"=>$faker->numberBetween(0,50),
    "total"=>$faker->randomDigit,
    "driver_id"=>$faker->numberBetween(0,50),
    "area_id"=>$faker->numberBetween(0,50),
    "status"=>$faker->randomElement([0,1,2,3,4,5]),

    ];
});