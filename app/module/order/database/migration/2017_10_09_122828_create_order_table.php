<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');

    $table->string('user_id');

    $table->string('total')->default(0)->nullable();

    $table->string('driver_id')->nullable();

    $table->string('area_id')->nullable();

    $table->string('status')->default(0);


$table->timestamps();
        });

//        Schema::table('order', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('order');
    }
}
