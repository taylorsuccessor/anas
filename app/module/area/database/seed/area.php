<?php 
namespace App\module\area\database\seed;

use Illuminate\Database\Seeder;
use App\module\area\model\Area as mArea;

class area extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mArea::insert([

[


    "name_en"=>'',
    "name_ar"=>'',
    "active"=>'',

],
]);
*/


        factory(mArea::class,30)->create();
    }
}
