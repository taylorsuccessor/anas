<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('area', function (Blueprint $table) {
            $table->increments('id');

    $table->string('name_en')->nullable();

    $table->string('name_ar')->nullabel();

    $table->string('active')->default(0);


$table->timestamps();
        });

//        Schema::table('area', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('area');
    }
}
