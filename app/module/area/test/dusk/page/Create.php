<?php
namespace App\module\area\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/area/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@name_ar','9')
            ->type('@active','9')
    
    ->clear('@name_en')
    ->click('@submit')
    ->assertSee('name_en field is required');



    $this->browser
        ->type('@name_en','9')
            ->type('@active','9')
    
    ->clear('@name_ar')
    ->click('@submit')
    ->assertSee('name_ar field is required');



    $this->browser
        ->type('@name_en','9')
            ->type('@name_ar','9')
    
    ->clear('@active')
    ->click('@submit')
    ->assertSee('active field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@name_en','9')
            ->type('@name_ar','9')
            ->type('@active','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@name_en"=>"[name=name_en]",

    "@name_ar"=>"[name=name_ar]",

    "@active"=>"[name=active]",


            '@submit'=>'[type=submit]'
        ];
    }
}
