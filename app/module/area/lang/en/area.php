<?php
return [


'area'=>'Area',
'areaCreate'=>'Create Area ',
    'addarea'=>'Add Area',
    'editarea'=>'Edit Area',
    'areaInfo'=>'Area Info',

    'areaTableHead'=>'Area Table Header',
    'areaTableDescription'=>'Area Description',


    'id'=>'Id',


    'name_en'=>'Name En',


    'name_ar'=>'Name Ar',


    'active'=>'Active',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
