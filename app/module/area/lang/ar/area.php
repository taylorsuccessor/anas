<?php
return [


'area'=>'المناطق',
'areaCreate'=>'انشاء منطقة',
    'addarea'=>'اضافة منطقة',
    'editarea'=>'تعديل المنطقة',
    'areaInfo'=>'معلومات المنطقة',

    'areaTableHead'=>'المناطق',
    'areaTableDescription'=>'قائمة المناطق',


    'id'=>'الرقم',


    'name_en'=>' الاسم الانكليزي ',


    'name_ar'=>'الاسم العربي',


    'active'=>'نشط',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
