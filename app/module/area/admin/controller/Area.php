<?php

namespace App\module\area\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\area\admin\request\createRequest;
use App\module\area\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\area\model\Area as mArea;
use App\module\area\admin\repository\AreaContract as rArea;


use App\module\user\admin\repository\UserContract as rUser;
use App\module\order\admin\repository\OrderContract as rOrder;

class Area extends Controller
{
    private $rArea;

    public function __construct(rArea $rArea)
    {
        $this->rArea=$rArea;
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        $statistic=null;
        $oResults=$this->rArea->getByFilter($request,$statistic);

        if(!isset($request->ajaxRequest)){
            return view('admin.area::index', compact('oResults','request','statistic'));
        }


        $aResults=[];

        if(count($oResults)){
            foreach($oResults as $oResult){
                $aResults[]=[

                    'id'=>  $oResult->id,

                    'name'=>  $oResult->name(),

                    'active'=>  $oResult->active(),

                    'created_at'=>  $oResult->created_at,

                    'updated_at'=>  $oResult->updated_at,

                ];
            }
        }

        return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request)
    {


        return view('admin.area::create',compact('request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {


        $oResults=$this->rArea->create($request->all());

        if(!isset($request->ajaxRequest)){
            return redirect('admin/area');
        }


        return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return view
     */
    public function show($id,Request $request,rUser $rUser,rOrder $rOrder)
    {


        $area=$this->rArea->show($id);



        if(isset($request->ajaxRequest)){
            return new JsonResponse(['status'=>'success','data'=>$area],200,[],JSON_UNESCAPED_UNICODE);
        }



        $request->merge(['area_id'=>$id,'page_name'=>'page']);


        $request->page_name='page_user';
        $oUserResults=$rUser->getByFilter($request);
        $request->page_name='page_order';
        $oOrderResults=$rOrder->getByFilter($request);

        return view('admin.area::show', compact('area','request','oUserResults','oOrderResults'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return view
     */
    public function edit($id)
    {


        $area=$this->rArea->show($id);


        return view('admin.area::edit', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {

        $result=$this->rArea->update($id,$request);

        if(!isset($request->ajaxRequest)){
            return redirect(route('admin.area.index'));
        }

        return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return redirect
     */
    public function destroy($id,Request $request)
    {
        $area=$this->rArea->destroy($id);

        if(!isset($request->ajaxRequest)){
            return redirect(route('admin.area.index'));
        }


        return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
    }


}
