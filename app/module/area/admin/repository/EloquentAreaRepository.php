<?php namespace App\module\area\admin\repository;

use Session;
use App\module\area\model\Area;
use App\module\area\admin\Repository\AreaContract;

class EloquentAreaRepository implements AreaContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Area();


//if(canAccess('admin.area.allData')) {
//
//}elseif(canAccess('admin.area.groupData')){
//$oResults = $oResults->where('area.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.area.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('area.id', '=' , $data['id']);
        }
        if (isset($data['name_en']) && !empty($data['name_en'])) {
            $oResults = $oResults->where('area.name_en', 'like' , '%'.$data['name_en'].'%');
        }
        if (isset($data['name_ar']) && !empty($data['name_ar'])) {
            $oResults = $oResults->where('area.name_ar', 'like' , '%'.$data['name_ar'].'%');
        }
        if (isset($data['active']) && $data['active']!='') {
            $oResults = $oResults->where('area.active', '=' , $data['active']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('area.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('area.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('area.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('area.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Area();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name();
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Area::create($data);

        if ($result) {
            Session::flash('flash_message', 'area added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$area = Area::findOrFail($id);

        return $area;
    }

    public function destroy($id)
    {

        $result =  Area::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'area deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$area = Area::findOrFail($id);
       $result= $area->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'area updated!');
            return true;
        } else {
            return false;
        }

    }

}
