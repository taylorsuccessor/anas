
//App Serivce provider


\Illuminate\Support\Facades\Schema::defaultStringLength(191);

//providers

/*
* custom module
*/

    App\module\user\UserServiceProvider::class,



    App\module\category\CategoryServiceProvider::class,



    App\module\product\ProductServiceProvider::class,



    App\module\product_type\ProductTypeServiceProvider::class,



    App\module\area\AreaServiceProvider::class,



    App\module\company_type\CompanyTypeServiceProvider::class,



    App\module\order\OrderServiceProvider::class,



    App\module\cart\CartServiceProvider::class,



    App\module\faq\FaqServiceProvider::class,



    App\module\contact_us\ContactUsServiceProvider::class,



    App\module\cms\CmsServiceProvider::class,



    App\module\advertise\AdvertiseServiceProvider::class,



    App\module\communication\CommunicationServiceProvider::class,



];


//seed
    $this->call('App\module\user\database\seed\user');

    $this->call('App\module\category\database\seed\category');

    $this->call('App\module\product\database\seed\product');

    $this->call('App\module\product_type\database\seed\product_type');

    $this->call('App\module\area\database\seed\area');

    $this->call('App\module\company_type\database\seed\company_type');

    $this->call('App\module\order\database\seed\order');

    $this->call('App\module\cart\database\seed\cart');

    $this->call('App\module\faq\database\seed\faq');

    $this->call('App\module\contact_us\database\seed\contact_us');

    $this->call('App\module\cms\database\seed\cms');

    $this->call('App\module\advertise\database\seed\advertise');

    $this->call('App\module\communication\database\seed\communication');


];



//to load all seeders to DatabaseSeeder.php

public function getModuleSeedFile(){

$allModulePath=app_path('module');
$moduleList=scandir($allModulePath);
if(is_array($moduleList)){
foreach($moduleList as $oneModule){
$oneModuleSeedPath=$allModulePath.'\\'.$oneModule.'\\database\\seed';
if(!is_dir($oneModuleSeedPath) || $oneModule =='.' ||$oneModule=='..'){continue;}
$fileList=scandir($oneModuleSeedPath);

if(is_array($fileList)){
foreach($fileList as $oneFile){
if(!\Illuminate\Support\Str::endsWith($oneFile,'.php')){continue;}
$oneFilePath='\\App\\module\\'.$oneModule.'\\database\\seed\\'.rtrim($oneFile,'.php');
$this->call($oneFilePath);
}
}


}
}
}






//test browser add it to some test


public function getModuleTestDuskPage($browser){

$allModulePath=app_path('module');
$moduleList=scandir($allModulePath);
if(is_array($moduleList)){
foreach($moduleList as $oneModule){
$oneModuleDuskPagePath=$allModulePath.'\\'.$oneModule.'\\test\\dusk\\page';
if(!is_dir($oneModuleDuskPagePath) || $oneModule =='.' ||$oneModule=='..'){continue;}
$pageList=scandir($oneModuleDuskPagePath);

if(is_array($pageList)){
foreach($pageList as $onePage){
if(!\Illuminate\Support\Str::endsWith($onePage,'.php')){continue;}
$onePage='\\App\\module\\'.$oneModule.'\\test\\dusk\\page\\'.rtrim($onePage,'.php');
$browser->visit(new $onePage());
}
}


}
}
}