<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('contact_us', function (Blueprint $table) {
            $table->increments('id');

    $table->string('user_name');

            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();


    $table->string('title')->nullable();

    $table->string('message')->nullable();


$table->timestamps();
        });

//        Schema::table('contact_us', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('contact_us');
    }
}
