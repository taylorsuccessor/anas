<?php


$factory->define(App\module\contact_us\model\ContactUs::class, function (Faker\Generator $faker) {

    return [

        "user_name" => $faker->name,
        "phone" => $faker->phoneNumber,
        "mobile" => $faker->phoneNumber,
        "title" => $faker->realText(10),
        "message" => $faker->realText(80),

    ];
});