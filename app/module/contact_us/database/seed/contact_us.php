<?php 
namespace App\module\contact_us\database\seed;

use Illuminate\Database\Seeder;
use App\module\contact_us\model\ContactUs as mContactUs;

class contact_us extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mContactUs::insert([

[


    "user_name"=>'',
    "phone"=>'',
"mobile"=>'',
    "title"=>'',
    "message"=>'',

],
]);
*/


        factory(mContactUs::class,30)->create();
    }
}
