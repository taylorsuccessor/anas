<?php namespace App\module\contact_us\admin\repository;

use Session;
use App\module\contact_us\model\ContactUs;
use App\module\contact_us\admin\Repository\ContactUsContract;

class EloquentContactUsRepository implements ContactUsContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new ContactUs();


//if(canAccess('admin.contact_us.allData')) {
//
//}elseif(canAccess('admin.contact_us.groupData')){
//$oResults = $oResults->where('contact_us.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.contact_us.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('contact_us.id', '=' , $data['id']);
        }
        if (isset($data['user_name']) && !empty($data['user_name'])) {
            $oResults = $oResults->where('contact_us.user_name', '=' , $data['user_name']);
        }
        if (isset($data['phone']) && !empty($data['phone'])) {
            $oResults = $oResults->where('contact_us.phone', '=' , $data['phone']);
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $oResults = $oResults->where('contact_us.title', '=' , $data['title']);
        }
        if (isset($data['message']) && !empty($data['message'])) {
            $oResults = $oResults->where('contact_us.message', '=' , $data['message']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('contact_us.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('contact_us.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('contact_us.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('contact_us.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new ContactUs();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name;
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = ContactUs::create($data);

        if ($result) {
            Session::flash('flash_message', 'contact_us added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$contact_us = ContactUs::findOrFail($id);

        return $contact_us;
    }

    public function destroy($id)
    {

        $result =  ContactUs::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'contact_us deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$contact_us = ContactUs::findOrFail($id);
       $result= $contact_us->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'contact_us updated!');
            return true;
        } else {
            return false;
        }

    }

}
