@extends('admin.layout::main')

@section('title', trans('general.contact_us'))
@section('content')


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.contact_us') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <ol class="breadcrumb pull-left">
                        <li><a href="#">{{ trans('general.contact_us') }}</a></li>
                        <li class="active">{{ trans('contact_us::contact_us.editcontact_us') }}</li>
                    </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>



            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('contact_us::contact_us.contact_us') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('contact_us::contact_us.editcontact_us') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">


                                {!! Form::model($contact_us, [
                                    'method' => 'PATCH',
                                    'url' => ['/admin/contact_us', $contact_us->id],
                                    'class' => 'form-horizontal'
                                ]) !!}







                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('contact_us::contact_us.editcontact_us') }}</span>
                                    </div>

                                    <div class="panel-body">







                                        <div class="row">

                                            <div class="form-group {{ $errors->has('user_name') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('user_name', trans('contact_us::contact_us.user_name'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('user_name', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('phone', trans('contact_us::contact_us.phone'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                        </div>
                                        <div class="row">
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('title', trans('contact_us::contact_us.title'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('mobile', trans('contact_us::contact_us.mobile'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">


                                            <div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('message', trans('contact_us::contact_us.message'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('message', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>





                                        <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                {!! Form::submit(trans('general.edit'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                    </div>
                </div>

            </div>





        </div>
    </div>
@endsection