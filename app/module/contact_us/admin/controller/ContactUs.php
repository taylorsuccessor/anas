<?php

namespace App\module\contact_us\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\contact_us\admin\request\createRequest;
use App\module\contact_us\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\contact_us\model\ContactUs as mContactUs;
use App\module\contact_us\admin\repository\ContactUsContract as rContactUs;



class ContactUs extends Controller
{
private $rContactUs;

public function __construct(rContactUs $rContactUs)
{
$this->rContactUs=$rContactUs;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rContactUs->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.contact_us::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'user_name'=>  $oResult->user_name,

    'phone'=>  $oResult->phone,

    'title'=>  $oResult->title,

    'message'=>  $oResult->message,

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.contact_us::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rContactUs->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/contact_us');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request)
{


$contact_us=$this->rContactUs->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$contact_us],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['contact_us_id'=>$id,'page_name'=>'page']);



return view('admin.contact_us::show', compact('contact_us','request'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$contact_us=$this->rContactUs->show($id);


return view('admin.contact_us::edit', compact('contact_us'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rContactUs->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.contact_us.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$contact_us=$this->rContactUs->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.contact_us.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
