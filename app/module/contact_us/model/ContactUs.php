<?php namespace App\module\contact_us\model;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = [
       "id","user_name","phone","mobile","title","message","created_at","updated_at"    ];
    protected $table='contact_us';

    public $timestamps =true ;

    protected $guarded = [];








}
