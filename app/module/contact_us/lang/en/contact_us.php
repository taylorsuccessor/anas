<?php
return [


'contact_us'=>'Contact Us',
'contact_usCreate'=>'Create Contact Us ',
    'addcontact_us'=>'Add Contact Us',
    'editcontact_us'=>'Edit Contact Us',
    'contact_usInfo'=>'Contact Us Info',

    'contact_usTableHead'=>'Contact Us Table Header',
    'contact_usTableDescription'=>'Contact Us Description',


    'id'=>'Id',


    'user_name'=>'User Name',


    'phone'=>'Phone',
    'mobile'=>'Mobile',


    'title'=>'Title',


    'message'=>'Message',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
