<?php
return [


    'contact_us'=>'اتصل بنا',
    'contact_usCreate'=>' انشاء طلب ',
    'addcontact_us'=>'اضافة طلب',
    'editcontact_us'=>'تعديل طلب',
    'contact_usInfo'=>'معلومات الطلب',

    'contact_usTableHead'=>'الطلبات',
    'contact_usTableDescription'=>'قائمة الطلبات',


    'id'=>'الرقم',


    'user_name'=>'اسم المستخدم',


    'phone'=>'رقم الهاتف',
    'mobile'=>' الموبايل',


    'title'=>'عنوان الطلب',


    'message'=>'الرسالة',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
