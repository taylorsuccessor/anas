<?php
namespace App\module\contact_us\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/contact_us/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@phone','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->clear('@user_name')
    ->click('@submit')
    ->assertSee('user_name field is required');



    $this->browser
        ->type('@user_name','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->clear('@phone')
    ->click('@submit')
    ->assertSee('phone field is required');



    $this->browser
        ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@message','9')
    
    ->clear('@title')
    ->click('@submit')
    ->assertSee('title field is required');



    $this->browser
        ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@title','9')
    
    ->clear('@message')
    ->click('@submit')
    ->assertSee('message field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@user_name','9')
            ->type('@phone','9')
            ->type('@title','9')
            ->type('@message','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@user_name"=>"[name=user_name]",

    "@phone"=>"[name=phone]",

    "@title"=>"[name=title]",

    "@message"=>"[name=message]",


            '@submit'=>'[type=submit]'
        ];
    }
}
