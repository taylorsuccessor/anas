<?php
namespace App\module\contact_us\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\contact_us\model\ContactUs;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/contact_us';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=ContactUs::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@user_name',$result->user_name)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->user_name);


        $browser->click('@searchTabButton');

       $browser->type('@phone',$result->phone)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->phone);


        $browser->click('@searchTabButton');

       $browser->type('@title',$result->title)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->title);


        $browser->click('@searchTabButton');

       $browser->type('@message',$result->message)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->message);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@user_name"=>"[name=user_name]",

    "@phone"=>"[name=phone]",

    "@title"=>"[name=title]",

    "@message"=>"[name=message]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
