<?php
namespace App\module\contact_us\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/contact_us/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@user_name')
    ->click('@submit')
    ->assertSee('user_name field is required');


    $this->browser
    ->clear('@phone')
    ->click('@submit')
    ->assertSee('phone field is required');


    $this->browser
    ->clear('@title')
    ->click('@submit')
    ->assertSee('title field is required');


    $this->browser
    ->clear('@message')
    ->click('@submit')
    ->assertSee('message field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@user_name')
    ->type('@user_name','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@phone')
    ->type('@phone','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@title')
    ->type('@title','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@message')
    ->type('@message','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@user_name"=>"[name=user_name]",
             
             "@phone"=>"[name=phone]",
             
             "@title"=>"[name=title]",
             
             "@message"=>"[name=message]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
