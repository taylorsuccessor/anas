<?php
return [


    'user'=>'المستخدمين',
    'userCreate'=>'انشاء مستخدم ',
    'adduser'=>'اضافة مستخدم',
    'edituser'=>'تعديل المستخدم',
    'userInfo'=>'معلومات المستخدم',

    'userTableHead'=>'المستخدمين',
    'userTableDescription'=>'قائمة المستخدمين',


    'id'=>'الرقم',


    'email'=>'البريد',


    'guest_email'=>'البريد الاضافي',


    'password'=>'كلمة السر',


    'android_device_id'=>'رقم جهاز الاندرويد',


    'ios_device_id'=>'رقم جهاز آيفون',


    'last_login'=>'اخر دخول',


    'first_name'=>'اسم العميل',


    'last_name'=>'اسم المؤسسة',

    'first_name_text'=>' الاسم الأول',


    'last_name_text'=>'الاسم الأخير',


    'birth_day'=>'تاريخ الميلاد',


    'avatar'=>'البديل',


    'phone'=>'رقم الهاتف',


    'mobile'=>'رقم الموبايل الإضافي',


    'area_id'=>'المنطقة',


    'country'=>'الدولة',


    'address'=>'العنوان',


    'gender'=>'الجنس',


    'occupation'=>'العمل',


    'type'=>'النوع',


    'session_id'=>'رقم الجلسة',


    'lat'=>'خطوط العرض',


    'long'=>'خطوط الطول',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',
    'password_confirmation'=>"تأكيد كلمة السر",

    'company_type_id'=>'نوع الشركة',
    'company_type'=>'نوع الشركة',
    'driverMap'=>'خريطة السائقين',

    'status'=>' الحالة ',
    'active'=>'  النشاط ',
];
