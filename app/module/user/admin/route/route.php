<?php
Route::group(['middleware' => ['authorization'],'prefix' => 'admin','as'=>'admin.', 'namespace' => '\App\module\user\admin\controller'], function () {

    Route::get('user/map','User@getMap');
    Route::resource('user','User');

    Route::get('user/report/customer','User@customerReport')->name('user.customerReport');
    Route::get('user/report/driver','User@driverReport')->name('user.driverReport');
    Route::get('user/report/admin','User@adminReport')->name('user.adminReport');

});


Route::get('/selectAddressFromMap','\App\module\user\admin\controller\User@selectAddressFromMap');
