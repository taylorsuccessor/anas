<?php

namespace App\module\user\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\user\admin\request\createRequest;
use App\module\user\admin\request\editRequest;

use Session;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\user\model\User as mUser;
use App\module\user\admin\repository\UserContract as rUser;
use App\module\company_type\admin\repository\CompanyTypeContract as rCompanyType;


use App\module\role\admin\repository\RoleContract as rRole;
use App\module\area\admin\repository\AreaContract as rArea;

use Illuminate\Http\JsonResponse;


class User extends Controller
{
    private $rUser;
    private $rRole;

    public function __construct(rUser $rUser,rRole $rRole)
    {
        $this->rUser=$rUser;
        $this->rRole=$rRole;
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        $statistic=null;
        $oResults=$this->rUser->getByFilter($request,$statistic);

        return view('admin.user::index', compact('oResults','request','statistic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request,rCompanyType $rCompanyType,rArea $rArea)
    {
        $roleList=$this->rRole->getAllList();
        $companyTypeList=$rCompanyType->getAllList();
        $areaList=$rArea->getAllList();
        return view('admin.user::create',compact('request','roleList','companyTypeList','areaList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {



        $request->merge([ 'password' => bcrypt($request->password)]);
        $oResults=$this->rUser->create($request->all());

        $this->rRole->assignUserRole($oResults->id,[1]);
        if($oResults  && isset($request->role) && count($request->role)){
            $this->rRole->assignUserRole($oResults->id,$request->role);
        }

        notification('new_user',['user'=>$oResults,'new_user_email'=>$oResults->email]);

        $replaceUserByAdminEmail=clone $oResults;
        $replaceUserByAdminEmail->email=config('module.admin_email');
        notification('new_user',['user'=>$replaceUserByAdminEmail,'new_user_email'=>$oResults->email]);

        if(!isset($request->ajaxRequest)){
            return redirect('admin/user');
        }


        return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return view
     */
    public function show($id,Request $request)
    {


        $user=$this->rUser->show($id);

        if(isset($request->ajaxRequest)){
            return new JsonResponse(['status'=>'success','data'=>$user],200,[],JSON_UNESCAPED_UNICODE);
        }

        $request->merge(['user_id'=>$id,'page_name'=>'page']);



        return view('admin.user::show', compact('user','request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return view
     */
    public function edit($id,rCompanyType $rCompanyType,rArea $rArea)
    {


        $user=$this->rUser->show($id);



        $roleList=$this->rRole->getAllList();
        $userRoleList=$user->role_list();
        $companyTypeList=$rCompanyType->getAllList();

        $areaList=$rArea->getAllList();
        return view('admin.user::edit', compact('user','roleList','userRoleList','companyTypeList','areaList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {
        if($request->has('password') && $request->password !=''){

            $request->merge([ 'password' => bcrypt($request->password)]);
        }elseif($request->has('password') && $request->password ==''){

            $request->request->remove('password');
        }

        $result=$this->rUser->update($id,$request);
        if($result  && isset($request->role) && count($request->role)){
            $this->rRole->assignUserRole($id,$request->role);
        }


        if(!isset($request->ajaxRequest)){
            return redirect(route('admin.user.index'));
        }


        return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return redirect
     */
    public function destroy($id)
    {
        $user=$this->rUser->destroy($id);
        return redirect(route('admin.user.index'));
    }

    /*
     * customer reports
     * @params Request
     * @return view
     */
    public function customerReport(Request $request)
    {

        $statistic=null;

        $this->rUser->exportExcel($request,'Customers');

        if(isset($request->print)){
            return view('admin.user::printExcel',['data'=>$this->rUser->toArray($this->rUser->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }

        $oResults=$this->rUser->getByFilter($request,$statistic);

        return view('admin.user::customerReport', compact('oResults','request','statistic'));
    }
    /*
     * driver reports
     * @params Request
     * @return view
     */
    public function driverReport(Request $request)
    {

        $statistic=null;
        $this->rUser->exportExcel($request,'Drivers');
        if(isset($request->print)){
            return view('admin.user::printExcel',['data'=>$this->rUser->toArray($this->rUser->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }
        $oResults=$this->rUser->getByFilter($request,$statistic);

        return view('admin.user::driverReport', compact('oResults','request','statistic'));
    }
    /*
     * admin reports
     * @params Request
     * @return view
     */
    public function adminReport(Request $request)
    {

        $statistic=null;
        $this->rUser->exportExcel($request,'Admin');
        if(isset($request->print)){
            return view('admin.user::printExcel',['data'=>$this->rUser->toArray($this->rUser->getByFilter($request->all()+['getAllRecords'=>1]))]);
        }
        $oResults=$this->rUser->getByFilter($request,$statistic);

        return view('admin.user::adminReport', compact('oResults','request','statistic'));
    }



    public function getMap( Request $request) {



        $fromPointList = $this->rUser->getByFilter(['type'=>config('array.user_type_driver_index')]);


        return view('admin.user::map',
            compact('request', 'fromPointList'));

    }


    public function selectAddressFromMap(){
        return view('admin.user::selectAddressFromMap');
    }




}
