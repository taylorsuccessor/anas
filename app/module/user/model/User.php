<?php namespace App\module\user\model;

//use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        "id","email","guest_email","password","android_device_id","ios_device_id","last_login","first_name","last_name","birth_day","avatar","phone","mobile","area_id","country","address","gender","occupation","type","company_type_id","token","session_id","lat","long","status","created_at","updated_at"    ];
    protected $table='user';

    public $timestamps =true ;

    protected $guarded = [];


    public function role(){
        return $this->hasManyThrough('App\module\role\model\Role','App\module\role\model\RoleUser','user_id','id','id','role_id');
    }

    public function role_user(){
        return $this->hasMany('App\module\role\model\RoleUser');
    }

    public function role_list(){
        $oRoleList=$this->role_user()->with('role')->get();
        $aRoleList=[];
        if(count($oRoleList)) {
            foreach ($oRoleList as $oneRoleUser) {
                if(isset($oneRoleUser->role->name)){
                    $aRoleList[$oneRoleUser->role->id]=$oneRoleUser->role->name;
                }
        }
        }

        return $aRoleList;
    }

    public function sendPasswordResetNotification($token)
    {
        notification('reset_password',[
            'user'=>$this,
            'token'=>$token
            ]);
        \Session::flash('flash_success','Please check your email Inbox to reset Password.');
    }

    public function area(){
        return $this->belongsTo('App\module\area\model\Area');
    }

    public function area_name(){
        return isset($this->area->id)? $this->area->{'name_'.session('locale')}:'';
    }
    public function company_type(){
        return $this->belongsTo('App\module\company_type\model\CompanyType');
    }

    public function company_type_name(){
        return isset($this->company_type->id)? $this->company_type->{'name_'.session('locale')}:'';
    }

    public function status(){
        return array_key_exists($this->status,trans('array.user_status'))? trans('array.user_status')[$this->status]:'';
    }
    public function type(){
        return array_key_exists($this->type,trans('array.user_type'))? trans('array.user_type')[$this->type]:'';
    }

}
