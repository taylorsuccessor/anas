<?php
use App\module\user\model\User;

$locations=[
    ['31.961418','35.904762'],
    ['31.969573','35.910599'],
    ['31.967243','35.880043'],
    ['31.963166','35.906994'],
    ['31.953845','35.874035'],
    ['31.944231','35.903389'],
    ['31.952971','35.915062'],
    ['31.942192','35.900986'],
    ['31.962437','35.879013'],
    ['31.950640','35.916435'],
    ['31.970156','35.874550'],
    ['31.947727','35.874550'],

    ['31.961418','35.921928'],
];

$factory->define(User::class,function(Faker\Generator $faker) use ($locations){
    $latLong=$faker->randomElement($locations);
    return [

            "email"=>$faker->email,
    "guest_email"=>$faker->email,
    "password"=>bcrypt('123456'),
    "android_device_id"=>$faker->sha1,
    "ios_device_id"=>$faker->sha1,
    "last_login"=>$faker->dateTime,
    "first_name"=>$faker->name,
    "last_name"=>$faker->name,
    "birth_day"=>$faker->date('Y-m-d','now'),
    "avatar"=>$faker->randomDigit,
    "phone"=>$faker->phoneNumber,
    "mobile"=>$faker->phoneNumber,
    "area_id"=>$faker->randomDigit,
    "country"=>$faker->randomDigit,
    "address"=>$faker->text(50),
    "gender"=>$faker->numberBetween(0,1),
    "occupation"=>$faker->text(10),
    "type"=>$faker->numberBetween(0,10),
        "company_type_id"=>$faker->numberBetween(0,40),
    "session_id"=>$faker->sha1,
    "lat"=>$latLong[0],
    "long"=>$latLong[1],
        "status"=>$faker->numberBetween(0,1),

    ];
});