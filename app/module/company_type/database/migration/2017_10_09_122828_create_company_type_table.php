<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('company_type', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_en');

            $table->string('name_ar');

            $table->string('active')->default(0);


            $table->timestamps();
        });

//        Schema::table('company_type', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('company_type');
    }
}
