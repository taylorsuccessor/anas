<?php 
namespace App\module\company_type\database\seed;

use Illuminate\Database\Seeder;
use App\module\company_type\model\CompanyType as mCompanyType;

class company_type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mCompanyType::insert([

[


    "name_en"=>'',
    "name_ar"=>'',
    "active"=>'',

],
]);
*/


        factory(mCompanyType::class,30)->create();
    }
}
