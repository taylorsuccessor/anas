<?php


$factory->define(App\module\company_type\model\CompanyType::class,function (Faker\Generator $faker){

    return [

            "name_en"=>$faker->randomElement([' global','national','arabic']),
    "name_ar"=>$faker->randomElement([ ' عالمي ',' محلي ',' وطني ']),
    "active"=>$faker->randomElement([0,1]),

    ];
});