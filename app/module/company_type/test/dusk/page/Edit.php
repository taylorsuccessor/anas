<?php
namespace App\module\company_type\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/company_type/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@name_en')
    ->click('@submit')
    ->assertSee('name_en field is required');


    $this->browser
    ->clear('@name_ar')
    ->click('@submit')
    ->assertSee('name_ar field is required');


    $this->browser
    ->clear('@active')
    ->click('@submit')
    ->assertSee('active field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@name_en')
    ->type('@name_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@name_ar')
    ->type('@name_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@active')
    ->type('@active','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@name_en"=>"[name=name_en]",
             
             "@name_ar"=>"[name=name_ar]",
             
             "@active"=>"[name=active]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
