<?php
return [


'company_type'=>'Company Type',
'company_typeCreate'=>'Create Company Type ',
    'addcompany_type'=>'Add Company Type',
    'editcompany_type'=>'Edit Company Type',
    'company_typeInfo'=>'Company Type Info',

    'company_typeTableHead'=>'Company Type Table Header',
    'company_typeTableDescription'=>'Company Type Description',


    'id'=>'Id',


    'name_en'=>'Name En',


    'name_ar'=>'Name Ar',


    'active'=>'Active',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
