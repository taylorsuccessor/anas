<?php
return [


    'company_type'=>'أنواع الشركات',
    'company_typeCreate'=>'انشاء نوع الشركة ',
    'addcompany_type'=>'اضافة نوع للشركات',
    'editcompany_type'=>'تعديل نوع الشركة',
    'company_typeInfo'=>'معلومات انواع الشركات',

    'company_typeTableHead'=>'انواع الشركات',
    'company_typeTableDescription'=>'قائمة انواع الشركات',


    'id'=>'الرقم',


    'name_en'=>' الاسم الانكليزي',


    'name_ar'=>'الاسم العربي',


    'active'=>'نشط',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
