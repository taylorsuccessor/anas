<?php namespace App\module\company_type\model;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    protected $fillable = [
       "id","name_en","name_ar","active","created_at","updated_at"    ];
    protected $table='company_type';

    public $timestamps =true ;

    protected $guarded = [];




public function user(){
return $this->hasMany('App\module\user\model\User');
}


    public function name(){
        return $this->{'name_'.session('locale')};
    }


    public function active(){
        return array_key_exists($this->active,trans('array.active'))? trans('array.active')[$this->active]:'';
    }





}
