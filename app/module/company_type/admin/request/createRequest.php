<?php
namespace App\module\company_type\admin\request;

use Illuminate\Foundation\Http\FormRequest;

class createRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name_en" => 'required',
            "name_ar" => 'required',
            "active" => 'required',


        ];
    }

//    public function messages()
//    {
//        return [
//            'name_en.required' => trans('validation.englishNameRequired'),
//            'name_ar.required' =>  trans('validation.englishNameRequired'),
//            'active.required' =>  trans('validation.activeRequired'),
//        ];
//    }
}
