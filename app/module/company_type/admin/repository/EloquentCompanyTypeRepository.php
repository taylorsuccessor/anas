<?php namespace App\module\company_type\admin\repository;

use Session;
use App\module\company_type\model\CompanyType;
use App\module\company_type\admin\Repository\CompanyTypeContract;

class EloquentCompanyTypeRepository implements CompanyTypeContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new CompanyType();


//if(canAccess('admin.company_type.allData')) {
//
//}elseif(canAccess('admin.company_type.groupData')){
//$oResults = $oResults->where('company_type.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.company_type.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('company_type.id', '=' , $data['id']);
        }
        if (isset($data['name_en']) && !empty($data['name_en'])) {
            $oResults = $oResults->where('company_type.name_en', '=' , $data['name_en']);
        }
        if (isset($data['name_ar']) && !empty($data['name_ar'])) {
            $oResults = $oResults->where('company_type.name_ar', '=' , $data['name_ar']);
        }
        if (isset($data['active']) &&  $data['active']!='') {
            $oResults = $oResults->where('company_type.active', '=' , $data['active']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('company_type.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('company_type.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('company_type.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('company_type.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new CompanyType();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name();
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = CompanyType::create($data);

        if ($result) {
            Session::flash('flash_message', 'company_type added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$company_type = CompanyType::findOrFail($id);

        return $company_type;
    }

    public function destroy($id)
    {

        $result =  CompanyType::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'company_type deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$company_type = CompanyType::findOrFail($id);
       $result= $company_type->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'company_type updated!');
            return true;
        } else {
            return false;
        }

    }

}
