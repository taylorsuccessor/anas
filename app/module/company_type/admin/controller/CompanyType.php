<?php

namespace App\module\company_type\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\company_type\admin\request\createRequest;
use App\module\company_type\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\company_type\model\CompanyType as mCompanyType;
use App\module\company_type\admin\repository\CompanyTypeContract as rCompanyType;


        use App\module\user\admin\repository\UserContract as rUser;

class CompanyType extends Controller
{
private $rCompanyType;

public function __construct(rCompanyType $rCompanyType)
{
$this->rCompanyType=$rCompanyType;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rCompanyType->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.company_type::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'name'=>  $oResult->name(),


    'active'=>  $oResult->active(),

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.company_type::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rCompanyType->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/company_type');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request,rUser $rUser)
{


$company_type=$this->rCompanyType->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$company_type],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['company_type_id'=>$id,'page_name'=>'page']);


    $request->page_name='page_user';
    $oUserResults=$rUser->getByFilter($request);

return view('admin.company_type::show', compact('company_type','request','oUserResults'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$company_type=$this->rCompanyType->show($id);


return view('admin.company_type::edit', compact('company_type'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rCompanyType->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.company_type.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$company_type=$this->rCompanyType->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.company_type.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
