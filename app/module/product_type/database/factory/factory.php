<?php


$factory->define(App\module\product_type\model\ProductType::class,function (Faker\Generator $faker){

    return [

            "name_en"=>$faker->randomElement([ ' package ',' one element ',' KG  ']).' - '.$faker->realText(10),
    "name_ar"=>$faker->randomElement([ ' ?????? ',' ???? ',' ???? ']).' - '.$faker->realText(10),

    ];
});