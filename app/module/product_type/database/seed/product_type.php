<?php 
namespace App\module\product_type\database\seed;

use Illuminate\Database\Seeder;
use App\module\product_type\model\ProductType as mProductType;

class product_type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mProductType::insert([

[


    "name_en"=>'',
    "name_ar"=>'',

],
]);
*/


        factory(mProductType::class,30)->create();
    }
}
