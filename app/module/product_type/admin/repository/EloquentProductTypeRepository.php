<?php namespace App\module\product_type\admin\repository;

use Session;
use App\module\product_type\model\ProductType;
use App\module\product_type\admin\Repository\ProductTypeContract;

class EloquentProductTypeRepository implements ProductTypeContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new ProductType();


//if(canAccess('admin.product_type.allData')) {
//
//}elseif(canAccess('admin.product_type.groupData')){
//$oResults = $oResults->where('product_type.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.product_type.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('product_type.id', '=' , $data['id']);
        }
        if (isset($data['name_en']) && !empty($data['name_en'])) {
            $oResults = $oResults->where('product_type.name_en', '=' , $data['name_en']);
        }
        if (isset($data['name_ar']) && !empty($data['name_ar'])) {
            $oResults = $oResults->where('product_type.name_ar', '=' , $data['name_ar']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('product_type.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('product_type.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('product_type.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('product_type.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new ProductType();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name();
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = ProductType::create($data);

        if ($result) {
            Session::flash('flash_message', 'product_type added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$product_type = ProductType::findOrFail($id);

        return $product_type;
    }

    public function destroy($id)
    {

        $result =  ProductType::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'product_type deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$product_type = ProductType::findOrFail($id);
       $result= $product_type->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'product_type updated!');
            return true;
        } else {
            return false;
        }

    }

}
