<?php

namespace App\module\product_type\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\product_type\admin\request\createRequest;
use App\module\product_type\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\product_type\model\ProductType as mProductType;
use App\module\product_type\admin\repository\ProductTypeContract as rProductType;


        use App\module\product\admin\repository\ProductContract as rProduct;

class ProductType extends Controller
{
private $rProductType;

public function __construct(rProductType $rProductType)
{
$this->rProductType=$rProductType;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rProductType->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.product_type::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'name_en'=>  $oResult->name_en,

    'name_ar'=>  $oResult->name_ar,

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.product_type::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rProductType->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/product_type');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request,rProduct $rProduct)
{


$product_type=$this->rProductType->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$product_type],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['product_type_id'=>$id,'page_name'=>'page']);


    $request->page_name='page_product';
    $oProductResults=$rProduct->getByFilter($request);

return view('admin.product_type::show', compact('product_type','request','oProductResults'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$product_type=$this->rProductType->show($id);


return view('admin.product_type::edit', compact('product_type'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rProductType->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.product_type.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$product_type=$this->rProductType->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.product_type.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
