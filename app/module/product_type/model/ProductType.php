<?php namespace App\module\product_type\model;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = [
       "id","name_en","name_ar","created_at","updated_at"    ];
    protected $table='product_type';

    public $timestamps =true ;

    protected $guarded = [];




public function product(){
return $this->hasMany('App\module\product\model\Product');
}


    public function name(){
        return $this->{'name_'.session('locale')};
    }




}
