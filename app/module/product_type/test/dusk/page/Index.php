<?php
namespace App\module\product_type\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\product_type\model\ProductType;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/product_type';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=ProductType::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@name_en',$result->name_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->name_en);


        $browser->click('@searchTabButton');

       $browser->type('@name_ar',$result->name_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->name_ar);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@name_en"=>"[name=name_en]",

    "@name_ar"=>"[name=name_ar]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
