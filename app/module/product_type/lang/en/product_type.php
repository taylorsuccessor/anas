<?php
return [


'product_type'=>'Product Type',
'product_typeCreate'=>'Create Product Type ',
    'addproduct_type'=>'Add Product Type',
    'editproduct_type'=>'Edit Product Type',
    'product_typeInfo'=>'Product Type Info',

    'product_typeTableHead'=>'Product Type Table Header',
    'product_typeTableDescription'=>'Product Type Description',


    'id'=>'Id',


    'name_en'=>'Name En',


    'name_ar'=>'Name Ar',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
