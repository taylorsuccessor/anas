<?php
return [

'name'=>'Name',
    'id'=>'Id',
    'search'=>'Search',
    'showing'=>'Showing',
    'to'=>'to',
    'of'=>'of',
    'entries'=>'entries',

'dashboard'=>'Dashboard',

'CopyRights'=>'2016 &copy; Elite Admin brought to you by themedesigner.in',

'language'=>'Language',


    'user'=>'User',
    'userList'=>'User List',
    'userCreate'=>'Create User ',
    'userMenuBottomHeader'=>'UserMenu Bottom Header',
    'userMenuBottomDescription'=>'User Menu Bottom Description',



    'category'=>'Category',
    'categoryList'=>'Category List',
    'categoryCreate'=>'Create Category ',
    'categoryMenuBottomHeader'=>'CategoryMenu Bottom Header',
    'categoryMenuBottomDescription'=>'Category Menu Bottom Description',



    'product'=>'Product',
    'productList'=>'Product List',
    'productCreate'=>'Create Product ',
    'productMenuBottomHeader'=>'ProductMenu Bottom Header',
    'productMenuBottomDescription'=>'Product Menu Bottom Description',



    'product_type'=>'Product Type',
    'product_typeList'=>'Product Type List',
    'product_typeCreate'=>'Create Product Type ',
    'product_typeMenuBottomHeader'=>'Product TypeMenu Bottom Header',
    'product_typeMenuBottomDescription'=>'Product Type Menu Bottom Description',



    'area'=>'Area',
    'areaList'=>'Area List',
    'areaCreate'=>'Create Area ',
    'areaMenuBottomHeader'=>'AreaMenu Bottom Header',
    'areaMenuBottomDescription'=>'Area Menu Bottom Description',



    'company_type'=>'Company Type',
    'company_typeList'=>'Company Type List',
    'company_typeCreate'=>'Create Company Type ',
    'company_typeMenuBottomHeader'=>'Company TypeMenu Bottom Header',
    'company_typeMenuBottomDescription'=>'Company Type Menu Bottom Description',



    'order'=>'Order',
    'orderList'=>'Order List',
    'orderCreate'=>'Create Order ',
    'orderMenuBottomHeader'=>'OrderMenu Bottom Header',
    'orderMenuBottomDescription'=>'Order Menu Bottom Description',



    'cart'=>'Cart',
    'cartList'=>'Cart List',
    'cartCreate'=>'Create Cart ',
    'cartMenuBottomHeader'=>'CartMenu Bottom Header',
    'cartMenuBottomDescription'=>'Cart Menu Bottom Description',



    'faq'=>'Faq',
    'faqList'=>'Faq List',
    'faqCreate'=>'Create Faq ',
    'faqMenuBottomHeader'=>'FaqMenu Bottom Header',
    'faqMenuBottomDescription'=>'Faq Menu Bottom Description',



    'contact_us'=>'Contact Us',
    'contact_usList'=>'Contact Us List',
    'contact_usCreate'=>'Create Contact Us ',
    'contact_usMenuBottomHeader'=>'Contact UsMenu Bottom Header',
    'contact_usMenuBottomDescription'=>'Contact Us Menu Bottom Description',



    'cms'=>'Cms',
    'cmsList'=>'Cms List',
    'cmsCreate'=>'Create Cms ',
    'cmsMenuBottomHeader'=>'CmsMenu Bottom Header',
    'cmsMenuBottomDescription'=>'Cms Menu Bottom Description',



    'advertise'=>'Advertise',
    'advertiseList'=>'Advertise List',
    'advertiseCreate'=>'Create Advertise ',
    'advertiseMenuBottomHeader'=>'AdvertiseMenu Bottom Header',
    'advertiseMenuBottomDescription'=>'Advertise Menu Bottom Description',



    'communication'=>'Communication',
    'communicationList'=>'Communication List',
    'communicationCreate'=>'Create Communication ',
    'communicationMenuBottomHeader'=>'CommunicationMenu Bottom Header',
    'communicationMenuBottomDescription'=>'Communication Menu Bottom Description',



];
