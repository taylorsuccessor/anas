<?php
return [


    'product_type'=>'نوع المنتج',
    'product_typeCreate'=>'انشاء نوع المنتج ',
    'addproduct_type'=>'اضافة نوع للمنتج',
    'editproduct_type'=>'تعديل نوع المنتج',
    'product_typeInfo'=>'معلومات نوع المنتج',

    'product_typeTableHead'=>'أنواع المنتجات',
    'product_typeTableDescription'=>'قائمة أنواع المنتجات',


    'id'=>'الرقم',


    'name_en'=>'اسم نوع المنتج',


    'name_ar'=>'الاسم العربي',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
