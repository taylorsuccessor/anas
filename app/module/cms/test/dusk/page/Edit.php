<?php
namespace App\module\cms\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/cms/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@title_en')
    ->click('@submit')
    ->assertSee('title_en field is required');


    $this->browser
    ->clear('@title_ar')
    ->click('@submit')
    ->assertSee('title_ar field is required');


    $this->browser
    ->clear('@body_en')
    ->click('@submit')
    ->assertSee('body_en field is required');


    $this->browser
    ->clear('@body_ar')
    ->click('@submit')
    ->assertSee('body_ar field is required');


    $this->browser
    ->clear('@slug')
    ->click('@submit')
    ->assertSee('slug field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@title_en')
    ->type('@title_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@title_ar')
    ->type('@title_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@body_en')
    ->type('@body_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@body_ar')
    ->type('@body_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@slug')
    ->type('@slug','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@title_en"=>"[name=title_en]",
             
             "@title_ar"=>"[name=title_ar]",
             
             "@body_en"=>"[name=body_en]",
             
             "@body_ar"=>"[name=body_ar]",
             
             "@slug"=>"[name=slug]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
