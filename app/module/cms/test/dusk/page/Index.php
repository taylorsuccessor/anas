<?php
namespace App\module\cms\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\cms\model\Cms;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/cms';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Cms::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@title_en',$result->title_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->title_en);


        $browser->click('@searchTabButton');

       $browser->type('@title_ar',$result->title_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->title_ar);


        $browser->click('@searchTabButton');

       $browser->type('@body_en',$result->body_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->body_en);


        $browser->click('@searchTabButton');

       $browser->type('@body_ar',$result->body_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->body_ar);


        $browser->click('@searchTabButton');

       $browser->type('@slug',$result->slug)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->slug);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@title_en"=>"[name=title_en]",

    "@title_ar"=>"[name=title_ar]",

    "@body_en"=>"[name=body_en]",

    "@body_ar"=>"[name=body_ar]",

    "@slug"=>"[name=slug]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
