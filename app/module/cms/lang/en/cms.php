<?php
return [


'cms'=>'Cms',
'cmsCreate'=>'Create Cms ',
    'addcms'=>'Add Cms',
    'editcms'=>'Edit Cms',
    'cmsInfo'=>'Cms Info',

    'cmsTableHead'=>'Cms Table Header',
    'cmsTableDescription'=>'Cms Description',


    'id'=>'Id',


    'title_en'=>'Title En',


    'title_ar'=>'Title Ar',


    'body_en'=>'Body En',


    'body_ar'=>'Body Ar',


    'slug'=>'Slug',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
