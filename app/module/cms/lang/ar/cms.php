<?php
return [


    'cms'=>'محتوى الصفحات',
    'cmsCreate'=>'انشاء صفحة ',
    'addcms'=>'اضافة صفحة',
    'editcms'=>'تعديل الصفحة',
    'cmsInfo'=>'معلومات الصفحة',

    'cmsTableHead'=>'الصفحات',
    'cmsTableDescription'=>'تعديل الصفحات',


    'id'=>'الرقم',


    'title_en'=>'العنوان',


    'title_ar'=>'العنوان العربي',


    'body_en'=>'المحتوى',


    'body_ar'=>'المحتوى العربي',


    'slug'=>'الاسم المميز',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
