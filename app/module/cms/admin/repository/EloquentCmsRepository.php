<?php namespace App\module\cms\admin\repository;

use Session;
use App\module\cms\model\Cms;
use App\module\cms\admin\Repository\CmsContract;

class EloquentCmsRepository implements CmsContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Cms();


//if(canAccess('admin.cms.allData')) {
//
//}elseif(canAccess('admin.cms.groupData')){
//$oResults = $oResults->where('cms.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.cms.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('cms.id', '=' , $data['id']);
        }
        if (isset($data['title_en']) && !empty($data['title_en'])) {
            $oResults = $oResults->where('cms.title_en', 'like' , '%'.$data['title_en'].'%');
        }
        if (isset($data['title_ar']) && !empty($data['title_ar'])) {
            $oResults = $oResults->where('cms.title_ar', 'like' , '%'. $data['title_ar'].'%');
        }
        if (isset($data['body_en']) && !empty($data['body_en'])) {
            $oResults = $oResults->where('cms.body_en', 'like' , '%'.$data['body_en'].'%');
        }
        if (isset($data['body_ar']) && !empty($data['body_ar'])) {
            $oResults = $oResults->where('cms.body_ar', 'like' , '%'.$data['body_ar'].'%');
        }
        if (isset($data['slug']) && !empty($data['slug'])) {
            $oResults = $oResults->where('cms.slug', '=' , $data['slug']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('cms.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('cms.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('cms.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('cms.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Cms();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name;
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Cms::create($data);

        if ($result) {
            Session::flash('flash_message', 'cms added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$cms = Cms::findOrFail($id);

        return $cms;
    }

    public function destroy($id)
    {

        $result =  Cms::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'cms deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$cms = Cms::findOrFail($id);
       $result= $cms->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'cms updated!');
            return true;
        } else {
            return false;
        }

    }

}
