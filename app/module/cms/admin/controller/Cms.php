<?php

namespace App\module\cms\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\cms\admin\request\createRequest;
use App\module\cms\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\cms\model\Cms as mCms;
use App\module\cms\admin\repository\CmsContract as rCms;



class Cms extends Controller
{
private $rCms;

public function __construct(rCms $rCms)
{
$this->rCms=$rCms;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rCms->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.cms::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'title'=>  $oResult->title(),
    'body'=>  $oResult->body(),

    'slug'=>  $oResult->slug,

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.cms::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rCms->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/cms');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request)
{


$cms=$this->rCms->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$cms],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['cms_id'=>$id,'page_name'=>'page']);



return view('admin.cms::show', compact('cms','request'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$cms=$this->rCms->show($id);


return view('admin.cms::edit', compact('cms'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rCms->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.cms.index'));
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$cms=$this->rCms->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.cms.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
