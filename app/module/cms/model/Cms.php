<?php namespace App\module\cms\model;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $fillable = [
       "id","title_en","title_ar","body_en","body_ar","slug","created_at","updated_at"    ];
    protected $table='cms';

    public $timestamps =true ;

    protected $guarded = [];

    public function title(){
        return $this->{'title_'.session('locale')};
    }
    public function body(){
        return $this->{'body_'.session('locale')};
    }








}
