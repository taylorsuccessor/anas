<?php 
namespace App\module\cms\database\seed;

use Illuminate\Database\Seeder;
use App\module\cms\model\Cms as mCms;

class cms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

mCms::insert([

[


    "title_en"=>'About Us',
    "title_ar"=>'About Us',
    "body_en"=>'about us page body english',
    "body_ar"=>'about us page body arabic',
    "slug"=>'about-us',

],
]);



        factory(mCms::class,30)->create();
    }
}
