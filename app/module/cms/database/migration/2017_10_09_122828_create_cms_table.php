<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms', function (Blueprint $table) {
            $table->increments('id');

    $table->text('title_en');

    $table->text('title_ar');

    $table->text('body_en');

    $table->text('body_ar');

    $table->string('slug');


$table->timestamps();
        });

//        Schema::table('cms', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('cms');
    }
}
