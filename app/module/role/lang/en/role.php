<?php
return [


'role'=>'Role',
'roleCreate'=>'Create Role ',
    'addrole'=>'Add Role',
    'editrole'=>'Edit Role',
    'roleInfo'=>'Role Info',

    'roleTableHead'=>'Role Table Header',
    'roleTableDescription'=>'Role Description',


    'id'=>'Id',


    'slug'=>'Slug',


    'name'=>'Name',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
