<?php
return [


    'role'=>'الصلاحيات',
    'roleCreate'=>'انشاء صلاحيات ',
    'addrole'=>'تعديل الصلاحيات',
    'editrole'=>'تعديل الصلاحية',
    'roleInfo'=>'معلومات الصلاحية',

    'roleTableHead'=>'الصلاحيات',
    'roleTableDescription'=>'قاشمة الصلاحيات',


    'id'=>'الرقم',


    'slug'=>'الاسم المميز',


    'name'=>'الاسم',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
