<?php namespace App\module\category\model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
       "id","name_en","name_ar","parent_id","img","active","created_at","updated_at"    ];
    protected $table='category';

    public $timestamps =true ;

    protected $guarded = [];




public function product(){
return $this->hasMany('App\module\product\model\Product');
}


    public function parent(){
        return $this->belongsTo('App\module\category\model\Category','parent_id');
    }

    public function parent_name(){
        return isset($this->parent->id)? $this->parent->name():'';
    }




    public function name(){
        return $this->{'name_'.session('locale')};
    }



    public function active(){
        return array_key_exists($this->active,trans('array.active'))? trans('array.active')[$this->active]:'';
    }




}
