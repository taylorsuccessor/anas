<?php
return [


    'category'=>'الفئات',
    'categoryCreate'=>'انشاء فئة',
    'addcategory'=>'اضافة فئة',
    'editcategory'=>'تعديل فئة',
    'categoryInfo'=>'معلومات الفئة',

    'categoryTableHead'=>'الفئات',
    'categoryTableDescription'=>'قائمة الفئات',


    'id'=>'الرقم',


    'name_en'=>'اسم الفئة',


    'name_ar'=>'الاسم العربي',


    'parent_id'=>'الفئة الرئيسية',


    'img'=>'الصورة',


    'active'=>'نشط',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',
    'mainCategory'=>' فئة أساسية ',



];
