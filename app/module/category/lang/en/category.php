<?php
return [


'category'=>'Category',
'categoryCreate'=>'Create Category ',
    'addcategory'=>'Add Category',
    'editcategory'=>'Edit Category',
    'categoryInfo'=>'Category Info',

    'categoryTableHead'=>'Category Table Header',
    'categoryTableDescription'=>'Category Description',


    'id'=>'Id',


    'name_en'=>'Name En',


    'name_ar'=>'Name Ar',


    'parent_id'=>'Category',


    'img'=>'Img',


    'active'=>'Active',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',
'mainCategory'=>'Main Category',


];
