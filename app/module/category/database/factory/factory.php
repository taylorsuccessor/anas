<?php


$factory->define(App\module\category\model\Category::class,function (Faker\Generator $faker){

    return [

            "name_en"=>$faker->realText(15),
    "name_ar"=>$faker->realText(10),
    "parent_id"=>$faker->numberBetween(0,50),
    "img"=>$faker->text(50),
    "active"=>$faker->randomElement([0,1]),

    ];
});