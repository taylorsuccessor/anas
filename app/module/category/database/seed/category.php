<?php 
namespace App\module\category\database\seed;

use Illuminate\Database\Seeder;
use App\module\category\model\Category as mCategory;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
mCategory::insert([

[


    "name_en"=>'',
    "name_ar"=>'',
    "parent_id"=>'',
    "img"=>'',
    "active"=>'',

],
]);
*/


        factory(mCategory::class,30)->create();
    }
}
