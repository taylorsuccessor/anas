<?php namespace App\module\category\admin\repository;

use Session;
use App\module\category\model\Category;
use App\module\category\admin\Repository\CategoryContract;

class EloquentCategoryRepository implements CategoryContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Category();


//if(canAccess('admin.category.allData')) {
//
//}elseif(canAccess('admin.category.groupData')){
//$oResults = $oResults->where('category.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.category.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('category.id', '=' , $data['id']);
        }
        if (isset($data['name_en']) && !empty($data['name_en'])) {
            $oResults = $oResults->where('category.name_en', '=' , $data['name_en']);
        }
        if (isset($data['name_ar']) && !empty($data['name_ar'])) {
            $oResults = $oResults->where('category.name_ar', '=' , $data['name_ar']);
        }
        if (isset($data['parent_id']) && $data['parent_id']!='') {
            $oResults = $oResults->where('category.parent_id', '=' , $data['parent_id']);
        }
        if (isset($data['main_category']) ) {
            $oResults = $oResults->where('category.parent_id', '=' ,0);
        }
        if (isset($data['sub_category']) ) {
            $oResults = $oResults->where('category.parent_id', '>' ,0);
        }
        if (isset($data['img']) && !empty($data['img'])) {
            $oResults = $oResults->where('category.img', '=' , $data['img']);
        }
        if (isset($data['active']) && $data['active'] !='') {
            $oResults = $oResults->where('category.active', '=' , $data['active']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('category.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('category.updated_at', '=' , $data['updated_at']);
        }




if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('category.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('category.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Category();

        if (isset($data['main_category']) ) {
            $oResults = $oResults->where('category.parent_id', '=' ,0);
        }
        if (isset($data['sub_category']) ) {

            $oResults = $oResults->where('category.parent_id', '>' ,0);
        }

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name();
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Category::create($data);

        if ($result) {
            Session::flash('flash_message', 'category added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$category = Category::findOrFail($id);

        return $category;
    }

    public function destroy($id)
    {


        $exist= \App\module\category\model\Category::where('parent_id','=',$id)->get();
        $existProduct= \App\module\product\model\Product::where('category_id','=',$id)->get();
        if(count($exist) || count($existProduct)){

            Session::flash('flash_warning', trans('general.youCantDelete'));
            return false;
        }

        $result =  Category::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'category deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$category = Category::findOrFail($id);
       $result= $category->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'category updated!');
            return true;
        } else {
            return false;
        }

    }

}
