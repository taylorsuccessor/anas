@extends('admin.layout::main')

@section('title', trans('general.category'))
@section('content')


    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;">
                <div class="col-lg-12">
                    <h4 class="page-title">{{ trans('general.category') }}</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                 <ol class="breadcrumb pull-left">
                          @if(isset($request->sub_category))
                        <li class="active">{{ trans('general.subCategory') }}</li>
                            @else
                            <li class="active">{{ trans('general.mainCategory') }}</li>
                        @endif
                          </ol>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <form role="search" class="app-search hidden-xs pull-right">
                        <input type="text" placeholder=" {{ trans('general.search') }} ..." class="form-control">
                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                    </form>
                </div>
            </div>



            <div class="row">


                <div class="col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">{{ trans('category::category.category') }}</h3>
                        <p class="text-muted m-b-40">{{ trans('category::category.editcategory') }}</p>
                        <!-- Nav tabs -->

                        @include('admin.layout::partial.messages')
                        <ul class="nav nav-tabs" role="tablist">


                            <li role="presentation" class="active">
                                <a href="#idetail" aria-controls="detail" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-home"></i>{{trans('general.basic')}}</span></a>
                            </li>



                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">


                            <div role="tabpanel" class="tab-pane active" id="idetail">


                                {!! Form::model($category, [
                                    'method' => 'PATCH',
                                    'url' => ['/admin/category', $category->id],
                                    'class' => 'form-horizontal'
                                ]) !!}







                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">{{ trans('category::category.editcategory') }}</span>

                                        <div class="row">
                                            <div class="imgPreview"><img  id="imgPreview" src="{{$category['img']}}" ></div>
                                        </div>



                                    </div>

                                    <div class="panel-body">




                                        <div class="row">
                                            <div class="form-group {{ $errors->has('name_en') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('name_en', trans('category::category.name_en'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                            <div class="form-group {{ $errors->has('name_ar') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('name_ar', trans('category::category.name_ar'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('name_ar', null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('name_ar', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">


                                            <div class="form-group {{ $errors->has('img') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('img', trans('category::category.img'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::text('img', null, ['class' => 'form-control uploadFile']) !!}
                                                    {!! $errors->first('img', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}  col-xs-6">
                                                {!! Form::label('active', trans('category::category.active'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('active',trans('array.active'), null, ['class' => 'form-control']) !!}
                                                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row">

                                            <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}  col-xs-6"    @if(!isset($request->sub_category)) style="display:none"  @endif>
                                                {!! Form::label('parent_id', trans('category::category.parent_id'), ['class' => 'col-sm-4 control-label']) !!}
                                                <div class="col-sm-8">
                                                    {!! Form::select('parent_id',[0=>trans('category::category.mainCategory')]+$categoryList, null, ['class' => 'form-control ']) !!}
                                                    {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>



                                            <div class="form-group">
                                            <div class="col-sm-offset-9 col-sm-3">
                                                @if(isset($request->sub_category))
                                                    {!! Form::hidden('sub_category',1) !!}
                                                @else
                                                    {!! Form::hidden('main_category',1) !!}

                                                @endif
                                                {!! Form::submit(trans('general.edit'), ['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                    </div>
                </div>

            </div>





        </div>
    </div>
@endsection