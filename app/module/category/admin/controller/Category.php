<?php

namespace App\module\category\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\category\admin\request\createRequest;
use App\module\category\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\category\model\Category as mCategory;
use App\module\category\admin\repository\CategoryContract as rCategory;


use App\module\product\admin\repository\ProductContract as rProduct;

class Category extends Controller
{
    private $rCategory;

    public function __construct(rCategory $rCategory)
    {
        $this->rCategory = $rCategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        $statistic = null;
        $oResults = $this->rCategory->getByFilter($request, $statistic);

        if (!isset($request->ajaxRequest)) {
            $categoryList = $this->rCategory->getAllList(['main_category'=>1]);
            return view('admin.category::index', compact('oResults', 'request', 'statistic','categoryList'));
        }


        $aResults = [];

        if (count($oResults)) {
            foreach ($oResults as $oResult) {
                $aResults[] = [

                    'id' => $oResult->id,

                    'name' => $oResult->name(),

                    'parent_id' => $oResult->parent_id,
                    'parent' => $oResult->parent_name(),

                    'img' => $oResult->img,

                    'active' => $oResult->active(),

                    'created_at' => $oResult->created_at,

                    'updated_at' => $oResult->updated_at,

                ];
            }
        }

        return new JsonResponse(['data' => $aResults, 'total' => (method_exists($oResults,'total')? $oResults->total():count($aResults)), 'statistic' => $statistic], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request)
    {
        $categoryList = $this->rCategory->getAllList(['main_category'=>1]);

        return view('admin.category::create', compact('request', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store(createRequest $request)
    {


        $oResults = $this->rCategory->create($request->all());

        if (!isset($request->ajaxRequest)) {
            return redirect('admin/category');
        }


        return new JsonResponse(['status' => 'success', 'data' => $oResults], 200, [], JSON_UNESCAPED_UNICODE);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function show($id, Request $request, rProduct $rProduct)
    {


        $category = $this->rCategory->show($id);


        if (isset($request->ajaxRequest)) {
            return new JsonResponse(['status' => 'success', 'data' => $category], 200, [], JSON_UNESCAPED_UNICODE);
        }


        $request->merge(['category_id' => $id, 'page_name' => 'page']);


        $request->page_name = 'page_product';
        $oProductResults = $rProduct->getByFilter($request);

        return view('admin.category::show', compact('category', 'request', 'oProductResults'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return view
     */
    public function edit($id)
    {


        $category = $this->rCategory->show($id);


        $categoryList=$this->rCategory->getAllList(['main_category'=>1]);
        return view('admin.category::edit', compact('category','categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function update($id, editRequest $request)
    {

        $result = $this->rCategory->update($id, $request);

        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.category.index'));
        }

        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return redirect
     */
    public function destroy($id, Request $request)
    {
        $category = $this->rCategory->destroy($id);

        if (!isset($request->ajaxRequest)) {
            return redirect(route('admin.category.index'));
        }


        return new JsonResponse(['status' => 'success'], 200, [], JSON_UNESCAPED_UNICODE);
    }


}
