<?php
return [


'communication'=>'Communication',
'communicationCreate'=>'Create Communication ',
    'addcommunication'=>'Add Communication',
    'editcommunication'=>'Edit Communication',
    'communicationInfo'=>'Communication Info',

    'communicationTableHead'=>'Communication Table Header',
    'communicationTableDescription'=>'Communication Description',


    'id'=>'Id',


    'address_en'=>'Address En',


    'address_ar'=>'Address Ar',


    'phone'=>'Phone',
    'phone2'=>'Phone two',


    'created_at'=>'Created At',


    'updated_at'=>'Updated At',



];
