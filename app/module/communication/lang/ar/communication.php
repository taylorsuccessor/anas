<?php
return [


    'communication'=>'التواصل',
    'communicationCreate'=>'انشاء معلومات التوصيل ',
    'addcommunication'=>'اضافة المعلومات',
    'editcommunication'=>'اضافة المعلومات',
    'communicationInfo'=>'معلومات التصال',

    'communicationTableHead'=>'معلومات التصال',
    'communicationTableDescription'=>'قائمة معلومات الاتصال',


    'id'=>'الرقم',


    'address_en'=>'العنوان',


    'address_ar'=>'العنوان العربي',


    'phone'=>'الرقم',
    'phone2'=>'الرقم الثاني',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',



];
