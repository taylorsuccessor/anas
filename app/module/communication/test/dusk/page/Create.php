<?php
namespace App\module\communication\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Create extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/communication/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testCreate();

    }

    public function testValidation(){



    $this->browser
        ->type('@address_ar','9')
            ->type('@phone','9')
    
    ->clear('@address_en')
    ->click('@submit')
    ->assertSee('address_en field is required');



    $this->browser
        ->type('@address_en','9')
            ->type('@phone','9')
    
    ->clear('@address_ar')
    ->click('@submit')
    ->assertSee('address_ar field is required');



    $this->browser
        ->type('@address_en','9')
            ->type('@address_ar','9')
    
    ->clear('@phone')
    ->click('@submit')
    ->assertSee('phone field is required');


    }



    public function testCreate(){




    $this->browser->visit($this->url());

    $this->browser
            ->type('@address_en','9')
            ->type('@address_ar','9')
            ->type('@phone','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@address_en','9')
            ->type('@address_ar','9')
            ->type('@phone','9')
    
    ->click('@submit')
    ->assertSee('added');



    $this->browser->visit($this->url());

    $this->browser
            ->type('@address_en','9')
            ->type('@address_ar','9')
            ->type('@phone','9')
    
    ->click('@submit')
    ->assertSee('added');


    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

    "@address_en"=>"[name=address_en]",

    "@address_ar"=>"[name=address_ar]",

    "@phone"=>"[name=phone]",


            '@submit'=>'[type=submit]'
        ];
    }
}
