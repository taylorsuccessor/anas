<?php
namespace App\module\communication\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Edit extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/communication/1/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $this->browser=$browser;
        $browser->assertPathIs($this->url());

        $this->testValidation();
        $this->testEdit();

    }

    public function testValidation(){


    $this->browser
    ->clear('@address_en')
    ->click('@submit')
    ->assertSee('address_en field is required');


    $this->browser
    ->clear('@address_ar')
    ->click('@submit')
    ->assertSee('address_ar field is required');


    $this->browser
    ->clear('@phone')
    ->click('@submit')
    ->assertSee('phone field is required');



    }



    public function testEdit(){





    $this->browser->visit($this->url());


    $this->browser
    ->clear('@address_en')
    ->type('@address_en','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@address_ar')
    ->type('@address_ar','9')
    ->click('@submit')
    ->assertSee('updated');


    $this->browser->visit($this->url());


    $this->browser
    ->clear('@phone')
    ->type('@phone','9')
    ->click('@submit')
    ->assertSee('updated');



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
     public function elements()
     {
      return [
              
             "@address_en"=>"[name=address_en]",
             
             "@address_ar"=>"[name=address_ar]",
             
             "@phone"=>"[name=phone]",
                         '@submit'=>'[type=submit]'
             ];
      }
}
