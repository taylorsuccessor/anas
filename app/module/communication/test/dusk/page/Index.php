<?php
namespace App\module\communication\test\dusk\page;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

use App\module\communication\model\Communication;

class Index extends BasePage
{
    private $browser=null;
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/communication';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
      $result=Communication::where('id','=',1)->first();




        $browser->click('@searchTabButton');

       $browser->type('@address_en',$result->address_en)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->address_en);


        $browser->click('@searchTabButton');

       $browser->type('@address_ar',$result->address_ar)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->address_ar);


        $browser->click('@searchTabButton');

       $browser->type('@phone',$result->phone)
           ->click('@submit')
           ->assertSeeIn('@resultTable',$result->phone);



    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [

            "@searchTabButton"=>'.right-side-toggle',


    "@address_en"=>"[name=address_en]",

    "@address_ar"=>"[name=address_ar]",

    "@phone"=>"[name=phone]",
            "@submit"=>"[name=search]",
            "@resultTable"=>'table',

        ];
    }
}
