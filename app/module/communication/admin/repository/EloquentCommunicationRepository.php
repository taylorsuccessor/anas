<?php namespace App\module\communication\admin\repository;

use Session;
use App\module\communication\model\Communication;
use App\module\communication\admin\Repository\CommunicationContract;

class EloquentCommunicationRepository implements CommunicationContract
{

    public function getByFilter($data,&$statistic=null)
    {

        $oResults = new Communication();


//if(canAccess('admin.communication.allData')) {
//
//}elseif(canAccess('admin.communication.groupData')){
//$oResults = $oResults->where('communication.user_id','=',  \Auth::user()->id);
//}elseif(canAccess('admin.communication.userData')){
//
//}else{return [];}

        if (isset($data['id']) && !empty($data['id'])) {
            $oResults = $oResults->where('communication.id', '=' , $data['id']);
        }
        if (isset($data['address_en']) && !empty($data['address_en'])) {
            $oResults = $oResults->where('communication.address_en', '=' , $data['address_en']);
        }
        if (isset($data['address_ar']) && !empty($data['address_ar'])) {
            $oResults = $oResults->where('communication.address_ar', '=' , $data['address_ar']);
        }
        if (isset($data['phone']) && !empty($data['phone'])) {
            $oResults = $oResults->where('communication.phone', '=' , $data['phone']);
        }
        if (isset($data['created_at']) && !empty($data['created_at'])) {
            $oResults = $oResults->where('communication.created_at', '=' , $data['created_at']);
        }
        if (isset($data['updated_at']) && !empty($data['updated_at'])) {
            $oResults = $oResults->where('communication.updated_at', '=' , $data['updated_at']);
        }


if ($statistic !== null) {
$statistic = $this->getStatistic(clone $oResults);
}

        if (isset($data['order']) && !empty($data['order'])) {
            $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : 'desc';
            $oResults = $oResults->orderBy('communication.'.$data['order'], $sort);
        }else{
$oResults = $oResults->orderBy('communication.id', 'desc');
}


        if(isset($data['getAllRecords']) && !empty($data['getAllRecords'])){
             $oResults = $oResults->get();
        }
        elseif (isset($data['page_name']) && !empty($data['page_name'])) {
             $oResults = $oResults->paginate(config('anas.pagination_size'), ['*'], $data['page_name']);
        }else{
             $oResults = $oResults->paginate(config('anas.pagination_size'));
        }
        return $oResults;
    }

    public function getAllList($data=[]){

          $oResults = new Communication();

          $oResults = $oResults->get();

$aResults=[];

foreach($oResults as $result){
$aResults[$result->id]=$result->name;
}
          return $aResults;
    }


public function getStatistic($oResults)
{
$oTotalResults=clone $oResults;

$current_month = gmdate('Y-m');

$totalResults=$oTotalResults->count();
return ['total'=>$totalResults];
}


    public function create($data)
    {

        $result = Communication::create($data);

        if ($result) {
            Session::flash('flash_message', 'communication added!');
            return $result;
        } else {
            return false;
        }
    }

    public function show($id)
    {

$communication = Communication::findOrFail($id);

        return $communication;
    }

    public function destroy($id)
    {

        $result =  Communication::destroy($id);

        if ($result) {
            Session::flash('flash_message', 'communication deleted!');
            return true;
        } else {
            return false;
        }
    }

    public function update($id,$data)
    {
$communication = Communication::findOrFail($id);
       $result= $communication->update(is_array($data)? $data:$data->all());
        if ($result) {
            Session::flash('flash_message', 'communication updated!');
            return true;
        } else {
            return false;
        }

    }

}
