<?php

namespace App\module\communication\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\communication\admin\request\createRequest;
use App\module\communication\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\communication\model\Communication as mCommunication;
use App\module\communication\admin\repository\CommunicationContract as rCommunication;



class Communication extends Controller
{
private $rCommunication;

public function __construct(rCommunication $rCommunication)
{
$this->rCommunication=$rCommunication;
}
/**
* Display a listing of the resource.
*
* @return void
*/
public function index(Request $request)
{

$statistic=null;
$oResults=$this->rCommunication->getByFilter($request,$statistic);

if(!isset($request->ajaxRequest)){
return view('admin.communication::index', compact('oResults','request','statistic'));
}


$aResults=[];

if(count($oResults)){
foreach($oResults as $oResult){
$aResults[]=[

    'id'=>  $oResult->id,

    'address_en'=>  $oResult->address_en,

    'address_ar'=>  $oResult->address_ar,

    'phone'=>  $oResult->phone,

    'created_at'=>  $oResult->created_at,

    'updated_at'=>  $oResult->updated_at,

];
}
}

return new JsonResponse(['data'=>$aResults,'total'=>$oResults->total(),'statistic'=>$statistic],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Show the form for creating a new resource.
*
* @return view
*/
public function create(Request $request)
{


return view('admin.communication::create',compact('request'));
}

/**
* Store a newly created resource in storage.
*
* @return redirect
*/
public function store(createRequest $request)
{


$oResults=$this->rCommunication->create($request->all());

if(!isset($request->ajaxRequest)){
return redirect('admin/communication');
}


return new JsonResponse(['status'=>'success','data'=>$oResults],200,[],JSON_UNESCAPED_UNICODE);

}

/**
* Display the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function show($id,Request $request)
{


$communication=$this->rCommunication->show($id);



if(isset($request->ajaxRequest)){
return new JsonResponse(['status'=>'success','data'=>$communication],200,[],JSON_UNESCAPED_UNICODE);
}



$request->merge(['communication_id'=>$id,'page_name'=>'page']);



return view('admin.communication::show', compact('communication','request'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
*
* @return view
*/
public function edit($id)
{


$communication=$this->rCommunication->show($id);


return view('admin.communication::edit', compact('communication'));
}

/**
* Update the specified resource in storage.
*
* @param  int  $id
*
* @return redirect
*/
public function update($id, editRequest $request)
{

$result=$this->rCommunication->update($id,$request);

if(!isset($request->ajaxRequest)){
return redirect('/admin/communication/1/edit');
}

return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
*
* @return redirect
*/
public function destroy($id,Request $request)
{
$communication=$this->rCommunication->destroy($id);

if(!isset($request->ajaxRequest)){
return redirect(route('admin.communication.index'));
}


return new JsonResponse(['status'=>'success'],200,[],JSON_UNESCAPED_UNICODE);
}


}
