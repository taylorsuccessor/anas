<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('communication', function (Blueprint $table) {
            $table->increments('id');

    $table->string('address_en');

    $table->string('address_ar');

            $table->string('phone');
            $table->string('phone2')->nullable();


$table->timestamps();
        });

//        Schema::table('communication', function (Blueprint $table) {
//            $table->renameColumn('price', 'total');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('communication');
    }
}
