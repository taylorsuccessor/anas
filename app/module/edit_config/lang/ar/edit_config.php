<?php
return [


    'edit_config'=>'تعديل الاعدادات',
    'edit_configCreate'=>'انشاء اعدادات ',
    'addedit_config'=>'اضافة اعدادات',
    'editedit_config'=>'تعديل',
    'edit_configInfo'=>'المعلومات',

    'edit_configTableHead'=>'الاعدادات',
    'edit_configTableDescription'=>'قائمة الاعدادات',


    'id'=>'الرقم',


    'key'=>'المفتاح',


    'value'=>'القيمة',


    'created_at'=>'تاريخ الانشاء',


    'updated_at'=>'تاريخ التعديل',

    'admin_email'=>'بريد المدير',
'home_banner'=>' الصورة الرئيسية ',



];
