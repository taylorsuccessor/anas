<?php
return [


'layout'=>'Layout',
'layoutCreate'=>'Create Layout ',
    'addlayout'=>'Add Layout',
    'editlayout'=>'Edit Layout',
    'layoutInfo'=>'Layout Info',

    'layoutTableHead'=>'Layout Table Header',
    'layoutTableDescription'=>'Layout Description',


    'id'=>'Id',


    'slug'=>'Slug',



'orders'=>' الطلبات ',
'users'=>' المستخدمين ',
'products'=>'المنتجات',
'categories'=>'الفئات',
'advertises'=>'الإعلانات',


'order'=>' طلب ',
'notConfirmOrder'=>' الطلبيات الغير مؤكدة ',
'pendingOrder'=>' الطلبيات المقدمة للتأكيد ',
'confirmedOrder'=>' الطلبيات الموافق عليها ',
'assignOrder'=>'الطلبيات المسندة لسائق',
'executedOrder'=>'الطلبيات المنجزة',
'rejectedOrder'=>' الطلبيات المرفوضة ',


'user'=>' مستخدم ',
'admin'=>' مدير ',
'driver'=>' سائق ',
'customer'=>' عميل ',
'vipCustomer'=>' عميل مميز ',

'product'=>' منتج ',
'totalProduct'=>' جميع المنتجات ',
'deactivateProduct'=>' المنتجات غير المفعلة ',
'activeProduct'=>' المنتجات المفعلة ',


'offer'=>'عرض',
'normalOffer'=>' عرض عادي ',
'vipOffer'=>' عرض مميز ',

'category'=>' فئة ',
'subCategory'=>' فئة فرعية ',
'mainCategory' => ' الفئة الرئيسية ',
];
