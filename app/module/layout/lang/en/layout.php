<?php
return [


'layout'=>'Layout',
'layoutCreate'=>'Create Layout ',
    'addlayout'=>'Add Layout',
    'editlayout'=>'Edit Layout',
    'layoutInfo'=>'Layout Info',

    'layoutTableHead'=>'Layout Table Header',
    'layoutTableDescription'=>'Layout Description',


    'id'=>'Id',


    'slug'=>'Slug',

'orders'=>'Orders',
'users'=>'Users',
'products'=>'Products',
'categories'=>'Categories',
'advertises'=>'Advertises',

'order'=>'Order',
'notConfirmOrder'=>'Not confirmed Order',
'pendingOrder'=>'Pending Order',
'confirmedOrder'=>'Confirmed Order',
'assignOrder'=>'Assigned Order',
'executedOrder'=>'Executed Order',
'rejectedOrder'=>'Rejected Order',


'user'=>'User',
'admin'=>'Admin',
'driver'=>'Driver',
'customer'=>'Customer',
'vipCustomer'=>'VIP Customer',

'product'=>'Product',
'totalProduct'=>'Total Product',
'deactivateProduct'=>'Deactivate Product',
'activeProduct'=>'Active Product',


'offer'=>'Offer',
'normalOffer'=>'Normal Offer',
'vipOffer'=>'VIP Offer',

'category'=>'Category',
'subCategory'=>'Sub Category',
'mainCategory'=>'Main Category',
];
