<?php

namespace App\module\layout\admin\controller;


use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\module\cart\admin\request\createRequest;
use App\module\cart\admin\request\editRequest;
use Session;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


use App\module\cart\model\Cart as mCart;
use App\module\cart\admin\repository\CartContract as rCart;

use App\module\order\model\Order as mOrder;
use App\module\user\model\User as mUser;
use App\module\product\model\Product as mProduct;
use App\module\category\model\Category as mCategory;

use App\module\order\admin\repository\OrderContract as rOrder;
use App\module\product\admin\repository\ProductContract as rProduct;
use App\module\category\admin\repository\CategoryContract as rCategory;


use App\module\user\admin\repository\UserContract as rUser;

class Dashboard extends Controller
{


    public function index(Request $request, rProduct $rProduct)
    {

    //  success danger info  inverse
$statisticList=[];

/*
'notConfirmOrder'=>'Not confirmed Order',
'pendingOrder'=>'Pending Order',
'assignOrder'=>Assigned Order',
'executedOrder'=>'Executed Order',
'rejectedOrder'=>'Rejected Order',
*/
$totalOrder=mOrder::count() / 100;
$totalOrder=($totalOrder ==0)? 1:$totalOrder;
$notConfirmOrder=mOrder::where('status',config('array.order_status_not_confirmed_index'))->count();


   $statisticList[]=[
   'title'=>trans('layout::layout.notConfirmOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$notConfirmOrder,
   'percent'=> ($notConfirmOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'info',
   'arrow'=>'down',
   'newSectionTitle'=>'orders'
   ];




$pendingOrder=mOrder::where('status',config('array.order_status_pending_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.pendingOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$pendingOrder,
   'percent'=> ($pendingOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'inverse',
   'arrow'=>'up',
   ];


$confirmedOrder=mOrder::where('status',config('array.order_status_confirmed_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.confirmedOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$confirmedOrder,
   'percent'=> ($confirmedOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'inverse',
   'arrow'=>'up',
   ];




$assignOrder=mOrder::where('status',config('array.order_status_assigned_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.assignOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$assignOrder,
   'percent'=> ($assignOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'success',
   'arrow'=>'up',
   ];


$executedOrder=mOrder::where('status',config('array.order_status_executed_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.executedOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$executedOrder,
   'percent'=> ($executedOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'success',
   'arrow'=>'up',
   ];


$rejectedOrder=mOrder::where('status',config('array.order_status_reject_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.rejectedOrder'),
   'smallTitle'=>trans('layout::layout.order'),
   'total'=>$rejectedOrder,
   'percent'=> ($rejectedOrder/$totalOrder),
   'icon'=>'ti-shopping-cart',
   'status'=>'danger',
   'arrow'=>'up',
   ];



/*

'user'=>'User',
'admin'=>'Admin',
'driver'=>'Driver',
'customer'=>'Customer',
'vipCustomer'=>'VIP Customer',

       'user_type_admin_index'=>0,
    'user_type_customer_index'=>1,
    'user_type_vip_customer_index'=>2,
    'user_type_driver_index'=>3,
*/

$totalUser=mUser::count() / 100;

$userNumber=mUser::where('type',config('array.user_type_admin_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.admin'),
   'smallTitle'=>trans('layout::layout.user'),
   'total'=>$userNumber,
   'percent'=> ($userNumber/$totalUser),
   'icon'=>'ti-user',
   'status'=>'danger',
   'arrow'=>'up',
   'newSectionTitle'=>'users'
   ];

$userNumber=mUser::where('type',config('array.user_type_customer_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.customer'),
   'smallTitle'=>trans('layout::layout.user'),
   'total'=>$userNumber,
   'percent'=> ($userNumber/$totalUser),
   'icon'=>'ti-user',
   'status'=>'success',
   'arrow'=>'up',
   ];

$userNumber=mUser::where('type',config('array.user_type_vip_customer_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.vipCustomer'),
   'smallTitle'=>trans('layout::layout.user'),
   'total'=>$userNumber,
   'percent'=> ($userNumber/$totalUser),
   'icon'=>'ti-user',
   'status'=>'success',
   'arrow'=>'up',
   ];
$userNumber=mUser::where('type',config('array.user_type_driver_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.driver'),
   'smallTitle'=>trans('layout::layout.user'),
   'total'=>$userNumber,
   'percent'=> ($userNumber/$totalUser),
   'icon'=>'ti-user',
   'status'=>'success',
   'arrow'=>'up',
   ];


/*

'totalProduct'=>'Total Product',
'deactivateProduct'=>'Deactivate Product',
'activeProduct'=>'Active Product',

*/
$totalProduct=mProduct::count() / 100;


   $statisticList[]=[
   'title'=>trans('layout::layout.totalProduct'),
   'smallTitle'=>trans('layout::layout.product'),
   'total'=>$totalProduct*100,
   'percent'=> ($totalProduct/$totalProduct),
   'icon'=>'fa   fa-cubes',
   'status'=>'success',
   'arrow'=>'up',
   'hideProgressBar'=>1,
   'newSectionTitle'=>'products'
   ];


$productNumber=mProduct::where('active',config('array.active_deactivate_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.deactivateProduct'),
   'smallTitle'=>trans('layout::layout.product'),
   'total'=>$productNumber,
   'percent'=> ($productNumber/$totalProduct),
   'icon'=>'fa fa-cubes',
   'status'=>'danger',
   'arrow'=>'up',
   ];


$productNumber=mProduct::where('active',config('array.active_activate_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.activeProduct'),
   'smallTitle'=>trans('layout::layout.product'),
   'total'=>$productNumber,
   'percent'=> ($productNumber/$totalProduct),
   'icon'=>'fa fa-cubes',
   'status'=>'success',
   'arrow'=>'up',

   ];



$totalOffer=mProduct::where('offer_type','!=',config('array.product_offer_type_not_offer_index'))->count();
$totalOffer=$totalOffer==0? 1:$totalOffer / 100;
$productNumber=mProduct::where('offer_type',config('array.product_offer_type_offer_index'))->count();

   $statisticList[]=[
   'title'=>trans('layout::layout.normalOffer'),
   'smallTitle'=>trans('layout::layout.offer'),
   'total'=>$productNumber,
   'percent'=> ($productNumber/$totalOffer),
   'icon'=>'fa  fa-bullhorn ',
   'status'=>'success',
   'arrow'=>'up',

   ];



$productNumber=mProduct::where('offer_type',config('array.product_offer_type_vip_offer_index'))->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.vipOffer'),
   'smallTitle'=>trans('layout::layout.offer'),
   'total'=>$productNumber,
   'percent'=> ($productNumber/$totalOffer),
   'icon'=>'fa   fa-bullhorn ',
   'status'=>'success',
   'arrow'=>'up',

   ];



$totalCategory=mCategory::count() / 100;

$categoryNumber=mCategory::where('parent_id','=',0)->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.mainCategory'),
   'smallTitle'=>trans('layout::layout.category'),
   'total'=>$categoryNumber,
   'percent'=> ($categoryNumber/$totalCategory),
   'icon'=>'fa   fa-cubes',
   'status'=>'success',
   'arrow'=>'up',
   'hideProgressBar'=>1,
   'newSectionTitle'=>'categories'

   ];


$categoryNumber=mCategory::where('parent_id','>',0)->count();
   $statisticList[]=[
   'title'=>trans('layout::layout.subCategory'),
   'smallTitle'=>trans('layout::layout.category'),
   'total'=>$categoryNumber,
   'percent'=> ($categoryNumber/$totalCategory),
   'icon'=>'fa   fa-cubes',
   'status'=>'success',
   'arrow'=>'up',
   'hideProgressBar'=>1,

   ];


            return view('admin.layout::dashboard.index', compact('statisticList'));

        }
}