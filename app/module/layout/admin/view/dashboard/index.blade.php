@extends('admin.layout::main')
@section('title', trans('general.cart'))

@section('content')

    <div id="content-wrapper" >

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title" style="background:url(/assets/admin/images/banner-img.png) no-repeat center center /cover;                    ">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"> {{trans('general.dashboard') }}</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                <div class="row">



@foreach($statisticList as $oneStatistic)

@if(isset($oneStatistic['newSectionTitle']))
<div class="col-xs-12">
<h2>{{ trans('layout::layout.'.$oneStatistic['newSectionTitle']) }}</h2>
</div>
@endif

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title"><i class="{{$oneStatistic['icon'] }} text-success"></i> {{$oneStatistic['title'] }}</h3>
                            <div class="text-right"> <span class="text-muted"> {{$oneStatistic['smallTitle'] }} </span>
                                <h1><sup></sup> {{$oneStatistic['total'] }}</h1>
                            </div>
                           @if(!isset($oneStatistic['hideProgressBar']))
                            <span class="text-success">{{ round ($oneStatistic['percent'],2) }}%</span>

                           <div class="progress m-b-0">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                style="width:{{$oneStatistic['percent'] }}%;">

                                <span class="sr-only">{{$oneStatistic['percent'] }}% {{$oneStatistic['smallTitle'] }} </span>
                                 </div>
                            </div>
                            @else
                            <br>
                            @endif


                        </div>
                    </div>
  @endForeach







                </div>

            </div>
            <!-- /.container-fluid -->
               </div>
    </div>

    @stop
