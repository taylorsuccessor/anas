<?php
return [
    'admin_menu'=>[




        [
            'route' => 'admin.product.index',
            'title' => 'product',
            'icon' => 'fa  fa-cubes',
            'subMenus' => [



                [
                    'route' => 'admin.category.index',
                    'title' => 'mainCategory',
                    'icon' => 'fa fa-cubes',
                    'parameter'=>'?main_category=1',
                ],
                [
                    'route' => 'admin.category.index',
                    'title' => 'subCategory',
                    'icon' => 'fa fa-cubes',
                    'parameter'=>'?sub_category=1',
                ],
                [
                    'route' => 'admin.product.index',
                    'title' => 'product',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.product.changeQuantity',
                    'title' => 'changeQuantity',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],

                [
                    'route' => 'admin.product.addOffer',
                    'title' => 'addOffer',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],

            ]

        ],




        [
            'route' => 'admin.order.index',
            'title' => 'order',
            'icon' => 'fa  fa-file',
            'subMenus' => [




                [
                    'route' => 'admin.order.index',
                    'title' => 'invoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'',
                ],



                [
                    'route' => 'admin.order.index',
                    'title' => 'pendingInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_pending_index'),
                ],

                [
                    'route' => 'admin.order.index',
                    'title' => 'confirmedInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_confirmed_index'),
                ],


                [
                    'route' => 'admin.order.index',
                    'title' => 'assignedInvoiceDriver',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_assigned_index'),
                ],

                [
                    'route' => 'admin.order.index',
                    'title' => 'executedInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_executed_index'),
                ],



                [
                    'route' => 'admin.order.index',
                    'title' => 'rejectInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_reject_index'),
                ],



                [
                    'route' => 'admin.cart.index',
                    'title' => 'cart',
                    'icon' => 'fa fa-shopping-cart',
                    'parameter'=>'',
                ],
        ]

        ],






        [
            'route' => 'admin.user.index',
            'title' => 'driver',
            'icon' => 'fa  fa-user',
            'subMenus' => [

                [
                    'route' => 'admin.user.index',
                    'title' => 'driver',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_driver_index'),
                ],

            ]

        ],




        [
            'route' => 'admin.advertise.index',
            'title' => 'advertise',
            'icon' => 'fa  fa-bullhorn ',
            'subMenus' => [




                [
                    'route' => 'admin.advertise.index',
                    'title' => 'advertise',
                    'icon' => 'fa fa-bullhorn  ',
                    'parameter'=>'',
                ],
            ]

        ],






        [
            'route' => 'admin.user.index',
            'title' => 'setting',
            'icon' => 'fa  fa-users',
            'subMenus' => [




                [
                    'route' => 'admin.user.index',
                    'title' => 'user',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'admin',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_admin_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'driver',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_driver_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'customer',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_customer_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'vip_customer',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_vip_customer_index'),
                ],


            ]

        ],










        [
            'route' => 'admin.user.index',
            'title' => 'setting',
            'icon' => 'fa  fa-cogs',
            'subMenus' => [

                [
                    'route' => 'admin.product_type.index',
                    'title' => 'product_type',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.area.index',
                    'title' => 'area',
                    'icon' => 'fa fa-map',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.company_type.index',
                    'title' => 'company_type',
                    'icon' => 'fa fa-building',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.cms.index',
                    'title' => 'cms',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],

                [
                    'route' => 'admin.faq.index',
                    'title' => 'faq',
                    'icon' => 'fa fa-comments-o',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.communication.index',
                    'title' => 'communication',
                    'icon' => 'fa fa-fax',
                    'parameter'=>'/1/edit',
                ],

                [
                    'route' => 'admin.contact_us.index',
                    'title' => 'contact_us',
                    'icon' => 'fa fa-envelope',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.notification.index',
                    'title' => 'notification',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.role.index',
                    'title' => 'role',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.edit_config.create',
                    'title' => 'setting',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
            ]

        ],




        [
            'route' => 'admin.user.index',
            'title' => 'report',
            'icon' => 'fa  fa-file-text ',
            'subMenus' => [



 [
                    'route' => 'dashboard',
                    'title' => 'dashboard',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],


                [
                    'route' => 'admin.user.customerReport',
                    'title' => 'customerReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type[]='.config('array.user_type_vip_customer_index').'&type[]='.config('array.user_type_customer_index'),
                ],
                [
                    'route' => 'admin.user.driverReport',
                    'title' => 'driverReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type='.config('array.user_type_driver_index'),
                ],
                [
                    'route' => 'admin.user.adminReport',
                    'title' => 'adminReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type='.config('array.user_type_admin_index'),
                ],
                [
                    'route' => 'admin.cart.cartUserReport',
                    'title' => 'cartUserReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.cart.cartProductReport',
                    'title' => 'cartProductReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.product.productReport',
                    'title' => 'productReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],




            ]

        ],


        /*
advertise
-area
-cart
-category
cms
communication
-company_type
contact_us
faq
-order
-product
-product_type
*/


/*

        [
            'route' => 'admin.area.index',
            'title' => 'area',
            'icon' => 'fa  fa-map',
            'subMenus' => [




                [
                    'route' => 'admin.area.index',
                    'title' => 'area',
                    'icon' => 'fa fa-map',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.company_type.index',
                    'title' => 'company_type',
                    'icon' => 'fa fa-building',
                    'parameter'=>'',
                ],
            ]

        ],


        [
            'route' => 'admin.category.index',
            'title' => 'category',
            'icon' => 'fa  fa-cubes',
            'subMenus' => [




                [
                    'route' => 'admin.category.index',
                    'title' => 'category',
                    'icon' => 'fa fa-cubes',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.category.index',
                    'title' => 'mainCategory',
                    'icon' => 'fa fa-cubes',
                    'parameter'=>'?main_category=1',
                ],
                [
                    'route' => 'admin.category.index',
                    'title' => 'subCategory',
                    'icon' => 'fa fa-cubes',
                    'parameter'=>'?sub_category=1',
                ],

                [
                    'route' => 'admin.product_type.index',
                    'title' => 'product_type',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.product.index',
                    'title' => 'product',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.product.changeQuantity',
                    'title' => 'changeQuantity',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],

                [
                    'route' => 'admin.product.addOffer',
                    'title' => 'addOffer',
                    'icon' => 'fa fa-cube',
                    'parameter'=>'',
                ],

            ]

        ],



        [
            'route' => 'admin.order.index',
            'title' => 'order',
            'icon' => 'fa  fa-file-text',
            'subMenus' => [




                [
                    'route' => 'admin.order.index',
                    'title' => 'invoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'',
                ],



                [
                    'route' => 'admin.order.index',
                    'title' => 'pendingInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_pending_index'),
                ],

                [
                    'route' => 'admin.order.index',
                    'title' => 'confirmedInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_confirmed_index'),
                ],


                [
                    'route' => 'admin.order.index',
                    'title' => 'assignedInvoiceDriver',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_assigned_index'),
                ],

                [
                    'route' => 'admin.order.index',
                    'title' => 'executedInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_executed_index'),
                ],



                [
                    'route' => 'admin.order.index',
                    'title' => 'rejectInvoice',
                    'icon' => 'fa fa-file-text',
                    'parameter'=>'?status='.config('array.order_status_reject_index'),
                ],



                [
                    'route' => 'admin.cart.index',
                    'title' => 'cart',
                    'icon' => 'fa fa-shopping-cart',
                    'parameter'=>'',
                ],
            ]

        ],

        [
            'route' => 'admin.advertise.index',
            'title' => 'advertise',
            'icon' => 'fa  fa-bullhorn ',
            'subMenus' => [




                [
                    'route' => 'admin.advertise.index',
                    'title' => 'advertise',
                    'icon' => 'fa fa-bullhorn  ',
                    'parameter'=>'',
                ],
            ]

        ],


        [
            'route' => 'admin.cms.index',
            'title' => 'cms',
            'icon' => 'fa  fa-wordpress  ',
            'subMenus' => [




                [
                    'route' => 'admin.cms.index',
                    'title' => 'cms',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],
            ]

        ],

        [
            'route' => 'admin.communication.index',
            'title' => 'communication',
            'icon' => 'fa  fa-fax',
            'subMenus' => [




                [
                    'route' => 'admin.communication.index',
                    'title' => 'communication',
                    'icon' => 'fa fa-fax',
                    'parameter'=>'/1/edit',
                ],
            ]

        ],


        [
            'route' => 'admin.contact_us.index',
            'title' => 'contact_us',
            'icon' => 'fa  fa-envelope  ',
            'subMenus' => [




                [
                    'route' => 'admin.contact_us.index',
                    'title' => 'contact_us',
                    'icon' => 'fa fa-envelope',
                    'parameter'=>'',
                ],
            ]

        ],



        [
            'route' => 'admin.faq.index',
            'title' => 'faq',
            'icon' => 'fa  fa-comments-o',
            'subMenus' => [




                [
                    'route' => 'admin.faq.index',
                    'title' => 'faq',
                    'icon' => 'fa fa-comments-o',
                    'parameter'=>'',
                ],
            ]

        ],



        [
            'route' => 'admin.user.index',
            'title' => 'setting',
            'icon' => 'fa  fa-cogs',
            'subMenus' => [




                [
                    'route' => 'admin.user.index',
                    'title' => 'user',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'admin',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_admin_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'driver',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_driver_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'customer',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_customer_index'),
                ],
                [
                    'route' => 'admin.user.index',
                    'title' => 'vip_customer',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'?type='.config('array.user_type_vip_customer_index'),
                ],
                [
                    'route' => 'admin.role.index',
                    'title' => 'role',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.notification.index',
                    'title' => 'notification',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.edit_config.create',
                    'title' => 'setting',
                    'icon' => 'fa fa-gears',
                    'parameter'=>'',
                ],
                ]

        ],






        [
            'route' => 'admin.user.index',
            'title' => 'report',
            'icon' => 'fa  fa-file-text ',
            'subMenus' => [


                [
                    'route' => 'admin.user.customerReport',
                    'title' => 'customerReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type[]='.config('array.user_type_vip_customer_index').'&type[]='.config('array.user_type_customer_index'),
                ],
                [
                    'route' => 'admin.user.driverReport',
                    'title' => 'driverReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type='.config('array.user_type_driver_index'),
                ],
                [
                    'route' => 'admin.user.adminReport',
                    'title' => 'adminReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'?type='.config('array.user_type_admin_index'),
                ],
                [
                    'route' => 'admin.cart.cartUserReport',
                    'title' => 'cartUserReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],
                [
                    'route' => 'admin.cart.cartProductReport',
                    'title' => 'cartProductReport',
                    'icon' => 'fa fa-wordpress  ',
                    'parameter'=>'',
                ],

            ]

        ],


*/



    ],
];